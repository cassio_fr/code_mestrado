function [H_trng, mse_trng, ww, ff] = som_kl21_ver2(X,ww,cov_init_s,cov_init_x,cov_s,ETA_init,ITERATION_N)

[n,N]=size(X);
[nw,M]=size(ww);
decay = 0.12;
sigma_S=(cov_init_s); sigma_X=(cov_init_x); ETA=ETA_init;
lowest_k=cov_init_x*cov_s/(1+(decay*cov_s)*ITERATION_N);
counter = 0;
for epoch = 1:ITERATION_N
     counter = counter + 1;
    %-----Annealing---
    new_cov_som=cov_s/(1+(decay*cov_s)*epoch); % Annealing
    sigma_S=new_cov_som*cov_init_x;
    sigma_X=new_cov_som*cov_init_x;
    ETA=ETA*1;
    %----------------
      
    dJ_w=zeros(2,length(ww));;
    
    for j=1:M
        kernel_ww(j,:)=kernel(repmat(ww(:,j),1,M)-ww,sigma_S);
    end
    
    for k=1:M
        num_1=sum(repmat(kernel_ww(k,:),2,1).* (inv(sigma_S) * (repmat(ww(:,k),1,M)-ww)),2); 
        ent_f=2*((-num_1/sum(kernel_ww(k,:))));   % Evaluating the Entropy
        kernel_xw= kernel(repmat(ww(:,k),1,N)-X,sigma_X);
        num_3=sum(repmat(kernel_xw,2,1).* (inv(sigma_X) * (repmat(ww(:,k),1,N)-X)),2); 
        corr_fg=num_3/sum(kernel_xw);  % Evaluating the Cross_information potential between data samples and Processing Elements (PEs)
        
        dJ_w(:,k)=(ent_f+corr_fg)/M; 
    end
    ww = ww - ETA*dJ_w; % Update equation
    %    figure(1)
    if(mod(epoch,1)==0)
        
        figure(1);%subplot(2,1,1),
        cla,plot(X(1,:),X(2,:),'.'); hold on ; plot(ww(1,:),ww(2,:),'r.','MarkerSize',16); drawnow,...
            axis([ 0.3 1 -0.1 0.8]);
        
       
        ff(counter) = getframe(gcf);
       
        [err,index]=min(dist(X',ww)');
       
        mse_trng=mean(err.^2);
               
        [freq , bin_width] = hist(err, 30);
        prob = freq/sum(freq) + 0.001; % computes relative frequency in each bin
        H_trng = -sum(prob(1:end).*log(prob(1:end)));
       
        %----------error entropy calculation using Parzen----
        if(epoch==ITERATION_N)
            sigma_e = 0.01*var(err); err_entropy = 0;
            for tt = 1 :  length(err)
                err_entropy = err_entropy - log(sum(kernel(repmat(err(tt),1,length(err))-err,sigma_e))/length(err));
            end
            err_entropy = err_entropy/length(err)
       
            
        end
    end    
      
end     

function [J] = cost(X,ww,sigma_X,sigma_S,M,N,epoch)
F=0; G=0;
for i=1:M
    F=F+log(sum(kernel(repmat(ww(:,i),1,M)-ww,sigma_S))/M);       
    G=G+log(sum(kernel(repmat(ww(:,i),1,N)-X, sigma_X))/N); 
end
F=mean(F); 
G=mean(G);
J=F-G;

%---------------------------------------
function out = kernel(in,zigma)
in2 = inv(zigma)*in;
out = exp(-0.5*sum(in.*in2,1))/(2*pi*det(zigma))^(size(in,1)/2);
return 
%---------------------------------------     
