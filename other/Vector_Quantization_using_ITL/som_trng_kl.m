%--------------FACE DATA EXAMPLE--------------
% compiled by ANANT HEGDE
% Takes 3 inputs, nw_size (NETWORK SIZE, which usually is 16 or
% 32 or even 64...the higher it goes, slower is the program.
% ITERATION_N--> is the number of iterations, I usally set it to 250
% mc--> MonteCarlo Runs/Trials.
%------------------------------------------------
function [X, ww, mse_trng, H_trng]=som_trng_kl(nw_size,ITERATION_N,mc)
tic

    load facedata.mat;
    datapoints=datapoints./max(max(datapoints));  % Loading the FACE DATA
    X=datapoints'; 
%-----------------------------
for i=1:mc  % mc is for MONTE CARLO SIMULATIONS
    clear ww_init;     
    [n,N] = size(X);
%----------- for facedata.mat----------------
      ww_init1=0.35*rand(1,nw_size)+0.40;
      ww_init2=0.6*rand(1,nw_size);         % Weight Initialization
      ww_init=[ww_init1;ww_init2]; 
        
     cov_s=2;std_data=std(X,0,2);
     cov_init_s=repmat(std_data.^2,1,n).*eye(n)         % face data
     cov_init_x=cov_init_s;
     ETA_init=.03;
%     %----------------------------------------------------
    [H_trng(i), mse_trng(i),ww(:,:,i), ff] = som_kl21_ver2(X,ww_init,cov_init_s, cov_init_x,cov_s, ETA_init,ITERATION_N); % Call the subroutine
end
  
  [H_trng;mse_trng]
  toc
