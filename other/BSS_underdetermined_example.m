% Author: C�ssio Fraga Dantas
% Date: 25/11/2015

% 3 Sources, 3000 samples
S = zeros(3,3000);
% Non-overlapping sources (sparse)
S(1,1:3:end) = randn(1,3000/3);
S(2,2:3:end) = randn(1,3000/3);
S(3,3:3:end) = randn(1,3000/3);
% Some overlapping
n_overlap = 0.1*3000;
overlap_idx = ceil(3000*rand(1,n_overlap));
S(1,overlap_idx) = randn(1,n_overlap);
overlap_idx = ceil(3000*rand(1,n_overlap));
S(2,overlap_idx) = randn(1,n_overlap);
overlap_idx = ceil(3000*rand(1,n_overlap));
S(3,overlap_idx) = randn(1,n_overlap);
% Overlapping souces
% S = randn(size(S));

% View sources
subplot(3,2,1),     plot(S(1,1:300)),       title('Source 1')
subplot(3,2,3),     plot(S(2,1:300),'r'),   title('Source 2')
subplot(3,2,5),     plot(S(3,1:300),'g'),   title('Source 3')

% Mixture matrix. 3 sources, 2 mixtures
A = randn(2,3);

% Mixture
X = A*S;
% Noise
SNR_dB = inf;
N = randn(size(X));
X = X + N*10^(-SNR_dB/10)*norm(X,'fro')/norm(N,'fro');

% View mixtures
subplot(3,2,2),     plot(X(1,1:300),'y'),   title('Mixture 1')
subplot(3,2,4),     plot(X(2,1:300),'m'),   title('Mixture 2')
% Scatter-plot of mixtures
figure, plot(X(1,:),X(2,:),'.')
hold on, quiver(zeros(1,3),zeros(1,3),A(1,:),A(2,:),'LineWidth',2)