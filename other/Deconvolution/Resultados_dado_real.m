%%% Pre-processing of the Jequitinhonha's basin cdp 2000 - 3000 
%%% Deconvolution of stacked data
%%% Kenji Nose Filho - kenjinose@yahoo.com.br
%%% 20/01/2014

clc;
clear;
close all;

setpath_seismiclab;

%% Load data
[D_stack,H_stack] = readsegy('jequi_7_p_stack.su');
D_stack           = D_stack(:,1:585);
X                 = D_stack(350:500,40:42); % 1.3960s a 1.9960s/ cdp 2239 a 2241
dt                = 0.004;
f1                = 5;
f2                = 10;
f3                = 75;
f4                = 80;
f0                = zeros(1,21);
f0(11)            = 1;
[P_stack,f]       = smooth_spectrum(D_stack,dt,0,'db');
t                 = 0:dt:5;
cdp               = 2200:2800;
x_awgn            = D_stack(330:end,:);
for ii=1:size(x_awgn,2);
    x_awgn(:,ii)      = x_awgn(:,ii)/(std(x_awgn(:,ii))+eps);
end

%% Deconvolution with Huber criterion

mu                                = -0.1;
Niter                             = 1000;
Nf                                = 21;
rr                                = 1;
f0                                = zeros(1,Nf);
f0(11)                            = 1;
[y_huber,f_huber,custo_huber]     = fir_hybrid2_grad(X,f0,rr,mu,Niter);
w_huber                           = bestwiener(f_huber,length(f0));
w_huber                           = w_huber./norm(w_huber,inf);
r_huber                           = filter(f_huber,1,x_awgn);
[r_huber,r_huber_lag]             = delag1(x_awgn,r_huber,length(f_huber));

%% Deconvolution with multichannel algorithm

fc  = 20;
tw  = -0.04:dt:0.04;
Nw  = length(tw);
h0  = (1-2*pi^2*fc^2*tw.^2).*exp(-pi^2*fc^2*tw.^2);
hh  = hilbert(h0);
th  = 0;
h0  = cos(th)*real(hh)+sin(th)*imag(hh);
h0  = h0';

% h0 = w_huber';
% Nw = length(h0);

sigma     = 0.001*var(x_awgn(:));
mu        = 0.01;
lambda    = 0.0001;
iter_max  = 5;

[r_multicanal,~,~,Jcost1,Jcost2] = dictionary_decon(x_awgn,h0,sigma,mu,lambda,iter_max);

%%

x_awgn       = [zeros(329,585); x_awgn];
r_huber      = [zeros(329,585); r_huber];
r_multicanal = [zeros(329,585); r_multicanal];

for ii=1:size(x_awgn,2);
    x_awgn(:,ii)       = x_awgn(:,ii)/(std(x_awgn(:,ii))+eps);
    r_huber(:,ii)      = r_huber(:,ii)/(std(r_huber(:,ii))+eps);
    r_multicanal(:,ii) = r_multicanal(:,ii)/(std(r_multicanal(:,ii))+eps);
end

close all

figure(2), 
plot(custo_huber,'LineWidth',2),

figure(3),
subplot(311), plot(Jcost1)
subplot(312), plot(Jcost2)
subplot(313), plot(Jcost1+Jcost2)

figure(4), colormap(seismic),
subplot(131), imagesc(cdp,t,perc99(gain(x_awgn,dt,'time',[3 0],0))), 
subplot(132), imagesc(cdp,t,perc99(gain(r_huber,dt,'time',[3 0],0))),
subplot(133), imagesc(cdp,t,perc99(gain(r_multicanal,dt,'time',[3 0],0))),

%%

[P_dn,f]     = smooth_spectrum(x_awgn,dt,0,'db');
P_huber      = smooth_spectrum(r_huber,dt,0,'db');
P_multicanal = smooth_spectrum(r_multicanal,dt,0,'db');

figure(5),
subplot(131), plot(f,P_dn,'k','Linewidth',2), axis([0 f(end) -20 0]); grid on
subplot(132), plot(f,P_huber,'k','Linewidth',2), axis([0 f(end) -20 0]); grid on
subplot(133), plot(f,P_multicanal,'k','Linewidth',2), axis([0 f(end) -20 0]); grid on

