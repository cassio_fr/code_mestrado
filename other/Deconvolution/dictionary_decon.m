function [r,dp,w,Jcost1,Jcost2] = dictionary_decon(d,w0,sigma,mu,lambda,iter_max);

nw = length(w0);
[nt,ntraces] = size(d);

I = eye(nt);

for ii=1:nt
    W0(:,ii) = filter(w0,1,I(:,ii));
end
W      = W0;
mask   = sign(abs(W0));
d      = d;
Rww    = W'*W;                          % Autocorrelation of wavelet
Rww    = Rww + sigma*I;
r      = pinv(Rww)*W'*d;
Rrr    = r*r';                          
Rrr    = Rrr + sigma*I;
dp     = W*r;
e      = dp-d;

for k = 1:iter_max
    %% sparse coding
    gradr     = -2*(Rww*r-W'*d) - lambda*sign(r);
    r         = r + mu*gradr;
    Rww       = W'*W;                          
    Rww       = Rww + sigma*I;
    %% dictionary update
    gradW     = -2*(W*Rrr-d*r');
    W         = W + 1*mu*gradW;
    W         = W.*mask;
    Rrr       = r*r';                          
    Rrr       = Rrr + sigma*I; 
    dp        = W*r;
    e         = dp-d;
    Jcost1(k) = norm(e(:),2)^2;
    Jcost2(k) = lambda*norm(r(:),1);
    
end

figure(10), imagesc(W)
w = W(1:nw,1);

return;


