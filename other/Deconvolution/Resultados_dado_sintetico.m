%%% Blind deconvolution with dictionary learning

clc;     
clear;
close all;

load synthetic2

%catches the default stream used by the random generator
defaultStream = RandStream.getGlobalStream;

%establishes the random seed
stream_exp = RandStream('mt19937ar','seed',1); defaultStream.State = stream_exp.State;

r  = Data(1:250,1:15);
r  = [zeros(100,15); r; zeros(100,15)];
dt = 2e-3;

fc = 40;
tw = -0.03:dt:0.03;
h  = (1-2*pi^2*fc^2*tw.^2).*exp(-pi^2*fc^2*tw.^2);
hh = hilbert(h);
th = -50*pi/180;
h  = cos(th)*real(hh)+sin(th)*imag(hh);
h  = h';

x = filter(h,1,r);

%% Aditive White Gaussian Noise
n              = randn(size(r));
x              = x./std(x(:));
n              = n./std(n(:));
SNR            = 8;
SNR_dB         = 10*log10(SNR)
alpha          = 1./sqrt(SNR);
n              = alpha*n;
x_awgn         = x + n;
% [x_awgn,d_lag] = delag1(r,x_awgn,length(h));
x_awgn         = x_awgn./std(x_awgn(:));

%% Dictionary learning algorithm

fc  = 60;
tw  = -0.03:dt:0.03;
h0  = (1-2*pi^2*fc^2*tw.^2).*exp(-pi^2*fc^2*tw.^2);
hh  = hilbert(h0);
th  = 0;
h0  = cos(th)*real(hh)+sin(th)*imag(hh);
h0  = h0';

sigma     = 0.5*var(x_awgn(:));
mu        = 0.001;
lambda    = 4;
iter_max  = 1000;
[refl,dp,w,Jcost1,Jcost2] = dictionary_decon(x_awgn,h0,sigma,mu,lambda,iter_max);

figure,
subplot(311), plot(Jcost1)
subplot(312), plot(Jcost2)
subplot(313), plot(Jcost1+Jcost2)

figure,
subplot(121), wigb(r)
subplot(122), wigb(refl)

figure,
subplot(121), wigb(x_awgn)
subplot(122), wigb(dp)

figure,
plot(h), hold on
plot(h0,'k')
plot(w,'r')
