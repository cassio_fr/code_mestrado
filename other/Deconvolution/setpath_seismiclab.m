%SETPATH: program to set path for toolboxes
 
 Dir1='C:\Users\Kenji\OneDrive\Documents\Profissional\MATLAB\SeismicMatlab\SeismicLab\codes\';
 Dir2='C:\Users\Kenji\OneDrive\Documents\Profissional\MATLAB\SeismicMatlab\SeismicLab\';
 Dir3='C:\Users\Kenji\OneDrive\Documents\Profissional\MATLAB\Kenji_Toolbox\';

 path(path, strcat(Dir1,'bp_filter'));
 path(path, strcat(Dir1,'decon'));
 path(path, strcat(Dir1,'dephasing'));
 path(path, strcat(Dir1,'fx'));
 path(path, strcat(Dir1,'interpolation'));
 path(path, strcat(Dir1,'kl_transform'));
 path(path, strcat(Dir1,'radon_transforms'));
 path(path, strcat(Dir1,'scaling_tapering'));
 path(path, strcat(Dir1,'segy'));
 path(path, strcat(Dir1,'seismic_plots'));
 path(path, strcat(Dir1,'synthetics'));
 path(path, strcat(Dir1,'velan_nmo'));
 path(path, strcat(Dir1,'spectra'));

 path(path, Dir2);
 path(path, strcat(Dir2,'SeismicLab_demos'));
 path(path, strcat(Dir2,'SeismicLab_data'));
 
 path(path, strcat(Dir3,'BSS'));
 path(path, strcat(Dir3,'Deconvolution'));
 path(path, strcat(Dir3,'FastICA_25'));
 path(path, strcat(Dir3,'Misc'));
 path(path, strcat(Dir3,'Robust_SVD'));
 path(path, strcat(Dir3,'Seismic'));
 path(path, strcat(Dir3,'SVD-DOSS'));
 path(path, strcat(Dir3,'SVD-ICA'));
 path(path, strcat(Dir3,'Wavelets'));
