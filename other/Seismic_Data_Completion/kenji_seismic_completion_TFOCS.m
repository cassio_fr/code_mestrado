% It is necessary to add the TFOCS path

load('kenji_seismic.mat')
imshow(d)

missing_data_idx = 79*size(d,1)+ (173:401);
omega = [(1:79*size(d,1)+172) ((79*size(d,1)+402):size(d,1)*size(d,2))];

observations = d(omega);    % the observed entries
mu           = 0.001;        % smoothing parameter

% The solver runs in seconds
tic
Xk = solver_sNuclearBP( {size(d,1),size(d,2),omega}, observations, mu );
toc

%% Media vizinhos
d_orig = d;
d(missing_data_idx) = (d(missing_data_idx-size(d,1)) + d(missing_data_idx+size(d,1)))/2;
figure, imshow(d)
%% Completing by patches

d_patch = d(173:173+10,80-5:80+5);
missing_data_idx = [5*11+(1:11)];
omega = [(1:5*11) ((5*11+12):11*11)];

observations = d_patch(omega);    % the observed entries
mu           = 0.01;        % smoothing parameter

% The solver runs in seconds
tic
Xk = solver_sNuclearBP( {size(d_patch,1),size(d_patch,2),omega}, observations, mu );
toc