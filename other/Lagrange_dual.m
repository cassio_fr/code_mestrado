n = 3;
A = randn(n);
b = randn(n,1);
 
t = 3;

cvx_begin
    variable x_ast(n)
    minimize square_pos(norm(A*x_ast-b))
    subject to 
        norm(x_ast,1) <= t
cvx_end

%% Lagrange dual problem
% Maximize g(lambda) since g is a lower bound for the optimum of the
% original problem.

%lambda = 0.2315:0.00001:0.2325;
lambda = 0:0.2:10;
g = zeros(1,length(lambda));
for k = 1:length(lambda)
    
cvx_begin
    variable x(n)
    minimize square_pos(norm(A*x-b)) + lambda(k)*(norm(x,1)-t)
cvx_end
    
g(k) = cvx_optval;

end