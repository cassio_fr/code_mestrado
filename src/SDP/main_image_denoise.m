D = params.initdict;
Y = params.data;

% params.struct_type = 'hankel';
optmization = 'alm_XY_alternate'; % Options are:
                        % 'alm_XY' : Augmented Lagrange using data matrices X and Y
                        % 'alm_XY_alternate' : Augmented Lagrange using data matrices X and Y 
                        %                      with alternating minimization
                        % 'CVX'    : CVX using only D
                        % 'CVX_XY' : CVX using data matrices X and Y
                        % 'low-rank' : Low-rank approximation of D

if strcmp(params.algo,'low_rank'), optmization = 'low-rank'; end

if (~isfield(params,'struct_type'))
    % Default structure type is Toeplitz (if not specified)
    params.struct_type = 'toeplitz';
end
    
switch lower(params.struct_type)
    case 'toeplitz'
        F1 = diag(ones(1,size(D,1)-1),-1);
        F2 = diag(ones(1,size(D,2)-1),-1)';
%         F2(end,1) = 1; % Using Z1 instead of Z0 (see Takahashi2013)
    case 'hankel'
        F1 = diag(ones(1,size(D,1)-1),-1);
        F2 = diag(ones(1,size(D,2)-1),-1);
%         F2(1,end) = 1; % Using Z1 instead of Z0 (see Takahashi2013)
    case 'vandermonde'
        x = randn(1,params.blocksize^2); x = x/norm(x);% we need to choose a vector. Too restrictive!
        F1 = diag(x); 
        F2 =  diag(ones(1,params.dictsize-1),-1).';
    case 'inv_vandermonde'
        %TODO
    case 'cauchy'
        x = randn(1,params.blocksize^2); x = x/norm(x);% we need to choose a vector. Too restrictive!
        y = randn(1,params.dictsize); y = y/norm(y);% we need to choose a vector. Too restrictive!
        F1 = diag(x);
        F2 = diag(y);
    case 'inv_cauchy'
        %TODO
    otherwise
        error('Invalid structure type specified');
end

%% Sparse Coding parameter setting
CODE_SPARSITY = 1;
CODE_ERROR = 2;

MEM_LOW = 1;
MEM_NORMAL = 2;
MEM_HIGH = 3;

%%%%% parse input parameters %%%%%

ompparams = {'checkdict','off'};

% coding mode %

if (isfield(params,'codemode'))
  switch lower(params.codemode)
    case 'sparsity'
      codemode = CODE_SPARSITY;
      thresh = params.Tdata;
    case 'error'
      codemode = CODE_ERROR;
      thresh = params.Edata;
    otherwise
      error('Invalid coding mode specified');
  end
elseif (isfield(params,'Tdata'))
  codemode = CODE_SPARSITY;
  thresh = params.Tdata;
elseif (isfield(params,'Edata'))
  codemode = CODE_ERROR;
  thresh = params.Edata;

else
  error('Data sparse-coding target not specified');
end


% max number of atoms %

if (codemode==CODE_ERROR && isfield(params,'maxatoms'))
  ompparams{end+1} = 'maxatoms';
  ompparams{end+1} = params.maxatoms;
end


% memory usage %

if (isfield(params,'memusage'))
  switch lower(params.memusage)
    case 'low'
      memusage = MEM_LOW;
    case 'normal'
      memusage = MEM_NORMAL;
    case 'high'
      memusage = MEM_HIGH;
    otherwise
      error('Invalid memory usage mode');
  end
else
  memusage = MEM_NORMAL;
end


% iteration count %

if (isfield(params,'iternum'))
  iternum = params.iternum;
else
  iternum = 10;
end


% omp function %

if (codemode == CODE_SPARSITY)
  ompfunc = @omp;
else
  ompfunc = @omp2;
end


% data norms %

XtX = []; XtXg = [];
if (codemode==CODE_ERROR && memusage==MEM_HIGH)
  XtX = sum(Y.^2);
end

err = zeros(1,iternum);
gerr = zeros(1,iternum);

if (codemode == CODE_SPARSITY)
  errstr = 'RMSE';
else
  errstr = 'mean atomnum';
end

%% Sparsecoding
G = [];
if (memusage >= MEM_NORMAL)
    G = D'*D;
end

if (memusage < MEM_HIGH)
  X = ompfunc(D,Y,G,thresh,ompparams{:});

else  % memusage is high

  if (codemode == CODE_SPARSITY)
    X = ompfunc(D'*Y,G,thresh,ompparams{:});

  else
    X = ompfunc(D'*Y,XtX,G,thresh,ompparams{:});
  end

end            
            
%% Optimization
switch lower(optmization)
    case 'cvx_xy'
        %% Solve via CVX using data Y
        X = X(:,1:40:end); % 256x1000 é o máximo que o servidor (noturno) aguenta
        Y = Y(:,1:40:end); % 64x1000 é o máximo que o servidor (noturno) aguenta

        % Normalization coefficient for Nuclear Norm
        g3_max = norm(Y); % max singular value
        g3 = 150*g3_max; % 50 para disp_rank 36; 150 para 16;

        cvx_begin
            variable De(size(D))
            minimize 0.5*square_pos(norm(De*X-Y,'fro')) + g3*norm_nuc(De - F1*De*F2)
        cvx_end
        D = normc(De);
        
    case 'alm_xy'
        lambda = params.alpha*norm(Y); % 450*norm(Y) para rank 21. 1000*norm(Y) para rank 14
                              % 3000*norm(Y) para rank 6
        %% Ajuste automatico para step size and u
        step = 1e-10/norm(D);
        u = 1e-10;
        
        Z = (D - F1*D*F2)/u; 
        D_hat = zeros(size(D));
        found = false;
        
        iter = 0;
        clear lbound rbound
        while ~found
            % Min dispD
            [U, S, V] = svd(D_hat - F1*D_hat*F2 - Z ,'econ');
            diagS = diag(S);
            svp = length(find(diagS > lambda/u));
            temp_dispD = U(:, 1:svp) * diag(diagS(1:svp) - lambda/u) * V(:, 1:svp)';
            % Min D_hat
            grad1 = D_hat - F1*D_hat*F2 - temp_dispD - Z + F1'*(temp_dispD - D_hat + F1*D_hat*F2 + Z)*F2';
            grad2 = (D_hat*X - Y)*X.';
            norm1 = norm(step*(u*grad1 + grad2), 'fro');
            D_hat = D_hat - step*(u*grad1 + grad2);
            grad1 = D_hat - F1*D_hat*F2 - temp_dispD - Z + F1'*(temp_dispD - D_hat + F1*D_hat*F2 + Z)*F2';
            grad2 = (D_hat*X - Y)*X.';
            norm2 = norm(step*(u*grad1 + grad2), 'fro');

            if norm2 < norm1 % Converges
               lbound = step;
               if exist('rbound','var')
                   step = (lbound + rbound)/2;
               else
                   step = step*1e3;
               end
            else % Diverges
                rbound = step;
                if exist('lbound','var')
                   step = (lbound + rbound)/2;
                else
                   step = step/1e3;
                end
            end

            if exist('lbound','var') && exist('rbound','var') 
                if (rbound - lbound)/lbound < 1e-6
                    found = true;
                    step = step/8;
                end
            end
            iter = iter + 1;
        end
        step

        found = false;
        iter = 0;
        clear lbound rbound
        while ~found  
            Z = (D - F1*D*F2)/u;
            % Min dispD
            [U, S, V] = svd(D_hat - F1*D_hat*F2 - Z ,'econ');
            diagS = diag(S);
            svp = length(find(diagS > lambda/u));
            dispD = U(:, 1:svp) * diag(diagS(1:svp) - lambda/u) * V(:, 1:svp)';
            % Min D_hat
            grad1 = D_hat - F1*D_hat*F2 - temp_dispD - Z + F1'*(temp_dispD - D_hat + F1*D_hat*F2 + Z)*F2';
            grad2 = (D_hat*X - Y)*X.';
            D_hat = D_hat - step*(u*grad1 + grad2);

            Z = Z + dispD - D_hat + F1*D_hat*F2;

            norm1 = norm(dispD - D_hat + F1*D_hat*F2, 'fro');

            % Min dispD
            [U, S, V] = svd(D_hat - F1*D_hat*F2 - Z ,'econ');
            diagS = diag(S);
            svp = length(find(diagS > lambda/u));
            dispD = U(:, 1:svp) * diag(diagS(1:svp) - lambda/u) * V(:, 1:svp)';
            % Min D_hat
            grad1 = D_hat - F1*D_hat*F2 - temp_dispD - Z + F1'*(temp_dispD - D_hat + F1*D_hat*F2 + Z)*F2';
            grad2 = (D_hat*X - Y)*X.';
            D_hat = D_hat - step*(u*grad1 + grad2);    

            norm2 = norm(dispD - D_hat + F1*D_hat*F2, 'fro');

            if norm2 < norm1 % Converges
               lbound = u;
               if exist('rbound','var')
                   u = (lbound + rbound)/2;
               else
                   u = u*1e3;
               end
            else % Diverges
                rbound = u;
                if exist('lbound','var')
                   u = (lbound + rbound)/2;
                else
                   u = u/1e3;
                end
            end

            if exist('lbound','var') && exist('rbound','var') 
                if (rbound - lbound)/lbound < 1e-6
                    found = true;
                    u = u/2;
                end
            end

            iter = iter+1;
        end
        u      
                  
        %% Inexact ALM
        % mu_bar = u*1e7;
        % rho = 1.5      
        Z = (D - F1*D*F2)/u; 

        D_hat = zeros(size(D));
        tol = 1e-7*norm(D,'fro');
        converged = false;
        iter = 1;
        tic
        while ~converged
        % for k = 1:20
        %     primal_converged = false;
        %     while primal_converged == false

                % Min dispD
                [U, S, V] = svd(D_hat - F1*D_hat*F2 - Z ,'econ');
                diagS = diag(S);
                svp = length(find(diagS > lambda/u));
                temp_dispD = U(:, 1:svp) * diag(diagS(1:svp) - lambda/u) * V(:, 1:svp)';
                % Min D_hat
        %         grad_converged = false;
        %         while ~grad_converged
                    grad1 = D_hat - F1*D_hat*F2 - temp_dispD - Z + F1'*(temp_dispD - D_hat + F1*D_hat*F2 + Z)*F2';
                    grad2 = (D_hat*X - Y)*X.';
                    temp_D_hat = D_hat - step*(u*grad1 + grad2);
        %             if norm(D_hat - temp_D_hat, 'fro') < tol
        %                 grad_converged = true;
        %             end
        %             norm(D_hat - temp_D_hat, 'fro')
                    D_hat = temp_D_hat;
        %         end

        %         if norm(D_hat - temp_D_hat, 'fro') < tol && norm(dispD - temp_dispD, 'fro') < tol
        %             primal_converged = true;
        %         end
                dispD = temp_dispD;
        %     end

            temp_Z = Z + dispD - D_hat + F1*D_hat*F2;

            % stop Criterion    
            if norm(temp_Z - Z, 'fro') < tol
                converged = true;
                disp(['Total nº of iterations: ' num2str(iter)]);
            end

%             norm(temp_Z - Z, 'fro')
            Z = temp_Z;

        %     u = min(u*rho, mu_bar);
            iter = iter+1;
            if iter > 1e5, error('ALM did not converge, probably due to bad parameter setting. Retrying...'),end
        end
        toc
        
        % Results
%         fprintf('\n=== RESULTS ===\n')
%         fprintf('||D - D_hat||_F   normalized      : %1.3f\n', norm(D-D_hat,'fro')/norm(D,'fro'));
%         fprintf('||Y - D_hat*X||_F normalized      : %1.3f\n', norm(Y-D_hat*X,'fro')/norm(Y,'fro'));
%         fprintf('||disp(D_hat)||_* / ||disp(D)||_* : %1.1f / %1.1f\n', sum(svd(D_hat - F1*D_hat*F2)), sum(svd(D - F1*D*F2)));
%         fprintf('rank(disp(D_hat)) / rank(disp(D)) : %d / %d\n', rank(D_hat - F1*D_hat*F2,1e-6),rank(D - F1*D*F2,1e-6));
        D = normc(D_hat);
        
    case 'alm_xy_alternate'
        lambda = params.alpha*norm(Y); % 450*norm(Y) para rank 21. 1000*norm(Y) para rank 14
                              % 3000*norm(Y) para rank 6
        %% Ajuste automatico para step size and u
        step = 1e-10/norm(D);
        u = 1e-10;
        
        found = false;
        
        iter = 0;
        clear lbound rbound
        while ~found
            Z = (D - F1*D*F2)/u; 
            D_hat = zeros(size(D));
            % Min dispD
            [U, S, V] = svd(D_hat - F1*D_hat*F2 - Z ,'econ');
            diagS = diag(S);
            svp = length(find(diagS > lambda/u));
            temp_dispD = U(:, 1:svp) * diag(diagS(1:svp) - lambda/u) * V(:, 1:svp)';
            % Min D_hat
            grad1 = D_hat - F1*D_hat*F2 - temp_dispD - Z + F1'*(temp_dispD - D_hat + F1*D_hat*F2 + Z)*F2';
            grad2 = (D_hat*X - Y)*X.';
            norm1 = norm(step*(u*grad1 + grad2), 'fro');
            D_hat = D_hat - step*(u*grad1 + grad2);
            grad1 = D_hat - F1*D_hat*F2 - temp_dispD - Z + F1'*(temp_dispD - D_hat + F1*D_hat*F2 + Z)*F2';
            grad2 = (D_hat*X - Y)*X.';
            norm2 = norm(step*(u*grad1 + grad2), 'fro');

            if norm2 < norm1 % Converges
               lbound = step;
               if exist('rbound','var')
                   step = (lbound + rbound)/2;
               else
                   step = step*1e3;
               end
            else % Diverges
                rbound = step;
                if exist('lbound','var')
                   step = (lbound + rbound)/2;
                else
                   step = step/1e3;
                end
            end

            if exist('lbound','var') && exist('rbound','var') 
                if (rbound - lbound)/lbound < 1e-6
                    found = true;
                    if params.sigma > 50
                        step = step/4;
                    else
                        step = step/3*(trainnum/40000);
                    end
                end
            end
            iter = iter + 1;
        end
        step

%         found = false;
%         iter = 0;
%         clear lbound rbound
%         while ~found  
%             Z = (D - F1*D*F2)/u;
%             D_hat = randn(size(D)); %D;
%             % Min dispD
%             [U, S, V] = svd(D_hat - F1*D_hat*F2 - Z ,'econ');
%             diagS = diag(S);
%             svp = length(find(diagS > lambda/u));
%             temp_dispD = U(:, 1:svp) * diag(diagS(1:svp) - lambda/u) * V(:, 1:svp)';
%             % Min D_hat
%             grad1 = D_hat - F1*D_hat*F2 - temp_dispD - Z + F1'*(temp_dispD - D_hat + F1*D_hat*F2 + Z)*F2';
%             grad2 = (D_hat*X - Y)*X.';
%             D_hat = D_hat - step*(u*grad1 + grad2);
% 
%             Z = Z + temp_dispD - D_hat + F1*D_hat*F2;
% 
%             norm1 = norm(temp_dispD - D_hat + F1*D_hat*F2, 'fro');
% 
%             % Min dispD
%             [U, S, V] = svd(D_hat - F1*D_hat*F2 - Z ,'econ');
%             diagS = diag(S);
%             svp = length(find(diagS > lambda/u));
%             temp_dispD = U(:, 1:svp) * diag(diagS(1:svp) - lambda/u) * V(:, 1:svp)';
%             % Min D_hat
%             grad1 = D_hat - F1*D_hat*F2 - temp_dispD - Z + F1'*(temp_dispD - D_hat + F1*D_hat*F2 + Z)*F2';
%             grad2 = (D_hat*X - Y)*X.';
%             D_hat = D_hat - step*(u*grad1 + grad2);    
% 
%             norm2 = norm(temp_dispD - D_hat + F1*D_hat*F2, 'fro');
% 
%             if norm2 < norm1 % Converges
%                lbound = u;
%                if exist('rbound','var')
%                    u = (lbound + rbound)/2;
%                else
%                    u = u*1e3;
%                end
%             else % Diverges
%                 rbound = u;
%                 if exist('lbound','var')
%                    u = (lbound + rbound)/2;
%                 else
%                    u = u/1e3;
%                 end
%             end
% 
%             if exist('lbound','var') && exist('rbound','var') 
%                 if (rbound - lbound)/lbound < 1e-6
%                     found = true;
%                     u = u/2;
%                 end
%             end
% 
%             iter = iter+1;
%         end
%         u      

        u = 1e7;
       
        %% Alternating
        success = false;
        while ~success
        try
            Z = (D - F1*D*F2)/u;  % It is faster if  these variables are not reseted every iteration
            D_hat = zeros(size(D));
            for k = 1:iternum
                %% Sparsecoding
                G = [];
                if (memusage >= MEM_NORMAL)
                    G = D'*D;
                end

                if (memusage < MEM_HIGH)
                  X = ompfunc(D,Y,G,thresh,ompparams{:});

                else  % memusage is high

                  if (codemode == CODE_SPARSITY)
                    X = ompfunc(D'*Y,G,thresh,ompparams{:});

                  else
                    X = ompfunc(D'*Y,XtX,G,thresh,ompparams{:});
                  end

                end            

                %% Inexact ALM
                % mu_bar = u*1e7;
                % rho = 1.5      
    %             Z = (D - F1*D*F2)/u; 

    %             D_hat = zeros(size(D));
                tol = 1e-4*norm(D,'fro'); % AQUI!!! ORIGINAL: 1e-7
                if k == iternum, tol = 1e-7*norm(D,'fro'); end
                converged = false;
                iter = 1;
                tic
                while ~converged
                % for k = 1:20
                %     primal_converged = false;
                %     while primal_converged == false

                        % Min dispD
                        [U, S, V] = svd(D_hat - F1*D_hat*F2 - Z ,'econ');
                        diagS = diag(S);
                        svp = length(find(diagS > lambda/u));
                        temp_dispD = U(:, 1:svp) * diag(diagS(1:svp) - lambda/u) * V(:, 1:svp)';
                        % Min D_hat
                %         grad_converged = false;
                %         while ~grad_converged
                            grad1 = D_hat - F1*D_hat*F2 - temp_dispD - Z + F1'*(temp_dispD - D_hat + F1*D_hat*F2 + Z)*F2';
                            grad2 = (D_hat*X - Y)*X.';
                            temp_D_hat = D_hat - step*(u*grad1 + grad2);
                %             if norm(D_hat - temp_D_hat, 'fro') < tol
                %                 grad_converged = true;
                %             end
                %             norm(D_hat - temp_D_hat, 'fro')
                            D_hat = temp_D_hat;
                %         end

                %         if norm(D_hat - temp_D_hat, 'fro') < tol && norm(dispD - temp_dispD, 'fro') < tol
                %             primal_converged = true;
                %         end
                        dispD = temp_dispD;
                %     end

                    temp_Z = Z + dispD - D_hat + F1*D_hat*F2;

                    % stop Criterion    
                    if norm(temp_Z - Z, 'fro') < tol
                        converged = true;
                        disp(['Total nº of iterations: ' num2str(iter)]);
                    end

    %                 norm(temp_Z - Z, 'fro')
                    Z = temp_Z;

                %     u = min(u*rho, mu_bar);
                    iter = iter+1;
                    if iter > 1e6, error('ALM did not converge, probably due to bad parameter setting. Retrying...'),end
                end
                toc

            % Results
    %         fprintf('\n=== RESULTS ===\n')
    %         fprintf('||D - D_hat||_F   normalized      : %1.3f\n', norm(D-D_hat,'fro')/norm(D,'fro'));
    %         fprintf('||Y - D_hat*X||_F normalized      : %1.3f\n', norm(Y-D_hat*X,'fro')/norm(Y,'fro'));
    %         fprintf('||disp(D_hat)||_* / ||disp(D)||_* : %1.1f / %1.1f\n', sum(svd(D_hat - F1*D_hat*F2)), sum(svd(D - F1*D*F2)));
    %         fprintf('rank(disp(D_hat)) / rank(disp(D)) : %d / %d\n', rank(D_hat - F1*D_hat*F2,1e-6),rank(D - F1*D*F2,1e-6));
            Dict_improvement = norm(normc(D_hat) - D)
            D = normc(D_hat);
            
            end    
            success = true;
        catch err
            disp(err.message); disp('Trying smaller step.')
            step = step/2
        end 
        end
        
    case 'cvx'
        %% Solve via CVX using only D
        % Normalization coefficient for L1 norm
        [D, X] = ksvd(params,verbose,msgdelta);
        g2_max = norm(D(:),inf); % max absolute row sum
        g2 = 0.15*g2_max;

        % Normalization coefficient for Nuclear Norm
        g3_max = norm(D); % max singular value
        g3 = 0.15*g3_max; % 0.7 for disp_rank=12 ; 0.45 for 22 ; 0.25 for 32; 0.15 for 42

        cvx_begin
            variable De(size(D))
            minimize 0.5*square_pos(norm(De-D,'fro')) + g3*norm_nuc(De - F1*De*F2)
        cvx_end

        cvx_begin
            cvx_precision low
            variables De(size(D)) Noise(size(D))
            minimize 0.5*square_pos(norm(Noise,'fro')) + g3*norm_nuc(De - F1*De*F2)
            subject to
               Noise + De == D;
        cvx_end

        % De_disp = De - F1*De*F2; % Displacement matriz associated with De
        % De_disp = svd_low_rank_approx(De_disp,rank(De_disp,1e-6)); % Rounding to zero the very small singular values
        % De = recover(De_disp, F1,0,size(D,1),F2,0,size(D,2));
        D = normc(De);
        
    case 'proj'
        %% Succesive projections
        % To approximate D directly
        [D, X] = ksvd(params,verbose,msgdelta);
        params.disp_rank = 20;

        if isfield(params,'disp_rank')
            if ~isnumeric(params.disp_rank)
                error('disp_rank parameter must be an integer')
            end
            disp_rank = round(params.disp_rank);
        else
            disp_rank = 20;
        end

        D_hat = D;
        itr = 1;
        while 1
            itr
            disp_D = stein_displacement_matrix(D_hat,F1,F2);
            disp_D = svd_low_rank_approx(disp_D,disp_rank);
            D_hat = recover(disp_D, F1,0,size(D,1),F2,0,size(D,2));
            D_hat = normc(D_hat);

            % Teste de convergência - criterio de parada
            singular_val = svd(stein_displacement_matrix(D_hat,F1,F2));
            if singular_val(disp_rank)> 1e7*singular_val(disp_rank+1)
                break
            end
            itr = itr+1;
        end
        D = D_hat;        
        
    case 'low-rank'
        [D, X] = ksvd(params,verbose,msgdelta);
        Y = params.data;
        rankD = params.alpha;
        
        D_hat = svd_low_rank_approx(D,rankD);
        D = normc(D_hat);
    case 'low-rank_xy'

        % Normalization coefficient for Nuclear Norm
        lambda = params.alpha*norm(Y); % 50 para disp_rank 36; 150 para 16;
        step = 3e-11;
        
        %% Ajuste automatico para step size    
        found = false;
        
        iter = 0;
        clear lbound rbound
        D_hat = D;
        while ~found
            grad = (D_hat*X - Y)*X.';
            [U, S, V] = svd(D_hat - step*grad,'econ');
            diagS = diag(S);
            svp = length(find(diagS > step*lambda));
            temp_D_hat = U(:, 1:svp) * diag(diagS(1:svp) - step*lambda) * V(:, 1:svp)';
            norm1 = norm(D_hat - temp_D_hat, 'fro');
            D_hat = temp_D_hat;
            
            grad = (D_hat*X - Y)*X.';
            [U, S, V] = svd(D_hat - step*grad,'econ');
            diagS = diag(S);
            svp = length(find(diagS > step*lambda));
            temp_D_hat = U(:, 1:svp) * diag(diagS(1:svp) - step*lambda) * V(:, 1:svp)';
            norm2 = norm(D_hat - temp_D_hat, 'fro');
            
            if norm2 < norm1 % Converges
               lbound = step;
               if exist('rbound','var')
                   step = (lbound + rbound)/2;
               else
                   step = step*1e3;
               end
            else % Diverges
                rbound = step;
                if exist('lbound','var')
                   step = (lbound + rbound)/2;
                else
                   step = step/1e3;
                end
            end

            if exist('lbound','var') && exist('rbound','var') 
                if (rbound - lbound)/lbound < 1e-6
                    found = true;
                    if params.sigma > 50
                        step = step/4;
                    else
                        step = step/10*(trainnum/40000);
                    end
                end
            end
            iter = iter + 1;
        end
        step
       
        %% Alternating
        for k = 1:iternum
            tol = 1e-4*norm(D,'fro');
            if k == iternum, tol = 1e-6*norm(D,'fro'); end
            %% Sparsecoding
            G = [];
            if (memusage >= MEM_NORMAL)
                G = D'*D;
            end

            if (memusage < MEM_HIGH)
              X = ompfunc(D,Y,G,thresh,ompparams{:});

            else  % memusage is high

              if (codemode == CODE_SPARSITY)
                X = ompfunc(D'*Y,G,thresh,ompparams{:});

              else
                X = ompfunc(D'*Y,XtX,G,thresh,ompparams{:});
              end

            end            

            %% Proximal Gradient Descent
            D_hat = D;
            iter = 1;
            grad_converged = false;
            tic
            success = false;
            while ~success
            try
                while ~grad_converged
                    grad = (D_hat*X - Y)*X.';
                    [U, S, V] = svd(D_hat - step*grad,'econ');
                    diagS = diag(S);
                    svp = length(find(diagS > step*lambda));
                    temp_D_hat = U(:, 1:svp) * diag(diagS(1:svp) - step*lambda) * V(:, 1:svp)';
                    if norm(D_hat - temp_D_hat, 'fro') < tol
                        grad_converged = true;
                        disp([num2str(k) '/' num2str(iternum) ' Total nº of iterations: ' num2str(iter)]);
                    end
%                     norm(D_hat - temp_D_hat, 'fro')
                    D_hat = temp_D_hat;
                    iter = iter+1;
                end
                success = true;
            catch err
                disp(err.message); disp('Trying smaller step.')
                step = step/2
                D_hat = D;
            end
            end
            toc

            Dict_improvement = norm(normc(D_hat) - D)
            D = normc(D_hat);
        end
        
end