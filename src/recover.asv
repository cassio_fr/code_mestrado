% Returns the recovered matrix X from its disclament matrix:
%   [X] = recover(Delta,A,a,p,B,b,q)
%
%   - INPUT ARGUMENTS
%   Delta: stein-type displacement matrix generated from X
%   A: left operator matrix on stein displacement (Delta = X - A*X*B)
%   a: A is a-potent of order p. If this is not the case, set a=0
%   p: A is a-potent of order p. If this is not the case, set p=0
%   B: right operator matrix on stein displacement
%   b: B is b-potent of order q. If this is not the case, set b=0
%   q: B is b-potent of order q. If this is not the case, set q=0
%   
%   /\
%   /!\ A or B (or both) have to be k-potent for the reconstruction to work
function [X] = recover(Delta,A,a,p,B,b,q)

if(b == q == 0) % A is a-potent of order p
    X  = zeros(size(Delta));
    for k = 0:p-1
        X = X + (A^k)*Delta*(B^k);
    end
    X = (X)*inv(eye(size(B))- a*B^p); % the inverse could be precalculated
                                      % for a specific B.
else % B is b-potent of order q
     % or maybe both are k-potent. In this case, both reconstructions apply
    X  = zeros(size(Delta));
    for k = 0:q-1
        X = X + (A^k)*Delta*(B^k);
    end
    X = inv(eye(size(A))- b*A^q)*(X);
end
end