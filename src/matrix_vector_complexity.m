m = 8; % m>=n
n = 5;
disp_rank = 2;

X = eye(n,m);
for k = 2:2:disp_rank
    X = X*toeplitz([1 randn(1,m-1)],[1 randn(1,m-1)]);
end

%% Reconstrução usando Z0, funciona com matrizes retangulares também.
%  Porém técnica de multiplicação rápida proposta em Gohberg1994 não pode
%  ser aplicada com Z0

%================== TOEPLITZ ==================%
F1 = diag(ones(1,n-1),-1); %Z0
F2 = diag(ones(1,m-1),1);  %Z0'

dispX = stein_displacement_matrix(X,F1,F2);
disp_rank = rank(dispX);

% Only for exact toeplitz matrix
G = [ [1 ; zeros(n-1,1)] X(:,1)];
H = [ [0 X(1,2:end)].'  [1 ; zeros(m-1,1)]];

% SVD low rank
[G, S, V] = svd(dispX);
G = G(:,1:disp_rank);
H = (S(1:disp_rank,1:disp_rank)*(V(:,1:disp_rank)).').';

% Works only for toeplitz with Z0 matrix on the displacement
soma = zeros(size(X));
for k = 1:disp_rank
    L = tril(gallery('circul',G(:,k)).');
    R = triu(gallery('circul',H(:,k)));
    soma = soma + L*R(1:n,:);
end
soma-X

%================== HANKEL ==================%
F1 = diag(ones(1,n-1),-1); %Z0
F2 = diag(ones(1,m-1),-1); %Z0

X = fliplr(X);

dispX = stein_displacement_matrix(X,F1,F2);
disp_rank = rank(dispX);

% Only for exact hankel matrix
G = [ [1 ; zeros(n-1,1)] X(:,end)];
H = [ [X(1,1:end-1) 0].'  [zeros(m-1,1); 1]];

% SVD low rank
[G, S, V] = svd(dispX);
G = G(:,1:disp_rank);
H = (S(1:disp_rank,1:disp_rank)*(V(:,1:disp_rank)).').';

% Works only for Hankel with Z0 matrix on the displacement
soma = zeros(size(X));
for k = 1:disp_rank
    L = tril(gallery('circul',G(:,k)).');
    R = triu(gallery('circul',H(end:-1:1,k)));
    soma = soma + L*R(1:n,:);
end
soma = fliplr(soma);
soma-X

%% Reconstrução com Z0 invertida (Kailath79), sum(UL)
F1 = diag(ones(1,n-1),1); %Z0'
F2 = diag(ones(1,m-1),-1);  %Z0

dispX = stein_displacement_matrix(X,F1,F2);
disp_rank = rank(dispX);

L = zeros(m,m,disp_rank);
R = zeros(n,n,disp_rank);

% Only for exact toeplitz matrix
G = [ [zeros(n-1,1); 1] X(:,end)];
H = [ [X(end,1:end-1) 0].'  [zeros(m-1,1); 1]];

% SVD low rank
[G, S, V] = svd(dispX);
G = G(:,1:disp_rank);
H = (S(1:disp_rank,1:disp_rank)*(V(:,1:disp_rank)).').';

% Works only for toeplitz with Z0 matrix on the displacement
soma = zeros(size(X));
for k = 1:disp_rank
    R(:,:,k) = triu(gallery('circul',G(end:-1:1,k)));
    L(:,:,k) = tril(gallery('circul',H(end:-1:1,k)).');
    soma = soma + R(:,:,k)*L(end-n+1:end,:,k);
end
soma-X

% Completing with zeros below (not useful)
L = zeros(m,m,disp_rank);
R = zeros(m,m,disp_rank);
% Only for exact toeplitz matrix
G = [ [zeros(n-1,1); 1] X(:,end)]; G = [G; zeros(m-n,2)];
H = [ [X(end,1:end-1) 0].'  [zeros(m-1,1); 1]];
% SVD low rank
[G, S, V] = svd(dispX);
G = G(:,1:disp_rank);G = [G; zeros(m-n,2)];
H = (S(1:disp_rank,1:disp_rank)*(V(:,1:disp_rank)).').';
% Works only for toeplitz with Z0 matrix on the displacement
soma = zeros(m,m);
for k = 1:disp_rank
    R(:,:,k) = triu(gallery('circul',G(end:-1:1,k)));
    L(:,:,k) = tril(gallery('circul',H(end:-1:1,k)).');
    soma = soma + R(:,:,k)*L(:,:,k);
end
soma(1:n,:)-X

%% Usando Z1 - necessário para técnica descrita em Gohberg1994
%  Porém não funciona com matrizes retangulares. 
%  Pode-se completar, como abaixo, mas apenas em toeplitz perfeitas
% if m>n, X = toeplitz([X(:,1); zeros(m-n,1)],X(1,:)); n = m; end


% pois precisa usar circ_{1/phi} então não pode usar Z0 (onde phi = 0)
F1 = diag(ones(1,n-1),-1); F1(1,end) = 1; %Z1, phi = 1
F2 = diag(ones(1,m-1),1); F2(end,1) = 1; %Z1'

dispX = stein_displacement_matrix(X,F1,F2);
disp_rank = rank(dispX);

% Only for Exact toeplitz matrix
G = [[1; zeros(n-1,1)] dispX(:,1) ];
H =  [ [0 dispX(1,2:end)].'  [1 ; zeros(m-1,1)]];

% SVD low rank
[G, S, V] = svd(dispX);
G = G(:,1:disp_rank);
H = (S(1:disp_rank,1:disp_rank)*(V(:,1:disp_rank)).').';

% Verifying eq 1.4 page 6, para phi = 1
soma = zeros(size(X));
L = zeros(n,n,disp_rank);
R = zeros(n,m,disp_rank);
for k = 1:disp_rank
   L(:,:,k) = gallery('circul',G(:,k)).';
   R(:,:,k) = (gallery('circul',H(:,k)).').';
   soma =  soma + L(:,:,k)*R(1:n,:,k);
end
soma % should be all zeros

% Works only for toeplitz with Z1 e Z1' on the displacement
% Equation 1.5
% phi = 1 e psi = 1/2
soma2 = zeros(size(X));
circ_lr = gallery('circul',X(:,1)).';
factor_circ = (ones(n,n)) + triu(ones(n,n),1); % 1/psi = 2
for k = 1:disp_rank
   L = (gallery('circul',G(:,k)).').*factor_circ; % circ_{phi}, com phi = 1
   R = ((gallery('circul',H(:,k)).')).'; % circ_{1/psi} onde 1/psi = 2
   soma2 =  soma2 + L*R(1:n,:);
end
soma2 = circ_lr + (1)/(1-2)*soma2;
soma2 - X % should be close to zero


% Equation 1.6
% phi = 1 e psi = 1/2
soma2 = zeros(size(X));
circ_lc = gallery('circul',[X(end,end); X(1:end-1,end)]).';
factor_circ = (ones(m,m)) + triu(ones(m,m),1); % 1/psi = 2
for k = 1:disp_rank
   L = gallery('circul',G(:,k)).'; % circ_{phi}, com phi = 1
   R = ((gallery('circul',H(:,k)).').*factor_circ).'; % circ_{1/psi} onde 1/psi = 2
   soma2 =  soma2 + L*R(1:n,:);
end
soma2 = circ_lc + (1/2)/(1/2-1)*soma2;
soma2 - X % should be close to zero

%Equation 1.8 - gerar matriz circular a partir FFT de diagonais
% phi = 1, D_phi = eye(n) (pode ser omitido)
% psi = 1/2
F = dftmtx(n);
sigma_phi = diag(F*X(:,1));
circ_phi = (1/n)*F'*sigma_phi*F;

ksi = (1/2)^(1/n);
D_psi = diag(ksi.^(0:n-1));
D_psi_inv = diag(1./(ksi.^(0:n-1)));
sigma_psi = diag(F*D_psi*X(:,1));
circ_psi = (1/n)*D_psi_inv*F'*sigma_psi*F*D_psi;

%% Equation 1.8b de MOUILLERON2011
m = 8; % m>=n
n = 5;
disp_rank = 2;

X = eye(n,m);
for k = 2:2:disp_rank
    X = X*toeplitz([1 randn(1,m-1)],[1 randn(1,m-1)]);
end

X = fliplr(X); % Converts from Toeplitz to Hankel

F1 = diag(ones(1,n-1),-1); F1(1,end) = 1; %Z1
F2 = diag(ones(1,m-1),1); %Z0'

dispX = sylvester_displacement_matrix(X,F1,F2);

% Only for Exact hankel matrix
G = [[1; zeros(n-1,1)] dispX(:,1) ];
H =  [ [0 dispX(1,2:end)].'  [1 ; zeros(m-1,1)]];

% Reconstruction - INCOMPLETE!
J = fliplr(eye(m)); % X*J = fliplr(X)
soma = zeros(size(X));
for k = 1:disp_rank
    T = toeplitz(G(:,k),[G(1,k); G(end:-1:2,k)]); % gallery('circul',G(:,k))
    soma = soma;
end

%% Multiplização usando reconstrução sum(L*U) para Z0
% D'*x
m = 6;
n = 3;
disp_rank = 1;

L = zeros(m,n,disp_rank);
U = zeros(n,n,disp_rank);
for k = 1:disp_rank
    L_temp = tril(gallery('circul',1:m)); 
    L(:,:,k) = L_temp(:,1:n);
    U(:,:,k) = triu(gallery('circul',1:n));
end
v = (1:n).';

result = 0;
for k = 1:disp_rank
    result = result + L(:,:,k)*U(:,:,k)*v; % result to be reproducted
end

% precalculated
fu = zeros(2*n,disp_rank);
fl1 = zeros(2*n,disp_rank); fl2 = zeros(2*n,disp_rank);
for k = 1:disp_rank
    fu(:,k) = fft(U(1,end:-1:1,k),2*n).';  % pruned n - precalculated
    fl1(:,k) = fft(L(1:n,1,k),2*n);        % pruned n - precalculated
    fl2(:,k) = fft([L(n+1,end:-1:2,k).'; L(n+(1:n),1,k)],2*n);    % not pruned - precalculated
end

% Multiplication
fv = fft(v,2*n);                % pruned n
fx1_sum = zeros(2*n,1);
fx2_sum = zeros(2*n,1);

for k = 1:disp_rank
    fuv = fv.*fu(:,k);

    % first verification
    uv = ifft(fuv);                 % pruned n
    uv = uv(n:2*n-1);
%     uv - U(:,:,k)*v(:,:,k) % should be all zeros

    fuv_cleaned = fft(uv,2*n);       % pruned n
    fx1 = fl1(:,k).*fuv_cleaned;
    fx2 = fl2(:,k).*fuv_cleaned;

    % second verification
%     x1 = ifft(fx1);                
%     x2 = ifft(fx2);                
%     [x1(1:n); x2(n:2*n-1)] - L(:,:,k)*U(:,:,k)*v

    fx1_sum = fx1_sum + fx1;
    fx2_sum = fx2_sum + fx2;

end

x1 = ifft(fx1_sum);                 % pruned n
x2 = ifft(fx2_sum);                 % pruned n
result2 =  [x1(1:n); x2(n:2*n-1)];
result2 - result

% D*x
m = 6;
n = 3;
disp_rank = 3;

L = zeros(n,n,disp_rank);
U = zeros(n,m,disp_rank);
U_temp = zeros(m,m);
for k = 1:disp_rank
    L(:,:,k) = tril(gallery('circul',1:n));
    U_temp(:,:) = triu(gallery('circul',1:m));
    U(:,:,k) = U_temp(1:n,:);
end
v = (1:m).';

result = 0;
for k = 1:disp_rank
    result = result + L(:,:,k)*U(:,:,k)*v; % result to be reproducted
end


fu1 = zeros(2*n,disp_rank); fu2 = zeros(2*n,disp_rank);
fl  = zeros(2*n,disp_rank);
for k = 1:disp_rank
    fu1(:,k) = fft(U(1,n:-1:1,k),2*n).';        % pruned n - precalculated
    fu2(:,k) = fft([U(1,end:-1:n+1,k).'; U(2:end,n+1,k)],2*n);    % not pruned - precalculated
    fl(:,k) = fft(L(1:end,1),2*n);  % pruned n - precalculated
end

% Multiplication
fv1 = fft(v(1:n),2*n);                % pruned n
fv2 = fft(v(n+1:end),2*n);            % pruned n
fx_sum = zeros(2*n,1);
for k = 1:disp_rank
    fuv1 = fu1(:,k).*fv1;
    fuv2 = fu2(:,k).*fv2;
    fuv = fuv1+fuv2;

    % first verification
    uv = ifft(fuv);                    % not pruned
    uv = uv(n:2*n-1);
%     uv - U*v % should be all zeros

    fuv_cleaned = fft(uv,2*n);         % pruned n
    fx = fl(:,k).*fuv_cleaned;

    % second verification
%     x = ifft(fx);
%     x - L(:,:,k)*U(:,:,k)*v
    fx_sum = fx_sum + fx;
end
result2 = ifft(fx_sum);                % pruned n
result2(1:n) - result