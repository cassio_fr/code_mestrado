% Demo from TFOCS

N   = 16;       % the matrix is N x N
r   = 2;        % the rank of the matrix
df  = 2*N*r - r^2;  % degrees of freedom of a N x N rank r matrix
nSamples    = 3*df; % number of observed entries

% For this demo, we will use a matrix with integer entries
% because it will make displaying the matrix easier.
iMax    = 5;
X       = randi(iMax,N,r)*randi(iMax,r,N); % Our target matrix


% Now suppose we only see a few entries of X. Let �Omega� be the set of observed entries

rPerm   = randperm(N^2); % use "randsample" if you have the stats toolbox
omega   = sort( rPerm(1:nSamples) );

% Print out the observed matrix in a nice format. The �NaN� entries represent unobserved values. The goal of this demo is to find out what those values are!

Y = nan(N);
Y(omega) = X(omega);
disp('The "NaN" entries represent unobserved values');
disp(Y)

%% MATRIX COMPLETION VIA TFOCS

% Add TFOCS to your path (modify this line appropriately):
addpath D:\Cassio\source\packages\TFOCS-1.3.1\

observations = X(omega);    % the observed entries
mu           = .001;        % smoothing parameter

% The solver runs in seconds
tic
Xk = solver_sNuclearBP( {N,N,omega}, observations, mu );
toc

%% MATRIX COMPLETION MATLABCENTRAL
% D� erro por causa dos NaN. N�o sei usar

[U S V] = svd(Xk);
nuclear_norm = sum(diag(S));

X2k = FindNuclearNormApprox(Y,nuclear_norm*2);
