mean_mc = zeros(1,20);
var_mc = zeros(1,20);

n = round(logspace(1,3,length(mean_mc)));

for jsize = 1:length(n)

    mc = zeros(1,1e4);
    tic
    for k = 1:length(mc)
        X = randn(n(jsize));
        mc(k) = mutual_coherence(X);
    end
    toc

    mean_mc(jsize) = mean(mc);
    var_mc(jsize) = var(mc);

end