% Returns Sylvester-type displacement operator with operator matrices A and B
function [Nabla_AB_X] = sylvester_displacement_matrix(X,A,B)
Nabla_AB_X = A*X - X*B;
end