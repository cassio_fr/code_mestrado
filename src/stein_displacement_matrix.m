% Returns Stein-type displacement operator with operator matrices A and B
function [Delta_AB_X] = stein_displacement_matrix(X,A,B)
Delta_AB_X = X - A*X*B;
end