% [approx_X, approx_error] = svd_low_rank_approx(X,max_rank)
%
% Returns the optimal low rank approximation of matrix X with maximal rank
% max_rank.
function [approx_X, approx_error] = svd_low_rank_approx(X,max_rank)

% SVD decomposition
[U S V] = svd(X);

% Intitial norm for renormalization
normX = sqrt(sum(diag(S).^2));
new_norm = sqrt(sum(diag(S(1:max_rank,1:max_rank)).^2));

% Only the max_rank highest singular values are kept
approx_X = U(:,1:max_rank)*S(1:max_rank,1:max_rank)*V(:,1:max_rank)';

% Renormalization TO BE CONFIRMED!!!
% approx_X = approx_X*normX/new_norm;

% Calculating the approximation error
diff = X - approx_X;
approx_error = sum(diff(:).^2)/sum(X(:).^2);
end