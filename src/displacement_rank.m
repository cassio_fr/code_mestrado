% Returns the displacement rank of matrix X with respect of F.
function [disp_rank] = displacement_rank(X,F)
disp_rank = rank(displacement_matrix(X,F));
end