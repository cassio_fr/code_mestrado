%% Gradient method for minimizing the following problem
% argmin{De}  1/2* ||dispD - De + F1*De*F2 ||_F ^2

% D = randn(10,10)
D = toeplitz(1:50);
F1 = diag(ones(size(D,1)-1,1),-1);
F2 = F1';
dispD = D - F1*D*F2;

De = zeros(size(D));
mu = 0.50; %Stepsize: seems to be the maximum

for k = 1:10000
   grad = De - F1*De*F2 - dispD  + F1'*(dispD - De + F1*De*F2)*F2';
   De = De - mu*grad; 
end
De