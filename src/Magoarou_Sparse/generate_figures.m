clim = [min(min(abs(D(:))),min(abs(D_hat(:)))) max(max(abs(D(:))),max(abs(D_hat(:))))];

    % % Same plot
    % subplot(1,2,1), imagesc(D,clim)
    % subplot(1,2,2), imagesc(D_hat,clim)

    % Different figures
    figure, imagesc(abs(D),clim)
    figure, imagesc(abs(D_hat),clim)

    figure, imagesc(abs(D-D_hat),clim)

    % Generate animated GIF
    if flag_gif
        imagesc(abs(D),clim);
        f = getframe;
        [im,map] = rgb2ind(f.cdata,512,'nodither');

        imagesc(abs(D_hat),clim);
        f = getframe;
        im(:,:,1,2) = rgb2ind(f.cdata,map,'nodither');
        cd Results\
        imwrite(im,map,gif_filename,  'LoopCount',inf,'DelayTime',1) 
        cd ..
    end