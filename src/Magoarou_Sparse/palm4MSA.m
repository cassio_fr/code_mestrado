function [lambda A_hat factors rightmost_factor] = palm4MSA(A,J,constraints,init_factors,init_rightmost_factor,N_iter,tol)

if J ~= numel(constraints) || J <=0
    error('Number of factors J must match the number of input constraints'); 
elseif ~iscell(constraints)
    error('Input constraints must be a cell of function handles'); 
end

rightmost_factor = zeros(size(A));
factors = zeros([size(A,1) size(A,1) J-1]);
for k = 1:J-1
    if ~isa(constraints{k},'function_handle')
        error('Input constraints must be a cell of function handles')
    end
    factors(:,:,k) = eye(size(A,1));
end

if nargin >= 4 % Factors initialization has been provided by user
    rightmost_factor = init_rightmost_factor;
    factors = init_factors;
end

% Initialization
if nargin < 6
    N_iter = 3e3;   % Maximum number of iterations (default value)
end
if nargin < 7
    tol = 1e-5;     % Tolerance for declaring convergence (default value)
end
lambda = 1;
normA = norm(A,'fro');
cost_old = intmax;
disp('Iteration:')
for k = 1:N_iter
    % Print status and check for convergence each 1000 iterations
    if(mod(k,1000)==0), 
        fprintf('>>%d',k),
        cost = norm(A-lambda*R,'fro')/normA;
        fprintf(' (mse=%1.3f) ',cost),
        if cost_old - cost < tol, break, end
        cost_old = cost;
    end
    
    % First term might be non-square
    L = eye(size(A,1));
    for l = 1:J-1
        L = factors(:,:,l)*L;
    end
    c = (1+1e-3)*lambda^2*(norm(L)^2);
    rightmost_factor = constraints{1}( rightmost_factor - (lambda/c)*L.'*(lambda*L*rightmost_factor - A) );
%         factors(:,:,1) = factors(:,:,1)/norm(factors(:,:,1),'fro');
    R = rightmost_factor;
    
    % Other terms
    for j = 1:J-1
        L = eye(size(A,1));
        for l = j+1:J-1
            L = factors(:,:,l)*L;
        end
        c = (1+1e-3)*lambda^2*(norm(R)^2)*(norm(L)^2);
        factors(:,:,j) = constraints{j+1}( factors(:,:,j) - (lambda/c)*L.'*(lambda*L*factors(:,:,j)*R - A)*R.' );
%         factors(:,:,j) = factors(:,:,j)/norm(factors(:,:,j),'fro');
        R = factors(:,:,j)*R;
    end
    lambda = trace(A.'*R)/trace(R.'*R);
end
disp('>>end')
A_hat = lambda*R; % The factor lambda does not appear on the original paper
