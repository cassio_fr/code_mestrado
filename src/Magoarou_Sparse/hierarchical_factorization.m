%% HIERARCHICAL FACTORIZATION
function [lambda D_hat S rightmost_S] = hierarchical_factorization(D,J,constraint,constraint_tilde)

S = zeros([size(D,1) size(D,1) J-1]); % J-1 matrices (all factors but the rightmost)
F = zeros([size(D) 2]);
T = D;

% Factorize the residual T into 2 factors
[lambda T_hat F2 F1] = palm4MSA(T,2,{constraint{1} constraint_tilde{1}});
T = lambda*F2;
rightmost_S = F1;
% Global Optimization
[lambda D_hat S(:,:,1) rightmost_S] = palm4MSA(D,2,{constraint{1} constraint_tilde{1}},T,rightmost_S);
T = lambda*S(:,:,1); % The leftmost factor is yet to be factorized

for l = 2:J-1
    % Factorize the residual T into 2 factors
    [lambda T_hat F2 F1] = palm4MSA(T,2,{constraint{l} constraint_tilde{l}});
    T = lambda*F2;
    S(:,:,l-1) = F1;
    % Global Optimization
    initialization = cat(3,S(:,:,1:l-1),T);
    [lambda D_hat S(:,:,1:l) rightmost_S] = palm4MSA(D,l+1,{constraint{1:l} constraint_tilde{l}},initialization,rightmost_S);
    T = lambda*S(:,:,l); % The leftmost factor is yet to be factorized
end


