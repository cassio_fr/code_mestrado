function [lambda A_hat factors] = palm4MSA_l1(A,J,constraints,init_factors,N_iter,tol)

if J ~= numel(constraints) || J <=0
    error('Number of factors J must match the number of input constraints'); 
elseif ~iscell(constraints)
    error('Input constraints must be a cell of function handles'); 
end

factors = zeros([size(A) J]);
for k = 1:J
    if ~isa(constraints{k},'function_handle')
        error('Input constraints must be a cell of function handles')
    end
    factors(:,:,k) = eye(size(A));
end

if nargin >= 4 % Factors initialization has been provided by user
    factors = init_factors;
end

% Initialization
if nargin < 5
    N_iter = 1e4;   % Maximum number of iterations (default value)
end
if nargin < 6
    tol = 1e-6;     % Tolerance for declaring convergence (default value)
end
lambda = 1;
normA = norm(A,'fro');
cost_old = intmax;
disp('Iteration:')
for k = 1:N_iter
    % Print status and check for convergence each 1000 iterations
    if(mod(k,1000)==0), 
        fprintf('>>%d',k),
        cost = norm(A-lambda*R,'fro')/normA;
        fprintf(' (mse=%1.3f) ',cost),
        if cost_old - cost < tol, break, end
        cost_old = cost;
    end
    
    R = eye(size(A));
    for j = 1:J
        L = eye(size(A));
        for l = j+1:J
            L = factors(:,:,l)*L;
        end
        c = (1+1e-3)*lambda^2*(norm(R)^2)*(norm(L)^2);
        factors(:,:,j) = constraints{j}(1/c, factors(:,:,j) - (lambda/c)*L.'*(lambda*L*factors(:,:,j)*R - A)*R.' );
        if ~isequal(factors(:,:,j),zeros(size(A)))
            factors(:,:,j) = factors(:,:,j)/norm(factors(:,:,j),'fro');
        else
            factors(:,:,j) = eye(size(A));
        end
        R = factors(:,:,j)*R;
%         lambda = trace(A.'*R)/trace(R.'*R);
    end
    lambda = trace(A.'*R)/trace(R.'*R);
end
disp('>>end')
A_hat = lambda*R; % The factor lambda does not appear on the original paper
