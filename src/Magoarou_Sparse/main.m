%% USER CONFIGURABLE PARAMETERS

flag_figures = 0;   % 1 to plots figures
flag_gif = 0;       % 1 to generate an animated gif with real and estimated Dictionaries (only if flag_figures is also set)
gif_filename = 'Dictionaries.gif';

% Parameters

% Dictionary dimensions
m = 50;     % data size
n = 2048;   % # of atoms

k_rightmost = 5;
sp = 4; % (s/m): mean number of non-zero elements per column
P = 1.4*m^2;
rho = 0.8;
J = 4;          %Number of factors

dict_type = 'gaussian';   % Options are:
                          % 'gaussian' - for a gaussian random dictionary
                          % 'sparse' - for a product of sparse matrices

addpath('Proj_Prox_Operators')

%% GENERATING DICTIONARY
generate_dictionary

%% DEFINING FACTORS' CONSTRAINTS
constraint = cell(1,J-1);
constraint_tilde = cell(1,J-1);

% l0 regularization
constraint{1} = @(D) columnwise_proj_l0(k_rightmost,D);
n_left = min(m^2,round(P));
constraint_tilde{1} = @(D) proj_l0(n_left,D);

for l = 2:J-1
   n_right = sp*m;
   n_left = min(m^2,round(P*(rho^(l-1))));
   constraint{l} = @(D) proj_l0(n_right,D);
   constraint_tilde{l} = @(D) proj_l0(n_left,D);
end

% l1 regularization
q = 5e-5*ones(1,J);
D_sort = sort(abs(D(:)),'descend');
q(1) = D_sort(sp*m+1); % weigth on the l1-norm

for l = 1:J-1
   constraint_l1{l} = @(t,D) prox_l1(q(l),t,D);
   constraint_l1_tilde{l} = @(t,D) prox_l1(q(l),t,D);
end

%% TEST LOOP
% SNR = 0:2:40;
SNR = 10;
MSE = zeros(size(SNR));
MSE_hierarch = zeros(size(SNR));

for k = 1:length(SNR)

Noise = randn(size(D));
SNR_dB = SNR(k);
D_noisy = D + 10^(-SNR_dB/10)*norm(D,'fro')*Noise/norm(Noise,'fro');

%% DIRECT FACTORIZATION
[lambda D_hat other_factors rightmost_factor] = palm4MSA(D_noisy,J,{constraint{1:J-1} constraint_tilde{J-1}});
MSE(k) = norm(D-D_hat,'fro')

%% HIERARCHICAL FACTORIZATION : 
%  Factorization is obtained by successive 2 factor factorizations.
[lambda D_hat_hierarch other_factors rightmost_factor] = hierarchical_factorization(D_noisy,J,constraint, constraint_tilde);
MSE_hierarch(k) = norm(D-D_hat_hierarch,'fro')

%% DIRECT FACTORIZATION l1
[lambda D_hat factors] = palm4MSA_l1(D_noisy,J,{constraint_l1{1:J-1} constraint_l1_tilde{J-1}});
MSE_l1(k) = norm(D-D_hat,'fro')

%% HIERARCHICAL FACTORIZATION l1: 
%  Factorization is obtained by successive 2 factor factorizations.
[lambda D_hat factors] = hierarchical_factorization_l1(D_noisy,J,constraint_l1, constraint_l1_tilde);
MSE_hierarch_l1(k) = norm(D-D_hat,'fro')

end

%% TESTES
limiar = zeros(1,J);
nsparse = zeros(1,J);

teste = rightmost_factor;
teste_sorted = sort(abs(teste(:)),'descend');
nsparse(1) = size(teste(teste~=0),1);
for k = 1:J-1
   teste = other_factors(:,:,k);
   teste_sorted = sort(abs(teste(:)),'descend');
   limiar(k+1) = teste_sorted(sp*m);
   nsparse(k+1) = size(teste(teste~=0),1);
end
RC  = sum(nsparse)/numel(D(D~=0));
RCG = 1/RC;


%% SAVING RESULTS
cd Results\, save ResultsSNR_0-2-40 D MSE MSE_hierarch, cd ..

%% FIGURES
if flag_figures
    generate_figures
end