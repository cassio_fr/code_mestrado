%   Calculates the proximal function related to the l1-norm 
%   ( for the function F(X) = q*||X||_1 )
%   t : stepsize (vectorized stepsize is not supported, see TFOCS for this functionality)

function X = prox_l1(q,t,X)

x = X(:); % Input X may be a matrix (treated as a vector) or a vector

if ~isnumeric( t ) || ~isreal( t ) ||  numel( t ) ~= 1 || t < 0 || ...
   ~isnumeric( q ) || ~isreal( q ) ||  numel( q ) ~= 1 || q < 0
error( 'Stepsize t must be a non-negative scalar.' );
end

s  = 1 - min( q*t./abs(x), 1 );
x  = x .* s;

% Reshape vector x to input shape
X = reshape(x,size(X));