function X = proj_l1(q,X)

x = X(:); % Input X may be a matrix (treated as a vector) or a vector

switch nargin,
case 2,
    if ~isnumeric( q ) || ~isreal( q ) || numel( q ) ~= 1 || q <= 0,
        error( 'Argument q must be positive.' );
    end
    s      = sort(abs(nonzeros(x)),'descend');
    cs     = cumsum(s);
    % ndx    = find( cs - (1:numel(s))' .* [ s(2:end) ; 0 ] >= q, 1 );
    ndx    = find( cs - (1:numel(s))' .* [ s(2:end) ; 0 ] >= q+2*eps(q), 1 ); % For stability
    if ~isempty( ndx )
        thresh = ( cs(ndx) - q ) / ndx;
        x      = x .* ( 1 - thresh ./ max( abs(x), thresh ) ); % May divide very small numbers
    end
otherwise,
    error( 'proj_l1 should receive two arguments.' );
end

% Reshape vector x to input shape
X = reshape(x,size(X));