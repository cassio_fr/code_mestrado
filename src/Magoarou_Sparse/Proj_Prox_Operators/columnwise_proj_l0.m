function X = columnwise_proj_l0(q,X)

for jcol = 1:size(X,2)
   X(:,jcol) = proj_l0(q,X(:,jcol)); 
end