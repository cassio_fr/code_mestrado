function X = proj_l0(q,X)

x = X(:); % Input X may be a matrix or a vector

% input checking
if ~isnumeric( q ) || ~isreal( q ) ||  numel( q ) ~= 1 || mod(q,1)~=0 || q <= 0 || q>numel(x),
    error( 'Argument q must be a positive integer smaller than the size of x.' );
end

[~, I]     = sort(abs(x),'descend');
I = I(1:q); % index of the 'q' biggest elements of x is stored
idx = true(size(x));
idx(I) = false; % indexes all entries but the 'q' biggest

x(idx)  = 0; 

% Reshape vector x to input shape
X = reshape(x,size(X));