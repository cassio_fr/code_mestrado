%% HIERARCHICAL FACTORIZATION
function [lambda D_hat S] = hierarchical_factorization(D,J,constraint,constraint_tilde)

S = zeros([size(D) J]); % J matrices (factors)
F = zeros([size(D) 2]);
T = D;

for l = 1:J-1
    % Factorize the residual T into 2 factors
    [lambda T_hat F] = palm4MSA_l1(T,2,{constraint{l} constraint_tilde{l}});
    T = lambda*F(:,:,2);
    S(:,:,l) = F(:,:,1);
    % Global Optimization
    initialization = cat(3,S(:,:,1:l),T);
    [lambda D_hat S(:,:,1:l+1)] = palm4MSA_l1(D,l+1,{constraint{1:l} constraint_tilde{l}},initialization);
    T = lambda*S(:,:,l+1); % The leftmost factor is yet to be factorized
end


