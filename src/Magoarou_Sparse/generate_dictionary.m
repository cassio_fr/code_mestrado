switch dict_type
    case 'sparse'
        % Dictionary is a product of sparse matrices
        s = sp*m;   %sparsity level of each matrix
        n_factors = J;
        D = eye(m);
        D_factors = zeros([size(D) n_factors-1]);
        
        D_rightmost_factor = zeros(m,n);
        % Generate sparse matrix with k non-zero elements at each column
        idx = repmat((0:n-1)*m,k_rightmost,1) + ceil(m*rand(k_rightmost,n));
        D_rightmost_factor(idx(:)) = randn(1,k_rightmost*n);
        D = D_rightmost_factor;
        
        for k = 1:n_factors-1
            factor = zeros(m);
            % Generate sparse matrix with s non-zero elements in total
            idx = ceil(m^2*rand(1,s));
            factor(idx) = randn(1,s); factor = factor/norm(factor,'fro');

            D_factors(:,:,k) = factor;
            % Norma resultante para D: norm(D,'fro')~= sqrt(I/s^(I-1)), when D = D1*D2..*DI
            D = factor*D;
        end
        D = D/norm(D,'fro');
        clear factor
    otherwise
        % Gaussian random dictionary
        D = randn(m,n);
        D = D/norm(D,'fro');
end