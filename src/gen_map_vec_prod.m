% Creation date: 25/09/2015
% Amap = gen_map_vec_prod(n,b)
%
% Description:  This function returns the linear map Amap that operates 
%               over a matrix M (n,m) and corresponds to the product of 
%               this matrix M with a certain vector b.
%               i.e: Amap*X(:) == X*b
%
% Inputs:   n is the number of lines on matrix M
%           b is a (m,1) vector
%
% Outputs:  Amap is a (n, n*m) matrix

function Amap = gen_map_vec_prod(n,b)
m = length(b);
Amap = zeros(n,m*n);
% TODO tentar fazer sem for
for k = 1:n
    idx = sub2ind(size(Amap), k*ones(1,m), k:n:n*m);
    Amap(idx) = b;
end