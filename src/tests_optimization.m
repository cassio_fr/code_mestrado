D = randn(5,10);
D_hat = zeros(size(D));
%% Low-rank aproximation of D
% Inexact ALM - NÃO FUNCIONA!!! POIS IMPÕE D_hat = D
D_hat = zeros(size(D));

mu = 0.3 ; %10/norm(D)^2;
% mu_bar = mu*1e7;
% rho = 1.5        
Z = D/mu; % zeros(size(D));

tol = 1e-6*norm(D,'fro');
converged = false;
iter = 1;
% while ~converged
for i = 1:20
    % Min dispD
    [U, S, V] = svd(D + Z ,'econ');
    diagS = diag(S);
    svp = length(find(diagS > 1/mu));
    D_hat = U(:, 1:svp) * diag(diagS(1:svp) - 1/mu) * V(:, 1:svp)';

    temp_Z = Z + D - D_hat;
    
    % stop Criterion    
    if norm(temp_Z - Z, 'fro') < tol
        converged = true;
    end   
    Z = temp_Z;
    Z
    
    %     mu = min(mu*rho, mu_bar);
    iter = iter+1;
end

% Proximal Gradient
c =(1+1e-3); %tk = lambda/c (tamanho do passo)
lambda = 0.1*norm(D)^2; %lambda = 1/mu
tol = 1e-6*norm(D,'fro');
converged = false;
iter = 1;
while ~converged
    [U, S, V] = svd(D_hat - (1/c)*(D_hat - D), 'econ');
    diagS = diag(S);
    svp = length(find(diagS > lambda*1/c));
    temp_D = U(:, 1:svp) * diag(diagS(1:svp) - lambda*1/c) * V(:, 1:svp)';
    if( norm(D_hat - temp_D, 'fro') < tol )
        converged = true;
        disp(['Total nº of iterations: ' num2str(iter)]);
    end
    D_hat = temp_D;
    iter = iter+1;
end

% Results
fprintf('\n=== RESULTS ===\n')
fprintf('||D - D_hat||_F   normalized : %1.3f\n', norm(D-D_hat,'fro')/norm(D,'fro'));
fprintf('||Y - D_hat*X||_F normalized : %1.3f\n', norm(Y-D_hat*X,'fro')/norm(Y,'fro'));
fprintf('||D_hat||_* / ||D||_*        : %1.1f / %1.1f\n', sum(svd(D_hat)), sum(svd(D)));
fprintf('rank(D_hat) / rank(D)        : %d / %d\n', rank(D_hat,1e-6),rank(D,1e-6));

% CVX
cvx_begin
    variable De(size(D))
    minimize 0.5*square_pos(norm(De-D,'fro')) + lambda*norm_nuc(De)
cvx_end

% Results
fprintf('\n=== RESULTS ===\n')
fprintf('||D - De||_F   normalized : %1.3f\n', norm(D-De,'fro')/norm(D,'fro'));
fprintf('||Y - De*X||_F normalized : %1.3f\n', norm(Y-De*X,'fro')/norm(Y,'fro'));
fprintf('||D_hat||_* / ||D||_*        : %1.1f / %1.1f\n', sum(svd(D_hat)), sum(svd(D)));
fprintf('rank(D_hat) / rank(D)        : %d / %d\n', rank(D_hat,1e-6),rank(D,1e-6));

%% Low displacement rank aproximation of D
% Inexact ALM (Gives same result as CVX)
F1 = diag(ones(1,size(D,1)-1),-1);
F2 = diag(ones(1,size(D,2)-1),-1)';
D_hat = zeros(size(D));

step = 0.4; % Funcionou até 1.6, mas não altera o número de iterações necessárias.
lambda = 0.5*norm(D); %0.15*norm(D)^2; % peso da norma nuclear na otimização
mu = .5/norm(D-F1*D*F2); % Quanto maior, mais rápido converge. Pra rand(5,10) funciona até 0.7. Pra rand(20,40) funciona até 0.6
% mu_bar = mu*1e7;
% rho = 1.5      % Não funciona!
Z = (D - F1*D*F2)/mu; % zeros(size(D));

tol = 1e-7*norm(D,'fro');
converged = false;
iter = 1;
while ~converged
%     primal_converged = false;
%     while primal_converged == false
        
        % Min dispD
        [U, S, V] = svd(D_hat - F1*D_hat*F2 - Z ,'econ');
        diagS = diag(S);
        svp = length(find(diagS > lambda/mu));
        temp_dispD = U(:, 1:svp) * diag(diagS(1:svp) - lambda/mu) * V(:, 1:svp)';
        % Min D_hat
%         grad_converged = false;
%         while ~grad_converged
            grad1 = D_hat - F1*D_hat*F2 - temp_dispD - Z + F1'*(temp_dispD - D_hat + F1*D_hat*F2 + Z)*F2';
            grad2 = D_hat - D;
            temp_D_hat = D_hat - step*(mu*grad1 + grad2);
            if norm(D_hat - temp_D_hat, 'fro') < tol
                grad_converged = true;
            end
            D_hat = temp_D_hat;
%         end
    
%         if norm(D_hat - temp_D_hat, 'fro') < tol && norm(dispD - temp_dispD, 'fro') < tol
%             primal_converged = true;
%         end
        dispD = temp_dispD;
%     end
    
    temp_Z = Z + dispD - D_hat + F1*D_hat*F2;
   
    % stop Criterion    
    if norm(temp_Z - Z, 'fro') < tol
        converged = true;
        disp(['Total nº of iterations: ' num2str(iter)]);
    end   
    
    Z = temp_Z;

%     mu = min(mu*rho, mu_bar);
    iter = iter+1;
end

fprintf('\n=== RESULTS ===\n')
fprintf('||D - D_hat||_F   normalized : %1.3f\n', norm(D-D_hat,'fro')/norm(D,'fro'));
fprintf('||disp(D_hat)||_*  / ||D||_* : %1.1f / %1.1f\n', sum(svd(D_hat-F1*D_hat*F2)), sum(svd(D - F1*D*F2)));
fprintf('rank( disp(D_hat) )/ rank(D) : %d   / %d\n', rank(D_hat-F1*D_hat*F2,1e-6),rank(D - F1*D*F2,1e-6));


% Proximal Gradient
F1 = diag(ones(1,size(D,1)-1),-1);
F2 = diag(ones(1,size(D,2)-1),-1)';
D_hat = zeros(size(D));
c = (1+1e-3); % As long as c>=1, it does not interfere on the result. The biggest c, the slowest the convergence.
lambda = 0.15*norm(D)^2;
tol = 1e-7*norm(D,'fro');
converged = false;
iter = 1;
while ~converged
    T = D_hat - (1/c)*(D_hat - D);
    [U, S, V] = svd(T - F1*T*F2, 'econ');
    diagS = diag(S);
    svp = length(find(diagS > lambda*1/c));
    dispD = U(:, 1:svp) * diag(diagS(1:svp) - lambda*1/c) * V(:, 1:svp)';
    temp_D = recover(dispD, F1,0,size(D,1),F2,0,size(D,2));
    
    % Stop criterion
    if( norm(D_hat - temp_D, 'fro') < tol )
        converged = true;
        disp(['Total nº of iterations: ' num2str(iter)]);
    end
    D_hat = temp_D;
    iter = iter+1;
end

fprintf('\n=== RESULTS ===\n')
fprintf('||D - D_hat||_F   normalized : %1.3f\n', norm(D-D_hat,'fro')/norm(D,'fro'));
fprintf('||disp(D_hat)||_*  / ||D||_* : %1.1f / %1.1f\n', sum(svd(D_hat-F1*D_hat*F2)), sum(svd(D - F1*D*F2)));
fprintf('rank( disp(D_hat) )/ rank(D) : %d   / %d\n', rank(D_hat-F1*D_hat*F2,1e-6),rank(D - F1*D*F2,1e-6));

% CVX
lambda = 0.5*norm(D); %lambda_CVX ~= lambda_prox/1.5; in order to obtain similar results
cvx_begin
    variable De(size(D))
    minimize 0.5*square_pos(norm(De-D,'fro')) + lambda*norm_nuc(De - F1*De*F2)
cvx_end

% Results
fprintf('\n=== RESULTS ===\n')
fprintf('||D - De||_F   normalized : %1.3f\n', norm(D-De,'fro')/norm(D,'fro'));
fprintf('||De||_* / ||D||_*        : %1.1f / %1.1f\n', sum(svd(De-F1*De*F2)), sum(svd(D - F1*D*F2)));
fprintf('rank(De) / rank(D)        : %d    / %d\n', rank(De-F1*De*F2,1e-6),rank(D-F1*D*F2,1e-6));

%% Find low-rank D that minimizes norm(Y-DX,'fro')
F1 = diag(ones(1,size(D,1)-1),-1);
F2 = diag(ones(1,size(D,2)-1),-1)';

dim = size(D,1); % samples dimension
n_atoms = size(D,2);
n_samples = size(D,2)*3;
k = 3; %sparsity of X per column

X = zeros(n_atoms, n_samples);

% Generate sparse matrix with k non-zero elements at each column
idx = repmat((0:n_samples-1)*n_atoms,k,1) + ceil(n_atoms*rand(k,n_samples));
X(idx(:)) = randn(1,k*n_samples);

Y = D*X;

lambda = 0.05*norm(Y)^2; % peso da norma nuclear na otimização
D_hat = zeros(size(D));

% Proximal Gradient
c =(1+1e-3)*norm(X)^2;
tol = 1e-6*norm(D,'fro');
D_hat = zeros(size(D));
converged = false;
iter = 1;
while ~converged
    [U, S, V] = svd(D_hat - (1/c)*(D_hat*X - Y)*X.', 'econ');
    diagS = diag(S);
    svp = length(find(diagS > lambda*1/c));
    temp_D = U(:, 1:svp) * diag(diagS(1:svp) - lambda*1/c) * V(:, 1:svp)';
    if( norm(D_hat - temp_D, 'fro') < tol )
        converged = true;
        disp(['Total nº of iterations: ' num2str(iter)]);
    end
    D_hat = temp_D;
    iter = iter+1;
end

% Results
fprintf('\n=== RESULTS ===\n')
fprintf('||D - D_hat||_F   normalized : %1.3f\n', norm(D-D_hat,'fro')/norm(D,'fro'));
fprintf('||Y - D_hat*X||_F normalized : %1.3f\n', norm(Y-D_hat*X,'fro')/norm(Y,'fro'));
fprintf('||D_hat||_* / ||D||_*        : %1.1f / %1.1f\n', sum(svd(D_hat)), sum(svd(D)));
fprintf('rank(D_hat) / rank(D)        : %d / %d\n', rank(D_hat,1e-6),rank(D,1e-6));

% CVX
cvx_begin
    variable De(size(D))
    minimize 0.5*square_pos(norm(De*X - Y,'fro')) + lambda*norm_nuc(De)
cvx_end


% Results
fprintf('\n=== RESULTS ===\n')
fprintf('||D - De||_F   normalized : %1.3f\n', norm(D-De,'fro')/norm(D,'fro'));
fprintf('||Y - De*X||_F normalized : %1.3f\n', norm(Y-De*X,'fro')/norm(Y,'fro'));
fprintf('||De||_* / ||D||_*        : %1.1f / %1.1f\n', sum(svd(De)), sum(svd(D)));
fprintf('rank(De) / rank(D)        : %d / %d\n', rank(De,1e-6),rank(D,1e-6));

%% Find low displacement rank D that minimizes norm(Y-DX,'fro')
F1 = diag(ones(1,size(D,1)-1),-1);
F2 = diag(ones(1,size(D,2)-1),-1)';

dim = size(D,1); % samples dimension
n_atoms = size(D,2);
n_samples = size(D,2)*3;
k = 3; %sparsity of X per column

X = zeros(n_atoms, n_samples);

% Generate sparse matrix with k non-zero elements at each column
idx = repmat((0:n_samples-1)*n_atoms,k,1) + ceil(n_atoms*rand(k,n_samples));
X(idx(:)) = randn(1,k*n_samples);

Y = D*X;

lambda = 0.05*norm(Y)^2; % peso da norma nuclear na otimização

% Inexact ALM (Mesmo resultado que CVX, só que bem mais leve)
step = .1/norm(D); % Para D(5x10) norm ~4.9 funciona até 0.1. Para D(20x40) norm ~9.9 funciona até 0.079. Para D(5x100) norm ~11 parece funcionar até 0.059
                    % Conclusão: Parece funcionar aprox até step <= .5/norm(D)
mu = 100/norm(D-F1*D*F2); % Quanto maior, mais rápido converge. 
                             % Pra D(5,10) norm 4.4 normdisp 5.4 funciona até 22.15 (115/norm(dispD))
                             % Pra D(5,100) norm 11.6 normdisp 15.2 funciona até 6.54 (100/norm(dispD))
                             % Pra D(20,40) norm 10.4 normdisp 14.8 funciona até 4.06 (60/norm(dispD))
                             % Parece funcionar até mu<=50/norm(dispD)
                             
% mu_bar = mu*1e7;
% rho = 1.5      
Z = (D - F1*D*F2)/mu; 
D_hat = zeros(size(D));

% Ajuste automatico para step
found = false;
step = 1e-10/norm(D);
mu = 1e-10;
iter = 0;
clear lbound rbound
while ~found
    % Min dispD
    [U, S, V] = svd(D_hat - F1*D_hat*F2 - Z ,'econ');
    diagS = diag(S);
    svp = length(find(diagS > lambda/mu));
    temp_dispD = U(:, 1:svp) * diag(diagS(1:svp) - lambda/mu) * V(:, 1:svp)';
    % Min D_hat
    grad1 = D_hat - F1*D_hat*F2 - temp_dispD - Z + F1'*(temp_dispD - D_hat + F1*D_hat*F2 + Z)*F2';
    grad2 = (D_hat*X - Y)*X.';
    norm1 = norm(step*(mu*grad1 + grad2), 'fro');
    D_hat = D_hat - step*(mu*grad1 + grad2);
    grad1 = D_hat - F1*D_hat*F2 - temp_dispD - Z + F1'*(temp_dispD - D_hat + F1*D_hat*F2 + Z)*F2';
    grad2 = (D_hat*X - Y)*X.';
    norm2 = norm(step*(mu*grad1 + grad2), 'fro');
    
    if norm2 < norm1 % Converges
       lbound = step;
       if exist('rbound','var')
           step = (lbound + rbound)/2;
       else
           step = step*1e3;
       end
    else % Diverges
        rbound = step;
        if exist('lbound','var')
           step = (lbound + rbound)/2;
        else
           step = step/1e3;
        end
    end
    
    if exist('lbound','var') && exist('rbound','var') 
        if (rbound - lbound)/lbound < 1e-6
            found = true;
            step = step/8;
        end
    end
    iter = iter + 1
end
step

found = false;
iter = 0;
clear lbound rbound
while ~found  
    Z = (D - F1*D*F2)/mu;
    % Min dispD
    [U, S, V] = svd(D_hat - F1*D_hat*F2 - Z ,'econ');
    diagS = diag(S);
    svp = length(find(diagS > lambda/mu));
    dispD = U(:, 1:svp) * diag(diagS(1:svp) - lambda/mu) * V(:, 1:svp)';
    % Min D_hat
    grad1 = D_hat - F1*D_hat*F2 - temp_dispD - Z + F1'*(temp_dispD - D_hat + F1*D_hat*F2 + Z)*F2';
    grad2 = (D_hat*X - Y)*X.';
    D_hat = D_hat - step*(mu*grad1 + grad2);
        
    Z = Z + dispD - D_hat + F1*D_hat*F2;
  
    norm1 = norm(dispD - D_hat + F1*D_hat*F2, 'fro');
        
    % Min dispD
    [U, S, V] = svd(D_hat - F1*D_hat*F2 - Z ,'econ');
    diagS = diag(S);
    svp = length(find(diagS > lambda/mu));
    dispD = U(:, 1:svp) * diag(diagS(1:svp) - lambda/mu) * V(:, 1:svp)';
    % Min D_hat
    grad1 = D_hat - F1*D_hat*F2 - temp_dispD - Z + F1'*(temp_dispD - D_hat + F1*D_hat*F2 + Z)*F2';
    grad2 = (D_hat*X - Y)*X.';
    D_hat = D_hat - step*(mu*grad1 + grad2);    
    
    norm2 = norm(dispD - D_hat + F1*D_hat*F2, 'fro');
    
    if norm2 < norm1 % Converges
       lbound = mu;
       if exist('rbound','var')
           mu = (lbound + rbound)/2;
       else
           mu = mu*1e3;
       end
    else % Diverges
        rbound = mu;
        if exist('lbound','var')
           mu = (lbound + rbound)/2;
        else
           mu = mu/1e3;
        end
    end
    
    if exist('lbound','var') && exist('rbound','var') 
        if (rbound - lbound)/lbound < 1e-6
            found = true;
            mu = mu/2;
        end
    end
    
    iter = iter+1
end
mu


D_hat = zeros(size(D));
tol = 1e-7*norm(D,'fro');
converged = false;
iter = 1;
tic
while ~converged
% for k = 1:20
%     primal_converged = false;
%     while primal_converged == false
        
        % Min dispD
        [U, S, V] = svd(D_hat - F1*D_hat*F2 - Z ,'econ');
        diagS = diag(S);
        svp = length(find(diagS > lambda/mu));
        temp_dispD = U(:, 1:svp) * diag(diagS(1:svp) - lambda/mu) * V(:, 1:svp)';
        % Min D_hat
%         grad_converged = false;
%         while ~grad_converged
            grad1 = D_hat - F1*D_hat*F2 - temp_dispD - Z + F1'*(temp_dispD - D_hat + F1*D_hat*F2 + Z)*F2';
            grad2 = (D_hat*X - Y)*X.';
            temp_D_hat = D_hat - step*(mu*grad1 + grad2);
%             if norm(D_hat - temp_D_hat, 'fro') < tol
%                 grad_converged = true;
%             end
%             norm(D_hat - temp_D_hat, 'fro')
            D_hat = temp_D_hat;
%         end
    
%         if norm(D_hat - temp_D_hat, 'fro') < tol && norm(dispD - temp_dispD, 'fro') < tol
%             primal_converged = true;
%         end
        dispD = temp_dispD;
%     end
    
    temp_Z = Z + dispD - D_hat + F1*D_hat*F2;
   
    % stop Criterion    
    if norm(temp_Z - Z, 'fro') < tol
        converged = true;
        disp(['Total nº of iterations: ' num2str(iter)]);
    end
    
    norm(temp_Z - Z, 'fro')

    Z = temp_Z;

%     mu = min(mu*rho, mu_bar);
    iter = iter+1;
end
toc
% Results
fprintf('\n=== RESULTS ===\n')
fprintf('||D - D_hat||_F   normalized      : %1.3f\n', norm(D-D_hat,'fro')/norm(D,'fro'));
fprintf('||Y - D_hat*X||_F normalized      : %1.3f\n', norm(Y-D_hat*X,'fro')/norm(Y,'fro'));
fprintf('||disp(D_hat)||_* / ||disp(D)||_* : %1.1f / %1.1f\n', sum(svd(D_hat - F1*D_hat*F2)), sum(svd(D - F1*D*F2)));
fprintf('rank(disp(D_hat)) / rank(disp(D)) : %d / %d\n', rank(D_hat - F1*D_hat*F2,1e-6),rank(D - F1*D*F2,1e-6));

% Proximal Gradient
F1 = diag(ones(1,size(D,1)-1),-1);
F2 = diag(ones(1,size(D,2)-1),-1)';

c =(1+1e-3)*norm(X)^2;
tol = 1e-6*norm(D,'fro');
D_hat = zeros(size(D));
converged = false;
iter = 1;
while ~converged
    T = D_hat - (1/c)*(D_hat*X - Y)*X.';
    [U, S, V] = svd(T - F1*T*F2, 'econ');
    diagS = diag(S);
    svp = length(find(diagS > lambda*1/c));
    dispD = U(:, 1:svp) * diag(diagS(1:svp) - lambda*1/c) * V(:, 1:svp)';
    temp_D = recover(dispD, F1,0,size(D,1),F2,0,size(D,2));
    
    % Stop criterion
    if( norm(D_hat - temp_D, 'fro') < tol )
        converged = true;
        disp(['Total nº of iterations: ' num2str(iter)]);
    end
    D_hat = temp_D;
    iter = iter+1;
end

% Results
fprintf('\n=== RESULTS ===\n')
fprintf('||D - D_hat||_F   normalized      : %1.3f\n', norm(D-D_hat,'fro')/norm(D,'fro'));
fprintf('||Y - D_hat*X||_F normalized      : %1.3f\n', norm(Y-D_hat*X,'fro')/norm(Y,'fro'));
fprintf('||disp(D_hat)||_* / ||disp(D)||_* : %1.1f / %1.1f\n', sum(svd(D_hat - F1*D_hat*F2)), sum(svd(D - F1*D*F2)));
fprintf('rank(disp(D_hat)) / rank(disp(D)) : %d / %d\n', rank(D_hat - F1*D_hat*F2,1e-6),rank(D - F1*D*F2,1e-6));


% CVX
cvx_begin
    variable De(size(D))
    minimize 0.5*square_pos(norm(De*X - Y,'fro')) + lambda*norm_nuc(De - F1*De*F2)
cvx_end


% Results
fprintf('\n=== RESULTS ===\n')
fprintf('||D - De||_F   normalized : %1.3f\n', norm(D-De,'fro')/norm(D,'fro'));
fprintf('||Y - De*X||_F normalized : %1.3f\n', norm(Y-De*X,'fro')/norm(Y,'fro'));
fprintf('||De||_* / ||D||_*        : %1.1f / %1.1f\n', sum(svd(De-F1*De*F2)), sum(svd(D-F1*D*F2)));
fprintf('rank(De) / rank(D)        : %d / %d\n', rank(De-F1*De*F2,1e-6),rank(D-F1*D*F2,1e-6));