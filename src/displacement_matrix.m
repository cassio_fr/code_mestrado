% Returns the displacement matrix originated form matrix X with respect of F.
function [Delta_A_F] = displacement_matrix(X,F)
Delta_A_F = X - F*X*F';
end