% Source:
% http://www.mathworks.com/matlabcentral/fileexchange/50056-matrix-completion-using-nuclear-norm--spectral-norm-or-weighted-nuclear-norm-minimization/content/FindBestSpectralApprox.m
%
% Completes a matrix with missing entries, such that the obtained matrix
% has minimal norm.
function X = FindBestSpectralApprox(M, lambda)
% Finds the best approximation X, to a givem matrix M, such that
% ||X||<=lambda. The norm is the spectral norm (largest singular value).
    [u,s,v]=svd(M);
    s(s>lambda)=lambda;
    X=u*s*v';
end
    