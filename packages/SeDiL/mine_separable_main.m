%(c) Simon Hawe, Lehrstuhl fuer Datenverarbeitung Technische Universitaet
%Muenchen, 2012. Contact: simon.hawe@tum.de
addpath('../SeDiL/utilis/'); close all; rand('state',12349876);
% Path to tensor toolbox
addpath('../tensor_toolbox_2.5/');
% Currently possible selections
Operators    = {'TV', 'SVD', 'SVDN','DCT','RAND','ELAD','LOAD','NONE'};
Applications = {'Upsampling', 'Inpainting_mask', 'Inpainting_rnd', 'Denoising','CS'};
IName = 'august.png';
%IName = 'boat.png';
%IName = 'barbara.png';
% Width of square image patch
% First width, second height
P_sz            = [64,64];
P_sz            = [8,8];
Nbr_of_atoms    = round(sqrt(4)*P_sz);
%Nbr_of_atoms    = round(1*P_sz);
%Nbr_of_atoms    = round(P_sz);
%Nbr_of_atoms = 128;

S                   = [];
IdxSet              = {};
no_sep              = 0;

%% Sparse Coding parameter setting
D = params.initdict;
[S]   =  params.data;

CODE_SPARSITY = 1;
CODE_ERROR = 2;

MEM_LOW = 1;
MEM_NORMAL = 2;
MEM_HIGH = 3;

%%%%% parse input parameters %%%%%

ompparams = {'checkdict','off'};

% coding mode %

if (isfield(params,'codemode'))
  switch lower(params.codemode)
    case 'sparsity'
      codemode = CODE_SPARSITY;
      thresh = params.Tdata;
    case 'error'
      codemode = CODE_ERROR;
      thresh = params.Edata;
    otherwise
      error('Invalid coding mode specified');
  end
elseif (isfield(params,'Tdata'))
  codemode = CODE_SPARSITY;
  thresh = params.Tdata;
elseif (isfield(params,'Edata'))
  codemode = CODE_ERROR;
  thresh = params.Edata;

else
  error('Data sparse-coding target not specified');
end


% max number of atoms %

if (codemode==CODE_ERROR && isfield(params,'maxatoms'))
  ompparams{end+1} = 'maxatoms';
  ompparams{end+1} = params.maxatoms;
end


% memory usage %

if (isfield(params,'memusage'))
  switch lower(params.memusage)
    case 'low'
      memusage = MEM_LOW;
    case 'normal'
      memusage = MEM_NORMAL;
    case 'high'
      memusage = MEM_HIGH;
    otherwise
      error('Invalid memory usage mode');
  end
else
  memusage = MEM_NORMAL;
end


% iteration count %

if (isfield(params,'iternum'))
  iternum = params.iternum;
else
  iternum = 10;
end


% omp function %

if (codemode == CODE_SPARSITY)
  ompfunc = @omp;
else
  ompfunc = @omp2;
end


% data norms %

XtX = []; XtXg = [];
if (codemode==CODE_ERROR && memusage==MEM_HIGH)
  XtX = sum(S.^2);
end

err = zeros(1,iternum);
gerr = zeros(1,iternum);

if (codemode == CODE_SPARSITY)
  errstr = 'RMSE';
else
  errstr = 'mean atomnum';
end

%% Sparsecoding
G = [];
if (memusage >= MEM_NORMAL)
    G = D'*D;
end

if (memusage < MEM_HIGH)
  X = ompfunc(D,S,G,thresh,ompparams{:});

else  % memusage is high

  if (codemode == CODE_SPARSITY)
    X = ompfunc(D'*S,G,thresh,ompparams{:});

  else
    X = ompfunc(D'*S,XtX,G,thresh,ompparams{:});
  end

end 

%% Dictionary training
for j = 1:1
    % Get the images from which we learn
    clear Images;
    % Number of training patches
    n_patches    = params.trainnum;
    Images{1}    = '../SeDiL/training/';
    Images{2}    = IName;
    
    Learn_para          = init_learning_parameters_dict();
    
    X = bsxfun(@times,X,1./(sqrt(sum(S.^2))));
    Learn_para.X= reshape(full(X),Nbr_of_atoms(1),Nbr_of_atoms(2),size(X,2));

    % S containes the entire trainings set
%     [S]   =  complete_training_set(Images, n_patches, P_sz);
    [S]   =  params.data;
    % Normalize the patches and subtract the mean value
    ms = mean(S);
    S  = bsxfun(@minus,S,ms);
    S = bsxfun(@times,S,1./(sqrt(sum(S.^2))));
    n_patches    = size(S,2);

    if exist('LLGG','var')
        Learn_para.logger = LLGG;
    end
    
    Learn_para.d_sz = P_sz(1);
    
    if no_sep == 1
        P_sz            = prod(P_sz);
        Nbr_of_atoms    = prod(Nbr_of_atoms);
        Learn_para.d_sz = sqrt(P_sz(1));
    end
    % Normalizing the columns of the inital kernel
%     randn('seed',0);
    
    for i=1:numel(Nbr_of_atoms)
        % Random dictionary
%         D = randn(P_sz(i),Nbr_of_atoms(i));
%         [U,SS,V]=svd(D,0);
%         D = bsxfun(@minus,D,mean(D));
%         Learn_para.D{i}           = bsxfun(@times,D,1./sqrt(sum(D.^2,1)));
        % DCT
        Learn_para.D{i}           = odctdict(P_sz(i),Nbr_of_atoms(i));
    end
    
    Learn_para.max_iter = params.iternum;
    Learn_para.mu       = 1e2;    % Multiplier in log(1+mu*x^2)
    Learn_para.lambda   = 0.0135;%5e-2;  %1e9;%1e-3;    % Lagrange multiplier
    Learn_para.kappa    = 0.0129; %0.1/(Nbr_of_atoms(1)*Nbr_of_atoms(2)); %1e-1;    %1e4    % Weighting for Distinctive Terms
    Learn_para.q        = [0,1];
    
    % Displaying results every mod(iter,Learn_para.verbose) iterations
    Learn_para.verbose  = 20;
    max_out_iter = 10;%3
    res = cell(max_out_iter);
    tic
    
    lambda_end = 7e-3;%5e4;
    shrink1  = (lambda_end/Learn_para.lambda)^(1/(max_out_iter-1));
    shrink2  = (1e-2/Learn_para.kappa)^(1/(max_out_iter-1));
    
    S = reshape(S,[P_sz,n_patches]);
    
    Learn_para = learn_separable_dictionary(S, Learn_para);
    LP = Learn_para;
    LP.X=[];
    save(sprintf('DICT'),'-struct','LP');
    Learn_para.lambda = Learn_para.lambda*shrink1;
    Learn_para.X=[];
    
    toc
    LLGG = Learn_para.logger;
end

D = kron(Learn_para.D{1},Learn_para.D{2});

