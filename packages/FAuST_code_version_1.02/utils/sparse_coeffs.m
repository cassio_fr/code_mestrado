function Gamma = sparse_coeffs(D, Ntraining, Sparsity)
%% Description sparse_coeffs
%  Generation of sparse coefficients
%  Gamma = sparse_coeffs(D, Ntraining, Sparsity) generates Ntraining sparse
%  vectors stacked in a matrix Gamma. Each sparse vector is of size the 
%  number of atoms in the dictionary D, its support is drawn uniformly at
%  random and each non-zero entry is iid Gaussian. 
%
% For more information on the FAuST Project, please visit the website of 
% the project :  <http://faust.gforge.inria.fr>
%
%% License:
% Copyright (2016):	Luc Le Magoarou, Remi Gribonval
%			INRIA Rennes, FRANCE
%			http://www.inria.fr/
%
% The FAuST Toolbox is distributed under the terms of the GNU Affero 
% General Public License.
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as published 
% by the Free Software Foundation.
%
% This program is distributed in the hope that it will be useful, but 
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
% See the GNU Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%% Contacts:	
%	Luc Le Magoarou: luc.le-magoarou@inria.fr
%	Remi Gribonval : remi.gribonval@inria.fr
%
%% References:
% [1]	Le Magoarou L. and Gribonval R., "Flexible multi-layer sparse 
%	approximations of matrices and applications", Journal of Selected 
%	Topics in Signal Processing, 2016.
%	<https://hal.archives-ouvertes.fr/hal-01167948v1>
%%


Natoms = size(D,2);
Gamma = zeros(Natoms,Ntraining);
for i = 1:Ntraining
    r = randn(Sparsity ,1);
    pos_temp = randperm(Natoms);
    pos = pos_temp(1:Sparsity);
    Gamma(pos,i) = r;
end
end

