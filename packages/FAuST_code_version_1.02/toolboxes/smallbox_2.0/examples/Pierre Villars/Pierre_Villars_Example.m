%%  Pierre Villars Example
%   
%   This example is based on the experiment suggested by Professor Pierre
%   Vandergheynst on the SMALL meeting in Villars.
%   The idea behind is to use patches from source image as a dictionary in
%   which we represent target image using matching pursuit algorithm.
%   Calling Pierre_Problem function to get src image to be used as dictionary
%   and target image to be represented using MP with 3 patches from source image

%
%   Centre for Digital Music, Queen Mary, University of London.
%   This file copyright 2010 Ivan Damnjanovic.
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License as
%   published by the Free Software Foundation; either version 2 of the
%   License, or (at your option) any later version.  See the file
%   COPYING included with this distribution for more information.
%
%%

clear;

%   Defining the Problem structure

SMALL.Problem = generatePierreProblem();

%   Show original image and image that is used as a dictionary
figure('Name', 'Original and Dictionary Image');

subplot(1,2,1); imagesc(SMALL.Problem.imageTrg/SMALL.Problem.maxval);
title('Original Image');colormap(gray);axis off; axis image;
subplot(1,2,2); imagesc(SMALL.Problem.imageSrc/SMALL.Problem.maxval);
title('Dictionary image:');colormap(gray);axis off; axis image;

%   Using ten different dictionary sizes. First dictionary will contain all
%   patches from the source image and last one will have only
%   num_src_patches/2^9 atoms representing equidistant patches taken from
%   the source image.

n =10;
dictsize=zeros(1,n);
time = zeros(1,n);
psnr = zeros(1,n);

for i=1:n
    %   Set reconstruction function
    
    SMALL.Problem.reconstruct=@(x) Pierre_reconstruct(x, SMALL.Problem);
    
    %   Defining the parameters sparse representation
    SMALL.solver(i)=SMALL_init_solver;
    SMALL.solver(i).toolbox='SMALL';
    SMALL.solver(i).name='SMALL_MP';
    
    %   Parameters needed for matching pursuit (max number of atoms is 3
    %   and residual error goal is 1e-14
    
    SMALL.solver(i).param=sprintf('%d, 1e-14',3);
    
    % Represent the image using the source image patches as dictionary
    
    SMALL.solver(i)=SMALL_solve(SMALL.Problem, SMALL.solver(i));
    
    
    dictsize(1,i) = size(SMALL.Problem.A,2);
    time(1,i) = SMALL.solver(i).time;
    psnr(1,i) = SMALL.solver(i).reconstructed.psnr;
    
    %   Set new SMALL.Problem.A dictionary taking every second patch from
    %   previous dictionary
    
    SMALL.Problem.A=SMALL.Problem.A(:,1:2:dictsize(1,i));
    
    
    %%  show reconstructed image %%
    figure('Name', sprintf('dictsize=%d', dictsize(1,i)));
    
    imagesc(SMALL.solver(i).reconstructed.image/SMALL.Problem.maxval);
    title(sprintf('Reconstructed image, PSNR: %.2f dB in %.2f s',...
        SMALL.solver(i).reconstructed.psnr, SMALL.solver(i).time ));
    colormap(gray);axis off; axis image;
    
end

%%  plot time and psnr given dictionary size %%
figure('Name', 'time and psnr');

subplot(1,2,1); plot(dictsize(1,:), time(1,:), 'ro-');
title('Time vs number of source image patches used');
subplot(1,2,2); plot(dictsize(1,:), psnr(1,:), 'b*-');
title('PSNR vs number of source image patches used');