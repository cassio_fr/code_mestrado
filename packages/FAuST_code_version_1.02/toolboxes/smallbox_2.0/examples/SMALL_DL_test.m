function SMALL_DL_test
clear, clc, close all
% Create a 2-dimensional dataset of points that are oriented in 3
% directions on a x/y plane

%
%   Centre for Digital Music, Queen Mary, University of London.
%   This file copyright 2012 Daniele Barchiesi.
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License as
%   published by the Free Software Foundation; either version 2 of the
%   License, or (at your option) any later version.  See the file
%   COPYING included with this distribution for more information.

nData = 10000;						%number of data
theta = [pi/6 pi/3 4*pi/6];			%angles
nAngles = length(theta);			%number of angles
Q	  = [cos(theta); sin(theta)];	%rotation matrix
X	  = Q*randmog(nAngles,nData);	%training data

% find principal directions using PCA
XXt = X*X';							%cross correlation matrix
[U ~] = svd(XXt);					%svd of XXt

scale = 3;							%scale factor for plots
subplot(1,2,1), hold on
title('Principal Component Analysis')
scatter(X(1,:), X(2,:),'.');		%scatter training data
O = zeros(size(U));					%origin
quiver(O(1,1:2),O(2,1:2),scale*U(1,:),scale*U(2,:),...
	'LineWidth',2,'Color','k')		%plot atoms
axis equal							%scale axis

subplot(1,2,2), hold on
title('K-SVD Dictionary')
scatter(X(1,:), X(2,:),'.');
axis equal

nAtoms  = 3;						%number of atoms in the dictionary
nIter   = 1;						%number of dictionary learning iterations
initDict = normc(randn(2,nAtoms));	%random initial dictionary
O = zeros(size(initDict));			%origin

% apply dictionary learning algorithm
ksvd_params = struct('data',X,...			%training data
					 'Tdata',1,...			%sparsity level
					 'dictsize',nAtoms,...	%number of atoms
					 'initdict',initDict,...%initial dictionary
					 'iternum',10);			%number of iterations
DL = SMALL_init_DL('ksvd','ksvd',ksvd_params); %dictionary learning structure
DL.D = initDict;							%copy initial dictionary in solution variable
problem = struct('b',X);					%copy training data in problem structure

xdata = DL.D(1,:);
ydata = DL.D(2,:);
qPlot = quiver(O(1,:),O(2,:),scale*initDict(1,:),scale*initDict(2,:),...
	'LineWidth',2,'Color','k','UDataSource','xdata','VDataSource','ydata');

for iIter=1:nIter
	DL.ksvd_params.initdict = DL.D;
	pause
	DL = SMALL_learn(problem,DL);			%learn dictionary
	xdata = scale*DL.D(1,:);	
	ydata = scale*DL.D(2,:);
	refreshdata(qPlot,'caller');
    %set(qPlot,'XData',xdata,'YData',ydata);
end


function X = randmog(m, n)
% RANDMOG - Generate mixture of Gaussians
s = [0.2 2];
% Choose which Gaussian
G1 = (rand(m, n) < 0.9);
% Make them
X = (G1.*s(1) + (1-G1).*s(2)) .* randn(m,n);
