%%  Dictionary Learning for Image Denoising - KSVD vs Recursive Least Squares
%
%   This file contains an example of how SMALLbox can be used to test different
%   dictionary learning techniques in Image Denoising problem.
%   It calls generateImageDenoiseProblem that will let you to choose image,
%   add noise and use noisy image to generate training set for dictionary
%   learning.
%   Two dictionary learning techniques were compared:
%
%   -   KSVD - M. Elad, R. Rubinstein, and M. Zibulevsky, "Efficient
%              Implementation of the K-SVD Algorithm using Batch Orthogonal
%              Matching Pursuit", Technical Report - CS, Technion, April 2008.
%
%   -   MMDL - M. Yaghoobi, T. Blumensath and M. Davies, "Dictionary Learning
%              for Sparse Approximations with the Majorization Method", IEEE 
%              Trans. on Signal Processing, Vol. 57, No. 6, pp 2178-2191, 2009.


%   Centre for Digital Music, Queen Mary, University of London.
%   This file copyright 2011 Ivan Damnjanovic.
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License as
%   published by the Free Software Foundation; either version 2 of the
%   License, or (at your option) any later version.  See the file
%   COPYING included with this distribution for more information.
%   
%%



%   If you want to load the image outside of generateImageDenoiseProblem
%   function uncomment following lines. This can be useful if you want to
%   denoise more then one image for example.
%   Here we are loading test_image.mat that contains structure with 5 images : lena,
%   barbara,boat, house and peppers.
clear;
TMPpath=pwd;
FS=filesep;
[pathstr1, name, ext] = fileparts(which('SMALLboxSetup.m'));
cd([pathstr1,FS,'data',FS,'images']);
load('test_image.mat');
cd(TMPpath);

%   Deffining the noise levels that we want to test

noise_level=[10 20 25 50 100];

%   Here we loop through different noise levels and images 

for noise_ind=2:2
for im_num=1:1

% Defining Image Denoising Problem as Dictionary Learning
% Problem. As an input we set the number of training patches.

SMALL.Problem = generateImageDenoiseProblem(test_image(im_num).i, 40000, '',256, noise_level(noise_ind));
SMALL.Problem.name=int2str(im_num);

Edata=sqrt(prod(SMALL.Problem.blocksize)) * SMALL.Problem.sigma * SMALL.Problem.gain;
maxatoms = floor(prod(SMALL.Problem.blocksize)/2);

%   results structure is to store all results

results(noise_ind,im_num).noisy_psnr=SMALL.Problem.noisy_psnr;

%%
%   Use KSVD Dictionary Learning Algorithm to Learn overcomplete dictionary

%   Initialising Dictionary structure
%   Setting Dictionary structure fields (toolbox, name, param, D and time)
%   to zero values

SMALL.DL(1)=SMALL_init_DL();

% Defining the parameters needed for dictionary learning

SMALL.DL(1).toolbox = 'KSVD';
SMALL.DL(1).name = 'ksvd';

%   Defining the parameters for KSVD
%   In this example we are learning 256 atoms in 20 iterations, so that
%   every patch in the training set can be represented with target error in
%   L2-norm (Edata)
%   Type help ksvd in MATLAB prompt for more options.


SMALL.DL(1).param=struct(...
    'Edata', Edata,...
    'initdict', SMALL.Problem.initdict,...
    'dictsize', SMALL.Problem.p,...
    'exact', 1, ...
    'iternum', 20,...
    'memusage', 'high');

%   Learn the dictionary

SMALL.DL(1) = SMALL_learn(SMALL.Problem, SMALL.DL(1));

%   Set SMALL.Problem.A dictionary
%   (backward compatiblity with SPARCO: solver structure communicate
%   only with Problem structure, ie no direct communication between DL and
%   solver structures)

SMALL.Problem.A = SMALL.DL(1).D;
SMALL.Problem.reconstruct = @(x) ImageDenoise_reconstruct(x, SMALL.Problem);

%%
%   Initialising solver structure
%   Setting solver structure fields (toolbox, name, param, solution,
%   reconstructed and time) to zero values

SMALL.solver(1)=SMALL_init_solver;

% Defining the parameters needed for image denoising

SMALL.solver(1).toolbox='ompbox';
SMALL.solver(1).name='omp2';
SMALL.solver(1).param=struct(...
    'epsilon',Edata,...
    'maxatoms', maxatoms); 

%   Denoising the image - find the sparse solution in the learned
%   dictionary for all patches in the image and the end it uses
%   reconstruction function to reconstruct the patches and put them into a
%   denoised image

SMALL.solver(1)=SMALL_solve(SMALL.Problem, SMALL.solver(1));

%   Show PSNR after reconstruction

SMALL.solver(1).reconstructed.psnr

%%
%   For comparison purposes we will denoise image with Majorization
%   Minimization method
%   

%   Initialising solver structure
%   Setting solver structure fields (toolbox, name, param, solution,
%   reconstructed and time) to zero values

SMALL.solver(2)=SMALL_init_solver;

% Defining the parameters needed for image denoising

SMALL.solver(2).toolbox='ompbox';
SMALL.solver(2).name='omp2';
SMALL.solver(2).param=struct(...
    'epsilon',Edata,...
    'maxatoms', maxatoms); 

%   Initialising Dictionary structure
%   Setting Dictionary structure fields (toolbox, name, param, D and time)
%   to zero values

SMALL.DL(2)=SMALL_init_DL('MMbox', 'MM_cn', '', 1);


%   Defining the parameters for MOD
%   In this example we are learning 256 atoms in 20 iterations, so that
%   every patch in the training set can be represented with target error in
%   L2-norm (EData)
%   Type help ksvd in MATLAB prompt for more options.


SMALL.DL(2).param=struct(...
    'solver', SMALL.solver(2),...
    'initdict', SMALL.Problem.initdict,...
    'dictsize', SMALL.Problem.p,...
    'iternum', 20,...
    'iterDictUpdate', 1000,...
    'epsDictUpdate', 1e-7,...
    'cvset',0,...
    'show_dict', 0);

%   Learn the dictionary

SMALL.DL(2) = SMALL_learn(SMALL.Problem, SMALL.DL(2));

%   Set SMALL.Problem.A dictionary
%   (backward compatiblity with SPARCO: solver structure communicate
%   only with Problem structure, ie no direct communication between DL and
%   solver structures)

SMALL.Problem.A = SMALL.DL(2).D;
SMALL.Problem.reconstruct = @(x) ImageDenoise_reconstruct(x, SMALL.Problem);

%   Denoising the image - find the sparse solution in the learned
%   dictionary for all patches in the image and the end it uses
%   reconstruction function to reconstruct the patches and put them into a
%   denoised image

SMALL.solver(2)=SMALL_solve(SMALL.Problem, SMALL.solver(2));



% show results %

SMALL_ImgDeNoiseResult(SMALL);

results(noise_ind,im_num).psnr.ksvd=SMALL.solver(1).reconstructed.psnr;
results(noise_ind,im_num).psnr.odct=SMALL.solver(2).reconstructed.psnr;
results(noise_ind,im_num).vmrse.ksvd=SMALL.solver(1).reconstructed.vmrse;
results(noise_ind,im_num).vmrse.odct=SMALL.solver(2).reconstructed.vmrse;
results(noise_ind,im_num).ssim.ksvd=SMALL.solver(1).reconstructed.ssim;
results(noise_ind,im_num).ssim.odct=SMALL.solver(2).reconstructed.ssim;


results(noise_ind,im_num).time.ksvd=SMALL.solver(1).time+SMALL.DL(1).time;

%clear SMALL;
end
end
% save results.mat results
