%%  Dictionary Learning for Automatic Music Transcription - KSVD vs SPAMS
%   
%  
%   This file contains an example of how SMALLbox can be used to test diferent
%   dictionary learning techniques in Automatic Music Transcription problem.
%   It calls generateAMT_Learning_Problem that will let you to choose midi,
%   wave or mat file to be transcribe. If file is midi it will be first
%   converted to wave and original midi file will be used for comparison with
%   results of dictionary learning and reconstruction.
%   The function will generarte the Problem structure that is used to learn
%   Problem.p notes spectrograms from training set Problem.b using
%   dictionary learning technique defined in DL structure.
%   Two dictionary learning techniques were compared:
%
%   -   KSVD - M. Elad, R. Rubinstein, and M. Zibulevsky, "Efficient
%              Implementation of the K-SVD Algorithm using Batch Orthogonal
%              Matching Pursuit", Technical Report - CS, Technion, April 2008.
%
%   -   MMDL - M. Yaghoobi, T. Blumensath and M. Davies, "Dictionary Learning
%              for Sparse Approximations with the Majorization Method", IEEE 
%              Trans. on Signal Processing, Vol. 57, No. 6, pp 2178-2191,
%              2009.

%
%   Centre for Digital Music, Queen Mary, University of London.
%   This file copyright 2011 Ivan Damnjanovic.
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License as
%   published by the Free Software Foundation; either version 2 of the
%   License, or (at your option) any later version.  See the file
%   COPYING included with this distribution for more information.
%%

clear;


%   Defining Automatic Transcription of Piano tune as Dictionary Learning
%   Problem

SMALL.Problem = generateAMTProblem('',2048,0.75);

%%
%   Use KSVD Dictionary Learning Algorithm to Learn 88 notes (defined in
%   SMALL.Problem.p) using sparsity constrain only

%   Initialising Dictionary structure
%   Setting Dictionary structure fields (toolbox, name, param, D and time) 
%   to zero values

SMALL.DL(1)=SMALL_init_DL();

%   Defining fields needed for dictionary learning

SMALL.DL(1).toolbox = 'KSVD';
SMALL.DL(1).name = 'ksvd';
%   Defining the parameters for KSVD
%   In this example we are learning 88 atoms in 100 iterations, so that
%   every frame in the training set can be represented with maximum Tdata
%   dictionary elements. Type help ksvd in MATLAB prompt for more options.

SMALL.DL(1).param=struct(...
    'Tdata', 5,...
    'dictsize', SMALL.Problem.p,...
    'iternum', 50);

% Learn the dictionary

SMALL.DL(1) = SMALL_learn(SMALL.Problem, SMALL.DL(1));

%   Set SMALL.Problem.A dictionary and reconstruction function 
%   (backward compatiblity with SPARCO: solver structure communicate
%   only with Problem structure, ie no direct communication between DL and
%   solver structures)

SMALL.Problem.A = SMALL.DL(1).D;
SMALL.Problem.reconstruct = @(x) AMT_reconstruct(x, SMALL.Problem);

%%
%   Initialising solver structure
%   Setting solver structure fields (toolbox, name, param, solution, 
%   reconstructed and time) to zero values
%   As an example, SPAMS (Julien Mairal 2009) implementation of LARS
%   algorithm is used for representation of training set in the learned
%   dictionary.

SMALL.solver(1)=SMALL_init_solver;

%   Defining the parameters needed for sparse representation

SMALL.solver(1).toolbox='SMALL';
SMALL.solver(1).name='SMALL_pcgp';

%   Here we use mexLasso mode=2, with lambda=2, lambda2=0 and positivity
%   constrain (type 'help mexLasso' for more information about modes):
%   
%   min_{alpha_i} (1/2)||x_i-Dalpha_i||_2^2 + lambda||alpha_i||_1 + (1/2)lambda2||alpha_i||_2^2

SMALL.solver(1).param='20, 1e-2';
% struct(...
%     'lambda', 2,...
%     'pos', 1,...
%     'mode', 2);

%   Call SMALL_soolve to represent the signal in the given dictionary. 
%   As a final command SMALL_solve will call above defined reconstruction
%   function to reconstruct the training set (Problem.b) in the learned 
%   dictionary (Problem.A)

SMALL.solver(1)=SMALL_solve(SMALL.Problem, SMALL.solver(1));

%%
%   Analysis of the result of automatic music transcription. If groundtruth
%   exists, we can compare transcribed notes and original and get usual
%   True Positives, False Positives and False Negatives measures.

if ~isempty(SMALL.Problem.notesOriginal)
    AMT_res(1) = AMT_analysis(SMALL.Problem, SMALL.solver(1));
end



%%
% %   Here we solve the same problem using non-negative sparse coding with 
% %   SPAMS online dictionary learning (Julien Mairal 2009)
% %
%   Initialising solver structure
%   Setting solver structure fields (toolbox, name, param, solution, 
%   reconstructed and time) to zero values
%   As an example, SPAMS (Julien Mairal 2009) implementation of LARS
%   algorithm is used for representation of training set in the learned
%   dictionary.

SMALL.solver(2)=SMALL_init_solver;

%   Defining the parameters needed for sparse representation

SMALL.solver(2).toolbox='SPAMS';
SMALL.solver(2).name='mexLasso';

%   Here we use mexLasso mode=2, with lambda=3, lambda2=0 and positivity
%   constrain (type 'help mexLasso' for more information about modes):
%   
%   min_{alpha_i} (1/2)||x_i-Dalpha_i||_2^2 + lambda||alpha_i||_1 + (1/2)lambda2||alpha_i||_2^2

SMALL.solver(2).param=struct('lambda', 3, 'pos', 1, 'mode', 2);


% You can also test ALPS, IST from MMbox or any other solver, but results
% are not as good as SPAMS
%
% %   Initialising solver structure
% %   Setting solver structure fields (toolbox, name, param, solution,
% %   reconstructed and time) to zero values
% 
% SMALL.solver(2)=SMALL_init_solver;
% 
% % Defining the parameters needed for image denoising
% 
% SMALL.solver(2).toolbox='ALPS';
% SMALL.solver(2).name='AlebraicPursuit';
% 
% SMALL.solver(2).param=struct(...
%     'sparsity', 10,... 
%     'memory', 1,...
%     'mode', 6,...
%     'iternum', 100,... 
%     'tau',-1,...
%     'tolerance', 1e-14',...
%     'verbose',1); 

% %   Initialising Dictionary structure
% %   Setting Dictionary structure fields (toolbox, name, param, D and time)
% %   to zero values
% %   Initialising solver structure
% %   Setting solver structure fields (toolbox, name, param, solution,
% %   reconstructed and time) to zero values
% 
% SMALL.solver(2)=SMALL_init_solver;
% 
% % Defining the parameters needed for image denoising
% 
% SMALL.solver(2).toolbox='MMbox';
% SMALL.solver(2).name='mm1';
% SMALL.solver(2).param=struct(...
%     'lambda',50,...
%     'iternum',1000,...
%     'map',0); 

SMALL.DL(2)=SMALL_init_DL('MMbox', 'MM_cn', '', 1);


%   Defining the parameters for Majorization Minimization dictionary update
%
%   In this example we are learning 88 atoms in 200 iterations, so that


SMALL.DL(2).param=struct(...
    'solver', SMALL.solver(2),...
    'initdict', SMALL.Problem.A,...
    'dictsize', SMALL.Problem.p,...
    'iternum', 200,...
    'iterDictUpdate', 1000,...
    'epsDictUpdate', 1e-7,...
    'cvset',0,...
    'show_dict', 0);


%   Learn the dictionary

SMALL.DL(2) = SMALL_learn(SMALL.Problem, SMALL.DL(2));

%   Set SMALL.Problem.A dictionary and reconstruction function 
%   (backward compatiblity with SPARCO: solver structure communicate
%   only with Problem structure, ie no direct communication between DL and
%   solver structures)

SMALL.Problem.A = SMALL.DL(2).D;
SMALL.Problem.reconstruct=@(x) AMT_reconstruct(x, SMALL.Problem);


%   Call SMALL_soolve to represent the signal in the given dictionary. 
%   As a final command SMALL_solve will call above defined reconstruction
%   function to reconstruct the training set (Problem.b) in the learned 
%   dictionary (Problem.A)

SMALL.solver(2)=SMALL_solve(SMALL.Problem, SMALL.solver(2));


%   Analysis of the result of automatic music transcription. If groundtruth
%   exists, we can compare transcribed notes and original and get usual
%   True Positives, False Positives and False Negatives measures.

if ~isempty(SMALL.Problem.notesOriginal)
    AMT_res(2) = AMT_analysis(SMALL.Problem, SMALL.solver(2));
end


% Plot results and save midi files

if ~isempty(SMALL.Problem.notesOriginal)
    figAMT = SMALL_AMT_plot(SMALL, AMT_res);
else
    figAMT = figure('Name', 'Automatic Music Transcription KSVD vs SPAMS');
    subplot(2,1,1); plot(SMALL.solver(1).reconstructed.notes(:,5), SMALL.solver(1).reconstructed.notes(:,3), 'kd ');
            title (sprintf('%s dictionary in %.2f s', SMALL.DL(1).name, SMALL.DL(1).time));
                xlabel('Time');
                    ylabel('Note Number');
    subplot(2,1,2); plot(SMALL.solver(2).reconstructed.notes(:,5), SMALL.solver(2).reconstructed.notes(:,3), 'b* ');
            title (sprintf('%s dictionary in %.2f s', SMALL.DL(2).name, SMALL.DL(2).time));
                xlabel('Time');
                    ylabel('Note Number');
end

FS=filesep;

[pathstr1, name, ext] = fileparts(which('SMALLboxSetup.m'));
cd([pathstr1,FS,'results']);

[filename,pathname] = uiputfile({' *.mid;' },'Save KSVD result midi');
if filename~=0 writemidi(SMALL.solver(1).reconstructed.midi, [pathname,FS,filename]);end

[filename,pathname] = uiputfile({' *.mid;' },'Save SPAMS result midi');
if filename~=0 writemidi(SMALL.solver(2).reconstructed.midi, [pathname,FS,filename]);end

[filename,pathname] = uiputfile({' *.fig;' },'Save KSVD vs SPAMS AMT figure');
if filename~=0 saveas(figAMT, [pathname,FS,filename]);end



