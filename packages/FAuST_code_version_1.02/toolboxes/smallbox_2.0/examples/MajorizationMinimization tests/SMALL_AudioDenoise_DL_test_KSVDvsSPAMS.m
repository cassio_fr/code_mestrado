%% DICTIONARY LEARNING FOR AUDIO DENOISING 
% This file contains an example of how SMALLbox can be used to test different
% dictionary learning techniques in Audio Denoising problem.
% It calls generateAudioDenoiseProblem that will let you to choose audio file,
% add noise and use noisy audio to generate training set for dictionary
% learning.
   
%
%   Centre for Digital Music, Queen Mary, University of London.
%   This file copyright 2011 Ivan Damnjanovic.
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License as
%   published by the Free Software Foundation; either version 2 of the
%   License, or (at your option) any later version.  See the file
%   COPYING included with this distribution for more information.
%   
%%

clear;

% Defining Audio Denoising Problem as Dictionary Learning
% Problem

SMALL.Problem = generateAudioDenoiseProblem('male01_8kHz',0.1,512,1/128,'','','',4);

%% 
%   Initialising solver structure
%   Setting solver structure fields (toolbox, name, param, solution,
%   reconstructed and time) to zero values

SMALL.solver(1)=SMALL_init_solver('MMbox', 'mm1', '', 1);

% Defining the parameters needed for image denoising

SMALL.solver(1).param=struct(...
    'lambda', 0.2,...
    'epsilon', 3*10^-4,...
    'iternum',10); 

%   Initialising Dictionary structure
%   Setting Dictionary structure fields (toolbox, name, param, D and time)
%   to zero values

SMALL.DL(1)=SMALL_init_DL('MMbox', 'MM_cn', '', 1);


%   Defining the parameters for MOD
%   In this example we are learning 256 atoms in 20 iterations, so that
%   every patch in the training set can be represented with target error in
%   L2-norm (EData)
%   Type help ksvd in MATLAB prompt for more options.


SMALL.DL(1).param=struct(...
    'solver', SMALL.solver(1),...
    'initdict', SMALL.Problem.initdict,...
    'dictsize', SMALL.Problem.p,...
    'iternum', 20,...
    'iterDictUpdate', 10,...
    'epsDictUpdate', 10^-7,...
    'cvset',0,...
    'show_dict', 0);

%   Learn the dictionary

SMALL.DL(1) = SMALL_learn(SMALL.Problem, SMALL.DL(1));

%   Set SMALL.Problem.A dictionary
%   (backward compatiblity with SPARCO: solver structure communicate
%   only with Problem structure, ie no direct communication between DL and
%   solver structures)

SMALL.Problem.A = SMALL.DL(1).D;
SMALL.Problem.reconstruct = @(x) AudioDenoise_reconstruct(x, SMALL.Problem);
%   Denoising the image - find the sparse solution in the learned
%   dictionary for all patches in the image and the end it uses
%   reconstruction function to reconstruct the patches and put them into a
%   denoised image

SMALL.solver(1)=SMALL_solve(SMALL.Problem, SMALL.solver(1));

%%
%%
% %  sparse coding using SPAMS online dictionary learning
% 

SMALL.DL(2)=SMALL_init_DL();
SMALL.DL(2).toolbox = 'SPAMS';
SMALL.DL(2).name = 'mexTrainDL';
SMALL.DL(2).param=struct('D', SMALL.Problem.initdict, 'K', SMALL.Problem.p, 'lambda', 0.2, 'iter', 200, 'mode', 3, 'modeD', 0);


SMALL.DL(2) = SMALL_learn(SMALL.Problem, SMALL.DL(2));

% Defining Reconstruction function

SMALL.Problem.A = SMALL.DL(2).D;


%%
% Initialising solver structure 
% Setting toolbox, name, param, solution, reconstructed and time to zero values

SMALL.solver(2)=SMALL_init_solver;

% Defining the parameters needed for sparse representation

SMALL.solver(2).toolbox='ompbox';
SMALL.solver(2).name='omp2';
SMALL.solver(2).param=struct(...
    'epsilon',0.2,...
    'maxatoms', 128); 
% Represent Training set in the learned dictionary 

SMALL.solver(2)=SMALL_solve(SMALL.Problem, SMALL.solver(2));




%%
% Plot results and save midi files

% show results %


SMALL_AudioDeNoiseResult(SMALL);
    