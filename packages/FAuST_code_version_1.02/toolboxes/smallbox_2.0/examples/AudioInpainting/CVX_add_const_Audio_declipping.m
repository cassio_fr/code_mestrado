function solver=CVX_add_const_Audio_declipping(Problem, solver, idxFrame)

%% CVX additional constrains 
    % Clipping level: take the analysis window into account
    % clipping level detection
    
    signal = Problem.b1;
    signalFull = Problem.b(:,idxFrame);
    Dict = Problem.A;
    DictFull = Problem.B;
    Clipped = ~Problem.M(:,idxFrame);
    W=1./sqrt(diag(Dict'*Dict));
    
    idxCoeff = find(solver.solution~=0);
    
    solution = solver.solution;
    
    wa = Problem.wa(Problem.windowSize); % analysis window
    
    
    clippingLevelEst = max(abs(signal./wa(~Clipped)'));

    idxPos = find(signalFull>=0 & Clipped);
    idxNeg = find(signalFull<0  & Clipped);

    DictPos=DictFull(idxPos,:);
    DictNeg=DictFull(idxNeg,:);
    
    
    wa_pos = wa(idxPos);
    wa_neg = wa(idxNeg);
    b_ineq_pos = wa_pos(:)*clippingLevelEst;
    b_ineq_neg = -wa_neg(:)*clippingLevelEst;
    if isfield(Problem,'Upper_Limit') && ~isempty(Problem.Upper_Limit)
        b_ineq_pos_upper_limit = wa_pos(:)*Problem.Upper_Limit;
        b_ineq_neg_upper_limit = -wa_neg(:)*Problem.Upper_Limit;
    else
        b_ineq_pos_upper_limit = Inf;
        b_ineq_neg_upper_limit = -Inf;
    end
    
    
    j = length(idxCoeff);
    
    if isinf(b_ineq_pos_upper_limit)
        %% CVX code
        cvx_begin
        cvx_quiet(true)
        variable solution(j)
        minimize(norm(Dict(:,idxCoeff)*solution-signal))
        subject to
        DictPos(:,idxCoeff)*(W(idxCoeff).*solution) >= b_ineq_pos
        DictNeg(:,idxCoeff)*(W(idxCoeff).*solution) <= b_ineq_neg
        cvx_end
        if cvx_optval>1e3
            cvx_begin
            cvx_quiet(true)
            variable solution(j)
            minimize(norm(Dict(:,idxCoeff)*solution-xObs))
            cvx_end
        end
    else
        %% CVX code
        cvx_begin
        cvx_quiet(true)
        variable solution(j)
        minimize(norm(Dict(:,idxCoeff)*solution-signal))
        subject to
        DictPos(:,idxCoeff)*(W(idxCoeff).*solution) >= b_ineq_pos
        DictNeg(:,idxCoeff)*(W(idxCoeff).*solution) <= b_ineq_neg
        DictPos(:,idxCoeff)*(W(idxCoeff).*solution) <= b_ineq_pos_upper_limit
        DictNeg(:,idxCoeff)*(W(idxCoeff).*solution) >= b_ineq_neg_upper_limit
        cvx_end
        if cvx_optval>1e3
            cvx_begin
            cvx_quiet(true)
            variable solution(j)
            minimize(norm(Dict(:,idxCoeff)*solution-xObs))
            cvx_end
        end
    end
 
   solver.solution(idxCoeff) = solution;
    
   