function SMALLboxSetup(varargin)
%%  SMALLboxSetup 
% 
%   Will automatically download and install existing toolboxes
%   on sparse representations and dictionary learning
% 
%   For this function an internet connection is required.  
%
%   SMALLbox initialisation

%
%   Centre for Digital Music, Queen Mary, University of London.
%   This file copyright 2009 Ivan Damnjanovic, Matthew Davies.
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License as
%   published by the Free Software Foundation; either version 2 of the
%   License, or (at your option) any later version.  See the file
%   COPYING included with this distribution for more information.
%   
%%
clc;

FS=filesep;

fprintf('\n ********************************************************************');
fprintf('\n\n This script will install the SMALLbox Evaluation Framework v.1.1');
fprintf('\n\n It contains the following toolboxes:');
fprintf('\n Sparco version 1.2');
fprintf('\n SPGL1 Toolbox version 1.7 ');
fprintf('\n SparseLab Toolbox version 2.1');
fprintf('\n Sparsify Toolbox version 0.4');
fprintf('\n GPSR Toolbox version 5.0');
fprintf('\n OMPbox version 10');
fprintf('\n OMPSbox version 1');
fprintf('\n KSVDbox version 13');
fprintf('\n KSVDSbox version 11');
fprintf('\n Rice Wavelet toolbox');
fprintf('\n CVX version 1.21');
fprintf('\n\n ********************************************************************');

fprintf('\n\n The toolbox will be installed in: ');
fprintf('\n %s%s\n',pwd,FS);
fprintf('\n ********************************************************************');
fprintf('\n\n IMPORTANT: To successfully install all toolboxes');
fprintf('\n you will need to have MEX setup to compile C files.');
fprintf('\n\n If this is not already setup, please type "n" to exit and then ');
fprintf('\n run "mex -setup" or type "help mex" in the MATLAB command prompt.');
fprintf('\n\n ********************************************************************');


fprintf('\n ********************************************************************');
fprintf('\n\n IMPORTANT: YOU MUST HAVE AN INTERNET CONNECTION');
fprintf('\n YOU CANNOT INSTALL SMALLBOX WITHOUT ONE!');
fprintf('\n\n ********************************************************************');
install_ack = input('\n\n Do you wish to continue: ([y]/n)? ','s');

if strcmp(install_ack,'"n"'),
  install_ack = 'n';
end

if install_ack == 'n',
  return;
else
  fprintf('\n\n Installation now beginning...');

end

global SMALL_path;
SMALL_path=fileparts(mfilename('fullpath'));
SMALL_p=genpath(SMALL_path);
addpath(SMALL_p);

%%


if ~exist('sparcoSetup.m','file')
    fprintf('\n ******************************************************************');
    fprintf('\n\n Initialising SPARCO and Rice Wavelet Toolbox Setup');
    
    Sparco_path = [SMALL_path,FS,'toolboxes',FS,'SPARCO'];
    if exist([Sparco_path, FS, 'sparco-1.2.zip'],'file'),
        Sparco_zip=[Sparco_path, FS, 'sparco-1.2.zip'];
    else
        Sparco_zip='http://www.cs.ubc.ca/labs/scl/sparco/downloads.php?filename=sparco-1.2.zip';
        fprintf('\n\n Downloading toolbox, please be patient\n\n');
    end
    unzip(Sparco_zip,Sparco_path);
    Sparco_p=genpath(Sparco_path);
    addpath(Sparco_p);
    cd(SMALL_path)
    
    
    
    if exist('curvelab.pdf','file')
        crvroot = fileparts(which('curvelab.pdf'));
        addtopath(crvroot,'fdct_usfft_matlab');
        addtopath(crvroot,'fdct_wrapping_matlab');
        addtopath(crvroot,'fdct_wrapping_cpp/mex');
        addtopath(crvroot,'fdct3d/mex');
    else
        fprintf(['\nWarning: CurveLab is not in the path. Sparco Problems 50-51 ' ...
            'will not work.\n\n']);
    end
    
    
    cd(SMALL_path);

    fprintf('SPARCO Installation Successful!\n');
else
    fprintf('\n ******************************************************************');
    fprintf('\n\n SPARCO and Rice Wavelet Toolbox are already installed');
end
%%
    %removing Rice Wavelet tollbox provided with SPARCO from the path, so
    %the newer version provided with SMALLbox is used
    
    rmpath([SMALL_path,FS,'toolboxes',FS,'SPARCO',FS,'sparco-1.2',FS,'tools',FS,'rwt']);

if ~exist('spgsetup.m','file')
    fprintf('\n ******************************************************************');
    fprintf('\n\n Initialising SPGL1 Setup');
    
    try
        SPGL1_path = [SMALL_path,FS,'toolboxes',FS,'SPGL1'];
        if exist([SPGL1_path, FS, 'spgl1-1.7.zip'],'file'),
            SPGL1_zip=[SPGL1_path, FS, 'spgl1-1.7.zip'];
        else
            SPGL1_zip='http://www.cs.ubc.ca/~mpf/downloads/spgl1-1.7.zip';
            fprintf('\n\n Downloading toolbox, please be patient\n\n');
        end
        unzip(SPGL1_zip,SPGL1_path);
        SPGL1_p=genpath(SPGL1_path);
        addpath(SPGL1_p);
        
        
        cd([SPGL1_path,FS,'spgl1-1.7']);
        fprintf('Compiling SPGL1 MEX interfaces ...');
        try
            spgsetup;
            fprintf('\n SPGL1 Installation Successful!\n');
        catch
            warning('Could not compile SPGL1 MEX interfaces.');
        end
    catch
        fprintf('\n SPGL1 Installation Failed\n');
    end
    cd(SMALL_path);
else
    fprintf('\n ******************************************************************');
    fprintf('\n\n SPGL1 is already installed');
end
%%

if ~exist('SparsePath.m','file')
    fprintf('\n ******************************************************************');
    fprintf('\n\n Initialising SparseLab Setup');
    
    try
        SL_path = [pwd,FS,'toolboxes',FS,'SparseLab'];
        if exist([SL_path, FS, 'SparseLab21-Core.zip'],'file'),
            SL_zip=[SL_path, FS, 'SparseLab21-Core.zip'];
        else
            SL_zip='http://sparselab.stanford.edu/SparseLab_files/Download_files/SparseLab21-Core.zip';
            fprintf('\n\n Downloading toolbox, please be patient\n\n');
        end
        unzip(SL_zip,SL_path);
        SL_p=genpath(SL_path);
        addpath(SL_p);
        fprintf('\n SparseLab Installation Successful!\n');
    catch
        fprintf('\n SparseLab Installation Failed\n');
        cd(SMALL_path);
    end
    cd(SMALL_path);
else
    fprintf('\n ******************************************************************');
    fprintf('\n\n SparseLab is already installed');
end
%%

if ~exist('greed_pcgp.m','file')
    fprintf('\n ******************************************************************');
    fprintf('\n\n Initialising Sparsify Setup');
    
    try
        Sparsify_path = [pwd,FS,'toolboxes',FS,'Sparsify'];
        if exist([Sparsify_path, FS, 'sparsify_0_4.zip'],'file'),
            Sparsify_zip=[Sparsify_path, FS, 'sparsify_0_4.zip'];
        else
            Sparsify_zip='http://www.personal.soton.ac.uk/tb1m08/sparsify/sparsify_0_5.zip';
            fprintf('\n\n Downloading toolbox, please be patient\n\n');
        end
        unzip(Sparsify_zip,Sparsify_path);
        Sparsify_p=genpath(Sparsify_path);
        addpath(Sparsify_p);
        fprintf('\n Sparsify Installation Successful\n');
    catch
        fprintf('\n Sparsify Installation Failed\n');
    end
    cd(SMALL_path);
else
    fprintf('\n ******************************************************************');
    fprintf('\n\n Sparsify is already installed');
end
%%
if ~exist('GPSR_Basic.m','file')
    fprintf('\n ******************************************************************');
    fprintf('\n\n Initialising GPSR Setup');
    
    try
        GPSR_path = [pwd,FS,'toolboxes',FS,'GPSR'];
        if exist([GPSR_path, FS, 'GPSR_6.0.zip'],'file'),
            GPSR_zip=[GPSR_path, FS,'GPSR_6.0.zip'];
        else
            GPSR_zip='http://www.lx.it.pt/~mtf/GPSR/GPSR_6.0.zip';
            fprintf('\n\n Downloading toolbox, please be patient\n\n');
        end
        unzip(GPSR_zip,GPSR_path);
        
        GPSR_p=genpath(GPSR_path);
        addpath(GPSR_p);
        fprintf('\n GPSR Installation Successful\n');
    catch
        fprintf('\n GPSR Installation Failed');
    end
    cd(SMALL_path);
else
    fprintf('\n ******************************************************************');
    fprintf('\n\n GPSR is already installed');
end
%%
if ~exist('cvx_setup.m','file')
    fprintf('\n ******************************************************************');
    fprintf('\n\n Initialising CVX Setup');
    
    try
        CVX_path = [pwd,FS,'toolboxes',FS,'CVX'];
        machine=computer;
        if (strcmp(machine,'PCWIN')||strcmp(machine,'PCWIN64'))
            if exist([CVX_path, FS, 'cvx.zip'],'file'),
                CVX_zip=[CVX_path, FS,'cvx.zip'];
            else
                CVX_zip='http://cvxr.com/cvx/cvx.zip';
                fprintf('\n\n Downloading toolbox, please be patient\n\n');
            end
            unzip(CVX_zip,CVX_path);
        else
            if exist([CVX_path, FS, 'cvx.tar.gz'],'file'),
                CVX_tar=[CVX_path, FS,'cvx.tar.gz'];
            else
                CVX_tar='http://cvxr.com/cvx/cvx.tar.gz';
                fprintf('\n\n Downloading toolbox, please be patient\n\n');
            end
            untar(CVX_tar,CVX_path);
        end
        cd([CVX_path,FS,'cvx']);
        fprintf('\n\n Running cvx_setup.m script \n\n');
        cvx_setup;
        fprintf('\n\n Ignore the cvx_setup note about adding the path. It is done automatically in SMALLbox  \n\n');
        CVX_p=genpath(CVX_path);
        addpath(CVX_p);
        fprintf('\n CVX Installation Successful\n');
    catch
        fprintf('\n CVX Installation Failed');
    end
    cd(SMALL_path);
else
    fprintf('\n ******************************************************************');
    fprintf('\n\n CVX is already installed');
end

%%
%% KSVD utils setup

if ~(exist('addtocols')==3)
    cd([SMALL_path,FS,'util',FS,'ksvd utils']);
    make
    cd(SMALL_path);
end

%% fast omp for Gabor dictionary 

if ~(exist('omp2Gabor')==3)
    cd([SMALL_path,FS,'solvers', FS,'SMALL_ompGabor']);
    make
cd(SMALL_path);
end
%%
if ~exist('ksvdver.m','file')
    fprintf('\n ******************************************************************');
    fprintf('\n\n Initialising OMPbox and KSVDBox Setup');
    
    try
        KSVD_path = [pwd,FS,'toolboxes',FS,'KSVD'];
        if exist([KSVD_path, FS, 'ompbox10.zip'],'file'),
            omp_zip=[KSVD_path, FS, 'ompbox10.zip'];
        else
            omp_zip='http://www.cs.technion.ac.il/%7Eronrubin/Software/ompbox10.zip';
            fprintf('\n\n Downloading toolbox, please be patient\n\n');
        end
        unzip(omp_zip,[KSVD_path, FS, 'ompbox']);
        
        cd([KSVD_path, FS, 'ompbox', FS, 'private']);
        make;
        cd(SMALL_path);
        
        if exist([KSVD_path, FS, 'ksvdbox13.zip'],'file'),
            KSVD_zip=[KSVD_path, FS, 'ksvdbox13.zip'];
        else
            KSVD_zip='http://www.cs.technion.ac.il/%7Eronrubin/Software/ksvdbox13.zip';
            fprintf('\n\n Downloading toolbox, please be patient\n\n');
        end
        unzip(KSVD_zip,[KSVD_path, FS, 'ksvdbox']);
        cd([KSVD_path, FS, 'ksvdbox', FS, 'private']);
        make;
        cd(SMALL_path);
        KSVD_p=genpath(KSVD_path);
        addpath(KSVD_p);
        fprintf('\n KSVDBox and OMPBox Installation Successful\n');
    catch
        fprintf('\n KSVDBox and OMPBox Installation Failed');
        cd(SMALL_path);
    end
else
    fprintf('\n ******************************************************************');
    fprintf('\n\n KSVD is already installed');
end
%%
if ~exist('ksvdsver.m','file')
    fprintf('\n ******************************************************************');
    fprintf('\n\n Initialising OMPSbox and KSVDSBox Setup');
    
    try
        KSVDS_path = [pwd,FS,'toolboxes',FS,'KSVDS'];
        if exist([KSVDS_path, FS, 'ompsbox1.zip'],'file'),
            omps_zip=[KSVDS_path, FS, 'ompsbox1.zip'];
        else
            omps_zip='http://www.cs.technion.ac.il/%7Eronrubin/Software/ompsbox1.zip';
            fprintf('\n\n Downloading toolbox, please be patient\n\n');
        end
        unzip(omps_zip,[KSVDS_path, FS, 'ompsbox']);
        
        cd([KSVDS_path, FS, 'ompsbox', FS, 'private']);
        make;
        cd(SMALL_path);
        
        if exist([KSVDS_path, FS, 'ksvdsbox11.zip'],'file'),
            KSVDS_zip=[KSVDS_path, FS, 'ksvdsbox11.zip'];
        else
            KSVDS_zip='http://www.cs.technion.ac.il/%7Eronrubin/Software/ksvdsbox11.zip';
            fprintf('\n\n Downloading toolbox, please be patient\n\n');
        end
        unzip(KSVDS_zip,[KSVDS_path, FS, 'ksvdsbox']);
        cd([KSVDS_path, FS, 'ksvdsbox', FS, 'private']);
        make;
        cd(SMALL_path);
        KSVDS_p=genpath(KSVDS_path);
        addpath(KSVDS_p);
        fprintf('\n KSVDSbox and OMPSbox Installation Successful\n');
    catch
        fprintf('\n KSVDSbox and OMPSbox Installation Failed');
        cd(SMALL_path);
    end
else
    fprintf('\n ******************************************************************');
    fprintf('\n\n KSVDS is already installed');
end
%%

if ~exist('mexTrainDL.m','file')
    fprintf('\n ******************************************************************');
    fprintf('\n\n If you want to use SMALLbox with SPAMS On-line Dictionary Learning please go to:');
     fprintf('\n http://www.di.ens.fr/willow/SPAMS/index.html');
    fprintf('\n and follow the instructions. The License prohibits redistribution of the SPAMS.');
    
else
    fprintf('\n ******************************************************************');
    fprintf('\n\n SPAMS On-line Dictionary Learning is already installed');
end;
%%


if ~exist('midiInfo.m','file')
try
    matlab_midi_path = [pwd,FS,'util',FS,'matlab_midi'];
    if exist([matlab_midi_path, FS, 'matlab_midi.zip'],'file'),
        matlab_midi_zip=[SL_path, FS, 'matlab_midi.zip'];
    else
        matlab_midi_zip='http://www.kenschutte.com/static/code/matlab_midi.zip';
        fprintf('\n\n Downloading toolbox, please be patient\n\n');
    end
    unzip(matlab_midi_zip,matlab_midi_path);
    matlab_midi_p=genpath(matlab_midi_path);
    addpath(matlab_midi_p);
    fprintf('\n matlab_midi (http://www.kenschutte.com/midi/) Installation Successful!\n');
catch
    fprintf('\n matlab_midi (http://www.kenschutte.com/midi/) Installation Failed\n');
    cd(SMALL_path);
end

else
    fprintf('\n ******************************************************************');
    fprintf('\n\n matlab_midi (http://www.kenschutte.com/midi/) is already installed');
end



%% RWT setup
if ~(exist('mdwt')==3)
    cd([SMALL_path, FS, 'util', FS, 'Rice Wavelet Toolbox'])
    fprintf('Compiling the Rice Wavelet Toolbox MEX interfaces...');
    try
        if exist('mdwt'  ,'file')~=3, mex mdwt.c;   end
        if exist('midwt' ,'file')~=3, mex midwt.c;  end
        if exist('mrdwt' ,'file')~=3, mex mrdwt.c;  end
        if exist('mirdwt','file')~=3, mex mirdwt.c; end
        fprintf('Rice Wavelet Toolbox Installation Successful!\n\n');
    catch
        warning('Could not compile Rice Wavelet Toolbox MEX interfaces.\n');
    end
    cd(SMALL_path);
end
%%
fprintf('\n ******************************************************************');
fprintf('\n\n Initialising SMALLbox Examples Setup');

%     % Need to do a bit of temporary housekeeping first.
%     cd(SMALL_path);
%   try
%     cd(['Problems',FS,'private']);
%     if exist('addtocols'  ,'file')~=3,
%     fprintf('\n Compiling MEX interfaces for SMALL examples \n');
%     make;
%     end
%     fprintf('\n SMALLbox Problems Installation Successful! \n');
%   catch
%     fprintf('\n SMALLbox Problems Installation Failed \n');
%   end
%     cd(SMALL_path);



fprintf('\n ******************************************************************');
fprintf('\n\n SMALLbox Installation Complete!');


fprintf('\n\n For more information on the installed toolboxes see');
fprintf('\n\n Sparco: http://www.cs.ubc.ca/labs/scl/sparco/');
fprintf('\n\n SPGL1: http://www.cs.ubc.ca/labs/spgl1/?n=HomePage');
fprintf('\n\n SparseLab: http://sparselab.stanford.edu/   (PLEASE REGISTER SPARSELAB!)');
fprintf('\n\n Sparsify: http://www.see.ed.ac.uk/~tblumens/sparsify/sparsify.html');
fprintf('\n\n GPSR: http://www.lx.it.pt/~mtf/GPSR/');
fprintf('\n\n CVX: http://cvxr.com/cvx/');
fprintf('\n\n OMPbox and KSVDBox: http://www.cs.technion.ac.il/~ronrubin/\n');


% LIST DEMOS FROM EXAMPLE DIRECTORY...

  fprintf('\n Thank you for installing SMALLbox.');
  return;



  
