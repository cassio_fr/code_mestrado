function reconstructed = AudioDeclipping_reconstruct(y, Problem)
%%  Audio declipping Problem reconstruction function
%   
%   This reconstruction function is using sparse representation y 
%   in dictionary Problem.A to reconstruct declipped audio.
%   The output structure has following fields:
%       audioAllSamples  - signal with all samples taken from reconstructed
%                          signal
%       audioOnlyClipped - only clipped samples are reconstructed,
%                          others are taken from original signal
%       snrAll           - psnr of whole signal
%       snrMiss          - psnr of the reconstructed clipped samples
%
%   [1] I. Damnjanovic, M. E. P. Davies, and M. P. Plumbley "SMALLbox - an 
%   evaluation framework for sparse representations and dictionary 
%   learning algorithms," V. Vigneron et al. (Eds.): LVA/ICA 2010, 
%   Springer-Verlag, Berlin, Germany, LNCS 6365, pp. 418-425
%   [2] A. Adler, V. Emiya, M. G. Jafari, M. Elad, R. Gribonval, and M. D.
%   Plumbley, “Audio Inpainting,” submitted to IEEE Trans. Audio, Speech,
%   and Lang. Proc., 2011, http://hal.inria.fr/inria-00577079/en/.

%
%   Centre for Digital Music, Queen Mary, University of London.
%   This file copyright 2009 Ivan Damnjanovic.
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License as
%   published by the Free Software Foundation; either version 2 of the
%   License, or (at your option) any later version.  See the file
%   COPYING included with this distribution for more information.
%%

windowSize = Problem.windowSize;
overlap = Problem.overlap;
ws = Problem.ws(windowSize);
wa = Problem.wa(windowSize);
A = Problem.B;

orig     = Problem.original;
clipped  = Problem.clipped;
clipMask = Problem.clipMask;

% reconstruct audio frames

xFrames = diag(ws)*(A*y);
wNormFrames = (ws.*wa)'*ones(1,size(xFrames,2));

%   overlap and add

rec   = col2imstep(xFrames, size(clipped), [windowSize 1], [windowSize*overlap 1]);
wNorm = col2imstep(wNormFrames, size(clipped), [windowSize 1], [windowSize*overlap 1]); 
wNorm(find(wNorm==0)) = 1; 
recN  = rec./wNorm;

% change only clipped samples

recSignal = orig.*double(~clipMask) + recN.*double(clipMask); 

%% output structure image+psnr %%
reconstructed.audioAllSamples  = recN;
reconstructed.audioOnlyClipped = recSignal;
[reconstructed.snrAll , reconstructed.snrMiss] = SNRInpaintingPerformance(orig, clipped, recSignal, clipMask, 1);

end