function data = generateAudioDeclippingProblem(soundfile, clippingLevel, windowSize, overlap, wa, ws, wd, Dict_fun, redundancyFactor)
%%  Generate Audio Declipping Problem
%   
%   generateAudioDeclippingProblem is part of the SMALLbox [1] and generates
%   Audio declipping is a problem proposed in Audio Inpaining Toolbox and
%   in [2]. 
%
%   The function takes as an optional input 
%       soundfile   - name of the file
%       clippingLevel - (default 0.6)
%       windowSize  - 1D frame size (eg 512)
%       overlap     - ammount of overlaping frames between 0 and 1
%       wa,ws,wd    - analisys, synthesis and dictionary window functions
%       
%       Dict_fun    - function to be used to generate dictionary 
%       redundancyFactor - overcompletness of dictionary (default 2)
%   
%   The function outputs the structure with following fields:
%       original    - original signal
%       clipped     - clipped signal
%       clipMask    - mask indicating clipped samples
%       clippingLevel - (default 0.6)
%       Upper_Limit - maximum value of original data
%       fs          - sample rate of the original signal in Hertz
%       nbits       - the number of bits per sample
%       sigma       - added noise level
%       B           - dictionary to be used for sparse representation
%       M           - measurement matrix (non-clipped data in b)
%       b           - matrix of clipped frames
%       m           - size od dictionary atom 
%       n           - number of frames to be represented
%       p           - number of atoms in dictionary
%       windowSize  - 1D frame size (eg 512)
%       overlap     - ammount of overlaping frames between 0 and 1
%       wa,ws, wd   - analisys, synthesis and dictionary window functions
%
%   [1] I. Damnjanovic, M. E. P. Davies, and M. P. Plumbley "SMALLbox - an 
%   evaluation framework for sparse representations and dictionary 
%   learning algorithms," V. Vigneron et al. (Eds.): LVA/ICA 2010, 
%   Springer-Verlag, Berlin, Germany, LNCS 6365, pp. 418-425
%   [2] A. Adler, V. Emiya, M. G. Jafari, M. Elad, R. Gribonval, and M. D.
%   Plumbley, “Audio Inpainting,” submitted to IEEE Trans. Audio, Speech,
%   and Lang. Proc., 2011, http://hal.inria.fr/inria-00577079/en/.


%   Centre for Digital Music, Queen Mary, University of London.
%   This file copyright 2011 Ivan Damnjanovic.
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License as
%   published by the Free Software Foundation; either version 2 of the
%   License, or (at your option) any later version.  See the file
%   COPYING included with this distribution for more information.
%  
%%
FS=filesep;
TMPpath=pwd;

if ~ exist( 'soundfile', 'var' ) || isempty(soundfile)
    %ask for file name 
    [pathstr1, name, ext] = fileparts(which('SMALLboxSetup.m'));
    cd([pathstr1,FS,'data',FS,'audio']);
    [filename,pathname] = uigetfile({'*.mat; *.mid; *.wav'},'Select a file to transcribe');
    [pathstr, name, ext] = fileparts(filename);
    data.name=name;

    if strcmp(ext,'.mid')
        midi=readmidi(filename);
%         data.notesOriginal=midiInfo(midi);
        y=midi2audio(midi);
        wavwrite(y, 44100, 16, 'temp.wav');
        [x.signal, x.fs, x.nbits]=wavread('temp.wav');
        delete('temp.wav');
    elseif strcmp(ext,'.wav')
%         cd([pathstr1,FS, 'data', FS, 'audio', FS, 'midi']);
%         filename1=[name, '.mid'];
%         if exist(filename1, 'file')
%             midi=readmidi(filename1);
%             data.notesOriginal=midiInfo(midi);
%         end
        cd([pathstr1,FS, 'data', FS, 'audio', FS, 'wav']);
        [x.signal, x.fs, x.nbits]=wavread(filename);
    else
%         cd([pathstr1,FS, 'data', FS, 'audio', FS, 'midi']);
%         filename1=[name, '.mid'];
%         if exist(filename1, 'file')
%             midi=readmidi(filename1);
%             data.notesOriginal=midiInfo(midi);
%         end
        cd([pathstr1,FS, 'data', FS, 'audio', FS, 'mat']);
        x=load([pathname,filename]);
    end
else
    [x.signal, x.fs, x.nbits]=wavread(soundfile);
    [pathstr, name, ext] = fileparts(soundfile);
    data.name=name;
end

if ~ exist( 'clippingLevel', 'var' ) || isempty(clippingLevel), clippingLevel = 0.6; end
if ~ exist( 'windowSize', 'var' ) || isempty(windowSize), windowSize = 256; end
if ~ exist( 'overlap', 'var' ) || isempty(overlap), overlap = 0.5; end
if ~ exist( 'wa', 'var' ) || isempty(wa), wa = @wRect; end % Analysis window
if ~ exist( 'ws', 'var' ) || isempty(ws), ws = @wSine; end % Synthesis window
if ~ exist( 'wd', 'var' ) || isempty(wd), wd = @wRect; end % Weighting window for dictionary atoms

%% preparing signal

[problemData, solutionData] = generateDeclippingProblem(x.signal,clippingLevel);

x_clip = im2colstep(problemData.x,[windowSize 1],[overlap*windowSize 1]);
x_clip= diag(wa(windowSize)) * x_clip;
blkMask=im2colstep(double(~problemData.IMiss),[windowSize 1],[overlap*windowSize 1]);

%% Building dictionary
if ~exist( 'redundancyFactor', 'var' ) || isempty(redundancyFactor), redundancyFactor = 2; end % Weighting window for dictionary atoms
if exist('Dict_fun', 'var')&&~isempty(Dict_fun)
    param=struct('N', windowSize, 'redundancyFactor', redundancyFactor, 'wd', wd);
	data.B = Dict_fun(param);
end

data.b = x_clip;
data.M = blkMask;
data.original = solutionData.xClean;
data.clipped = problemData.x;
data.clipMask = problemData.IMiss;
data.clippingLevel = clippingLevel;
data.windowSize = windowSize;
data.overlap = overlap;
data.ws = ws;
data.wa = wa;
data.wd = wd;

data.fs = x.fs;
data.nbits = x.nbits;
data.Upper_Limit = max(solutionData.xClean);
[data.m, data.n] = size(x_clip);
data.p = windowSize*redundancyFactor; %number of dictionary elements 

cd(TMPpath);

end
