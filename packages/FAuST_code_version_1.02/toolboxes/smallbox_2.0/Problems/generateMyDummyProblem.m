function data=generateMyDummyProblem(varargin) 
%%  Template function that can be used for the sparse problem specification  
%
%   The problem specification function should take training signals and
%   convert them to the matrix of training elements to be used for
%   dictionary learning.
%
%   input arguments:    
%                   optional input parameters
%
%   output arguments: 
%                   data - SPARCO compatible problem structure

%%  Change copyright notice as appropriate:
%   Centre for Digital Music, Queen Mary, University of London.
%   This file copyright 2011 Ivan Damnjanovic.
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License as
%   published by the Free Software Foundation; either version 2 of the
%   License, or (at your option) any later version.  See the file
%   COPYING included with this distribution for more information.
%%
