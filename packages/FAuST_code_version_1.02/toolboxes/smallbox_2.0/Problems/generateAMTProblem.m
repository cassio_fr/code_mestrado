function data = generateAMTProblem(nfft, windowSize, overlap)
%%  Generate Automatic Music Transcription Problem
%   
%   generateAMT_Learning_Problem is a part of the SMALLbox and generates
%   a problem that can be used for comparison of Dictionary Learning/Sparse
%   Representation techniques in automatic music transcription scenario.
%   The function prompts a user for an audio file (mid, wav, mat) reads it
%   and generates a spectrogram given fft size (default nfft=4096), analysis
%   window size (windowSize=2822), and analysis window overlap (overlap =
%   0.5).
%   
%   The output of the function is stucture with following fields:
%       b - matrix with magnitudes of the spectrogram
%       f - vector of frequencies at wihch spectrogram is computed
%       windowSize - analysis window size
%       overlap - analysis window overlap
%       fs - sampling frequency
%       m - number of frequenciy points in spectrogram
%       n - number of time points in the spectrogram
%       p - number of dictionary elements to be learned (eg 88 for piano)
%       notesOriginal - notes of the original audio to be used for
%                       comparison (if midi of the original exists)
%       name - name of the audio file to transcribe

%   Centre for Digital Music, Queen Mary, University of London.
%   This file copyright 2009 Ivan Damnjanovic.
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License as
%   published by the Free Software Foundation; either version 2 of the
%   License, or (at your option) any later version.  See the file
%   COPYING included with this distribution for more information.
%  
%%
FS=filesep;
if ~ exist( 'nfft', 'var' ) || isempty(nfft), nfft = 4096; end
if ~ exist( 'windowSize', 'var' ) || isempty(windowSize), windowSize = 2822; end
if ~ exist( 'overlap', 'var' ) || isempty(overlap), overlap = 0.5; end

%%
%ask for file name
TMPpath=pwd;
[pathstr1, name, ext] = fileparts(which('SMALLboxSetup.m'));
cd([pathstr1,FS,'data',FS,'audio']);
[filename,pathname] = uigetfile({'*.mat; *.mid; *.wav'},'Select a file to transcribe');
[pathstr, name, ext] = fileparts(filename);
data.name=name;

data.notesOriginal=[];

if strcmp(ext,'.mid')
    midi=readmidi(filename);
    data.notesOriginal=midiInfo(midi);
    y=midi2audio(midi);
    wavwrite(y, 44100, 16, 'temp.wav');
    [x.signal, x.fs, x.nbits]=wavread('temp.wav');
    delete('temp.wav');
elseif strcmp(ext,'.wav')
    cd([pathstr1,FS, 'data', FS, 'audio', FS, 'midi']);
    filename1=[name, '.mid'];
    if exist(filename1, 'file')
        midi=readmidi(filename1);
        data.notesOriginal=midiInfo(midi);
    end
    cd([pathstr1,FS, 'data', FS, 'audio', FS, 'wav']);
    [x.signal, x.fs, x.nbits]=wavread(filename);
else
    cd([pathstr1,FS, 'data', FS, 'audio', FS, 'midi']);
    filename1=[name, '.mid'];
    if exist(filename1, 'file')
        midi=readmidi(filename1);
        data.notesOriginal=midiInfo(midi);
    end
    cd([pathstr1,FS, 'data', FS, 'audio', FS, 'mat']);
    x=load([pathname,filename]);
end
%%
[X, frX]=spectrogram(x.signal, hanning(windowSize), overlap*windowSize, nfft, x.fs);
%%
data.b=abs(X);
data.f=frX;
data.windowSize=windowSize;
data.overlap=overlap;
data.fs=x.fs;
data.m=size(X,1);
data.n=size(X,2);

data.p=88; %number of dictionary elements (ie notes to recover)
cd(TMPpath);

end
