function data=generatePierreProblem(src, trg, blocksize, dictsize);
%%  Generate Pierre Villars Problem
%
%   Pierre_Problem is a part of the SMALLbox and generates the problem
%   suggested by Professor Pierre Vandergheynst on the SMALL meeting in 
%   Villars.
%   The function takes as an input:
%   -   src - source image matrix (if not present function promts user for 
%             an image file) ,
%   -   trg - target image matrix (if not present function promts user for 
%             an image file) ,
%   -   blocksize - block (patch) vertical/horizontal dimension (default 8),
%   -   dictsize - dictionary size (default - all patches from target
%   image).
%
%   The output of the function is stucture with following fields:
%   -   srcname - source image name,
%   -   imageSrc - source image matrix,
%   -   trgname - target image name,
%   -   imageTrg - Target image matrix,
%   -   A - dictonary with patches from the source image,
%   -   b - measurement matrix (i.e. patches from target image to be
%           represented in dictionary A,
%   -   m - size of patches (default 25),
%   -   n - number of patches to be represented,
%   -   p - dictionary size,
%   -   blocksize - block size (default [5 5]),
%   -   maxval - maximum value (default - 255)
%   -   sparse - if 1 SMALL_solve will keep solution matrix in sparse form,
%                due to memory constrains.

%
%   Centre for Digital Music, Queen Mary, University of London.
%   This file copyright 2010 Ivan Damnjanovic.
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License as
%   published by the Free Software Foundation; either version 2 of the
%   License, or (at your option) any later version.  See the file
%   COPYING included with this distribution for more information.
%% prompt user for images %%

%   ask for source file name

TMPpath=pwd;
FS=filesep;
if ~ exist( 'src', 'var' ) || isempty(src)
[pathstr1, name, ext] = fileparts(which('SMALLboxSetup.m'));
cd([pathstr1,FS,'data',FS,'images']);
[filename,pathname] = uigetfile({'*.png;'},'Select a source image');
[pathstr, name, ext] = fileparts(filename);
data.srcname=name;
src = imread(filename);
src = double(src);
end;

%   ask for target file name

if ~ exist( 'trg', 'var' ) || isempty(trg)
[filename,pathname] = uigetfile({'*.png;'},'Select a target image');
[pathstr, name, ext] = fileparts(filename);
data.trgname=name;
trg = imread(filename);
trg = double(trg);
end;
cd(TMPpath);

%% set parameters %%

maxval = 255;
if ~ exist( 'blocksize', 'var' ) || isempty(blocksize),blocksize = 5;end

if ~ exist( 'dictsize', 'var' ) || isempty(dictsize),
    dictsize = (size(src,1)-blocksize+1)*(size(src,2)-blocksize+1);
    patch_idx=1:dictsize;
else  
    num_blocks_src=(size(src,1)-blocksize+1)*(size(src,2)-blocksize+1);
    patch_idx=1:floor(num_blocks_src/dictsize):dictsize*floor(num_blocks_src/dictsize);
end

p = ndims(src);
if (p==2 && any(size(src)==1) && length(blocksize)==1)
  p = 1;
end


% blocksize %
if (numel(blocksize)==1)
  blocksize = ones(1,p)*blocksize;
end

%% create dictionary data
S=im2col(src,blocksize,'sliding');

for j= 1:size(S,2)
    S(:,j)=S(:,j)./norm(S(:,j));
end

%% observed patches matrix 
%    that are going to be decomposed on the  dictionary 
T=im2col(trg,blocksize, 'distinct');

%% output structure %%

data.imageSrc = src;
data.imageTrg = trg;
data.A = S(:,patch_idx);
data.b = T;
data.m = size(T,1);
data.n = size(T,2);
data.p = size(data.A,2);
data.blocksize=blocksize;
data.maxval=maxval;

%   keep coefficients matrix in sparse form and do not convert it to full.
%   getting around out of memory problem when converting big matrix from
%   sparse to full... (check SMALL_solve function)
data.sparse=1;


