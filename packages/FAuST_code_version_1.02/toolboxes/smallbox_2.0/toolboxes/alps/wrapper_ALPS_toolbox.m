function [x , numiter, time, x_path] = wrapper_ALPS_toolbox(b, A, param)
%% SMALL wrapper for ALPS toolbox
%
%   Function gets as input
%       b - measurement vector 
%       A - dictionary 
%       K - desired sparsity level
%       param - structure containing additional parameters. These are:
%           - memory        Memory (momentum) of proposed algorithm. 
%                           Possible values are 0,1,'infty' for memoryless,
%                           one memory and infinity memory ALPS,
%                           respectively. Default value: memory = 0.
%           - tol           Early stopping tolerance. Default value: tol =
%                           1-e5.
%           - ALPSiters     Maximum number of algorithm iterations. Default
%                           value: 300.  
%           - mod           According to [1], possible values are
%                           [0,1,2,4,5,6]. This value comes from the binary 
%                           representation of the parameters:
%                           (solveNewtob, gradientDescentx, solveNewtonx), 
%                           which are explained next. Default value = 0.
%           - mu            Variable that controls the step size selection. 
%                           When mu = 0, step size is computed adaptively 
%                           per iteration. Default value: mu = 0. 
%           - tau           Variable that controls the momentum in
%                           non-memoryless case. Ignored in memoryless
%                           case. User can insert as value a function handle on tau.
%                           Description given below. Default value: tau = -1. 
%           - CGiters       Maximum iterations for Conjugate-Gradients method.
%           - CGtol         Tolerance variable for Conjugate-Gradients method.
%           - verbose       verbose = 1 prints out execution infromation.
%   Output:
%       x - sparse solution
%       numiter - number of iterations
%       time - time needed to solve the problem#
%       x_path - matrix containing x after every iteration
%
%   For more details see AlgebraicPursuit.m.

%
%   Centre for Digital Music, Queen Mary, University of London.
%   This file copyright 2011 Ivan Damnjanovic.
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License as
%   published by the Free Software Foundation; either version 2 of the
%   License, or (at your option) any later version.  See the file
%   COPYING included with this distribution for more information.
%   
%%

if isfield(param, 'sparsity')
   sparsity = param.sparsity;
else
   printf('\nAlebraic Pursuit algorithms require desired sparsity level.\n("sparsity" field in solver parameters structure)\n ');
   return
end

if isfield(param, 'memory')
    memory = param.memory;
else
    memory = 0;
end

if isfield(param, 'mode')
    mode = param.mode;
else
    mode = 0;
end
if isfield(param, 'tolerance')
    tolerance = param.tolerance;
else
    tolerance = 1e-5;
end
if isfield(param, 'iternum')
    iternum = param.iternum;
else
    iternum = 300;
end
if isfield(param, 'verbose')
    verbose = param.verbose;
else
    verbose = 0;
end
if isfield(param, 'tau')
    tau = param.tau;
else
    tau = 1/2;
end
if isfield(param, 'useCG')
    useCG = param.useCG;
else
    useCG = 0;
end
if isfield(param, 'mu')
    mu = param.mu;
else
    mu = 0;
end
training_size = size(b,2);
x=zeros(size(A,2),training_size);
for i = 1:training_size
    [x(:,i), numiter, time, x_path] = AlgebraicPursuit(b(:,i), A, sparsity, 'memory', memory,...
        'mode', mode, 'tolerance', tolerance, 'ALPSiterations', iternum, ...
        'verbose', verbose, 'tau', tau, 'useCG', useCG, 'mu', mu);
end