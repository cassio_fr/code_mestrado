function [solveNewtonb, gradientDescentx, solveNewtonx] = configuration(mode)
% Function that maps 'mode' value into (SolveNewtonb, GradientDescentx, SolveNewtonx) configuration

if (mode == 0)
    solveNewtonb = 0;
    gradientDescentx = 0;
    solveNewtonx = 0;
elseif (mode == 1)
    solveNewtonb = 0;
    gradientDescentx = 0;
    solveNewtonx = 1;
elseif (mode == 2)
    solveNewtonb = 0;
    gradientDescentx = 1;
    solveNewtonx = 0;
elseif (mode == 4)
    solveNewtonb = 1;
    gradientDescentx = 0;
    solveNewtonx = 0;
elseif (mode == 5)
    solveNewtonb = 1;
    gradientDescentx = 0;
    solveNewtonx = 1;
elseif (mode == 6)
    solveNewtonb = 1;
    gradientDescentx = 1;
    solveNewtonx = 0;
end;
