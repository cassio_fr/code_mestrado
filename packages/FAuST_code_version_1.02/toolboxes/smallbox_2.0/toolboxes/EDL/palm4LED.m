function [lambda, facts] = palm4LED(params)
%palm4LED. Factorization of a data matrix into multiple factors using PALM.
%  [lambda, facts] = palm4LED(params) runs the PALM algorithm on the
%  specified set of signals (Algorithm 2 of [1]), returning the factors in
%  "facts" and the multiplicative scalar in "lambda".
%
%
%
%  Required fields in PARAMS:
%  --------------------------
%
%    'data' - Training data.
%      A matrix containing the training signals as its columns.
%
%    'nfacts' - Number of factors.
%      Specifies the desired number of factors.
%
%    'cons' - Constraint sets.
%      Specifies the constraint sets in which each factor should lie. It
%      should be a cell-array of size 1*nfacts, where the jth column sets
%      the constraints for the jth factor (starting from the left). cons(j)
%      should be itself a cell-array of size 1*4 taking this form for a
%      factor of size m*n:
%      {'constraint name', 'constraint parameter', m, n}
%
%    'niter' - Number of iterations.
%      Specifies the number of iterations to run.
%
%    'init_facts' - Initialization of "facts".
%      Specifies a starting point for the algorithm.
%
%
%
%  Optional fields in PARAMS:
%  --------------------------
%
%    'init_lambda' - Initialization of "lambda".
%      Specifies a starting point for the algorithm. The default value is 1
%
%    'verbose' - Verbosity of the function. if verbose=1, the function
%      outputs the error at each iteration. if verbose=0, the function runs
%      in silent mode. The default value is 0.
%
%    'update_way' - Way in which the factors are updated. If update_way = 1
%      ,the factors are updated from right to left, and if update_way = 0,
%      the factors are updated from left to right. The default value is 0.
%
%
%  References:
%  [1] Le Magoarou L. and Gribonval R., "Learning computationally efficient
%  dictionaries and their implementation as fast transforms", submitted to
%  NIPS 2014

%  Luc Le Magoarou
%  PANAMA team
%  Inria, Rennes - Bretagne Atlantique
%  luc.le-magoarou@inria.fr
%
%  June 2014


%%%% Setting optional parameters values %%%%
if (isfield(params,'init_lambda'))
    init_lambda = params.init_lambda ;
else
    init_lambda = 1;
end

if (isfield(params,'verbose'))
    verbose = params.verbose ;
else
    verbose = 0;
end


if (isfield(params,'update_way'))
    update_way = params.update_way;
else
    update_way = 0;
end


if params.nfacts ~= size(params.init_facts,2)
    error('Wrong initialization: params.nfacts and params.init_facts are in conflict')
end

%%%%%% Setting of the constraints %%%%%%%
handles_cell = cell(1,params.nfacts);
for ii=1:params.nfacts
    cons = params.cons{ii};
    if strcmp(cons{1},'sp')
        handles_cell{ii} = @(x) prox_sp(x,cons{2});
    elseif strcmp(cons{1},'spcol')
        handles_cell{ii} = @(x) prox_spcol(x,cons{2});
    elseif strcmp(cons{1},'splin')
        handles_cell{ii} = @(x) prox_splin(x,cons{2});
    elseif strcmp(cons{1},'normcol')
        handles_cell{ii} = @(x) prox_normcol(x,cons{2});
    elseif strcmp(cons{1},'splincol')
        handles_cell{ii} = @(x) prox_splincol(x,cons{2});
    elseif strcmp(cons{1},'const')
        handles_cell{ii} = @(x) cons{2};
    else
        error('The expressed type of constraint is not known')
    end
end


facts = params.init_facts;
lambda = init_lambda;

%%%%%% Main Loop %%%%%%%
X = params.data;
if update_way
    maj = params.nfacts:-1:1;
else
    maj = 1:params.nfacts;
end

for i = 1:params.niter
    for j = maj
        if strcmp(params.cons{j}{1},'const')
            facts{j} = handles_cell{j}(facts{j});
        else
            L = facts(1:j-1);
            R = facts(j+1:end);
            [grad, LC] = grad_comp(L,facts{j},R,params.data,lambda);
            c = LC*1.001;
            facts{j} = handles_cell{j}(facts{j} - (1/c)*grad);
        end
    end
    %lambda = trace(mult_right(X',facts))/trace(mult_right(dvp(facts)',facts));
    lambda = trace(mult_left(facts,X'))/trace(mult_left(facts,dvp(facts)'));
    %lambda = trace(X'*dvp(facts))/trace(dvp(facts)'*dvp(facts));
    if verbose
        disp(['Iter ' num2str(i) ', RMSE=' num2str(norm(X-lambda*dvp(facts),'fro')/sqrt(numel(X))) ])
    end
end
end