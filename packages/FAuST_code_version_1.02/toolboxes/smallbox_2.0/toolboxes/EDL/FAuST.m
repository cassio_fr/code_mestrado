function D = FAuST(params)
[lambda, facts, Gamma] = hierarchical_dic_fact(params);
%[lambda, facts] = hierarchical_fact(params);
D = lambda*dvp(facts);
%D = D';
%norms = sqrt(sum(D.^2,1));
%ind = find(norms>1e-4);
%D(:,ind) = D(:,ind)./repmat(norms(ind),size(D,1),1) ; %matnorm(:,ind);
%Gamma = Gamma';
%Gamma(ind,:) = Gamma(ind,:).*repmat(norms(ind)',1,size(Gamma,2)); 
D = {D,Gamma};
end