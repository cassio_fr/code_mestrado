function [lambda, facts, Gamma] = hierarchical_dic_fact(params)
%hierarchical_dic_fact. Hierarchical dictionary learning with OMP.
%  [lambda, facts] = hierarchical_fact(params) runs the hierarchical
%  dictionary learning algorithm (Algorithm 3 of [1])on the specified set
%  of signals, returning the dictionary factors in "facts", the
%  multiplicative scalar in "lambda" and the coefficients in "Gamma".
%
%
%  Required fields in PARAMS:
%  --------------------------
%
%    'data' - Data matrix.
%      A matrix containing the training signals as its columns.
%
%    'D' - Dictionary to factorize.
%      A matrix to factorize.
%
%    'coeffs' - Coefficient matrix.
%      The matrix of coefficients to update.
%
%    'nfacts' - Number of factors.
%      Specifies the desired number of factors in which to factorize the
%      dictionary.
%
%    'sparsity_coeffs' - Number of atoms used in the representation, i.e.
%      number of non-zero coefficients per column in gamma.
%
%    'cons' - Constraint sets.
%      Specifies the constraint sets in which each factor should lie. It
%      should be a cell-array of size 2*(nfacts-1), where the jth columns
%      sets the constraints for the jth factorization in two factors:
%      cons(1,j) specifies the constraints for the left factor and
%      cons(2,j) for the right factor. cons(i,j) should be itself a
%      cell-array of size 1*4 taking this form for a factor of size m*n:
%      {'constraint name', 'constraint parameter', m, n}
%
%
%  Optional fields in PARAMS:
%  --------------------------
%
%    'niter1' - Number of iterations for the 2-factorisation.
%      Specifies the desired number of iteration for the factorisations in
%      2 factors. The default value is 500.
%
%    'niter2' - Number of iterations for the global optimisation.
%      Specifies the desired number of iteration for the global
%      optimisation. The default value is 500.
%
%    'verbose' - Verbosity of the function. if verbose=1, the function
%      outputs the error at each iteration. if verbose=0, the function runs
%      in silent mode. The default value is 0.
%
%    'fact_side' - Side to be factorized iteratively: 1-the left or
%      0-the right. The default value is 0;
%
%    'update_way' - Way in which the factors are updated. If update_way = 1
%      ,the factors are updated from right to left, and if update_way = 0,
%      the factors are updated from left to right. The default value is 0.
%
%  References:
%  [1] Le Magoarou L. and Gribonval R., "Learning computationally efficient
%  dictionaries and their implementation as fast transforms", submitted to
%  NIPS 2014

%  Luc Le Magoarou
%  PANAMA team
%  Inria, Rennes - Bretagne Atlantique
%  luc.le-magoarou@inria.fr
%
%  June 2014

%%%% Setting parameters values %%%%
if (isfield(params,'niter1'))
    niter1 = params.niter1;
else
    niter1 = 500;
end

if (isfield(params,'niter2'))
    niter2 = params.niter2;
else
    niter2 = 500;
end

if (isfield(params,'verbose'))
    verbose = params.verbose;
else
    verbose = 0;
end

if (isfield(params,'update_way'))
    update_way = params.update_way;
else
    update_way = 0;
end

if (isfield(params,'fact_side'))
    fact_side = params.fact_side;
else
    fact_side = 0;
end

%%%% Verifying the validity of the constraints %%%%
verifSize =  params.cons{1,1}{4}...
    == params.cons{2,1}{3} && size(params.D,2) == params.cons{2,1}{4};
for i = 2:params.nfacts-1
    if fact_side
        verifSize = verifSize && params.cons{2,i-1}{3} == params.cons{2,i}{4}...
            && params.cons{1,i}{4} == params.cons{2,i}{3} && size(params.D,1) == params.cons{1,i}{3};
    else
        verifSize = verifSize && params.cons{1,i-1}{4} == params.cons{1,i}{3}...
            && params.cons{1,i}{4} == params.cons{2,i}{3} && size(params.D,2) == params.cons{2,i}{4};
    end
end

if ~verifSize
    error('Size incompatibility in the constraints')
end

if params.nfacts-1 ~= size(params.cons,2)
    error('The number of constraints is in conflict with the number of factors')
end

facts = cell(1,params.nfacts);
Gamma = params.coeffs;
lambda =1;
facts{1} = params.D;
Res = params.D;

%%STEP 2 : Hierarchical factorization (+OMP)
for ii = 1:params.nfacts-1
    cons = params.cons(:,ii);
    
    %%%% Factorization in 2 %%%%
    params2.niter = niter1;
    params2.nfacts = 2;
    params2.data = Res;
    params2.verbose = verbose;
    params2.update_way = update_way;
    params2.cons = [cons(1), cons(2)];
    params2.init_facts = {zeros(cons{1}{3},cons{1}{4}), eye(cons{2}{3},cons{2}{4})};
    if update_way
        params2.init_facts = {eye(cons{1}{3},cons{1}{4}), zeros(cons{2}{3},cons{2}{4})};
    end
    params2.init_lambda = 1;
    [lambda2, facts2] = palm4LED(params2);
    if fact_side
        facts(3:end) = facts(2:end-1);
        facts(1:2) = facts2;
    else
        facts(ii:ii+1) = facts2;
    end
    lambda = lambda*lambda2;
    
    
    %%%% Global optimization %%%%
    params3.niter = niter2;
    params3.nfacts = ii+2;
    params3.data = params.data;
    params3.verbose = verbose;
    params3.update_way = update_way;
    if fact_side
        gamma_cons = {'const', Gamma, size(Gamma,1), size(Gamma,2)};
        params3.cons = [cons(1), params.cons(2,ii:-1:1),{gamma_cons}];
    else
        gamma_cons = {'const', Gamma, size(Gamma,1), size(Gamma,2)};
        params3.cons = [params.cons(1,1:ii), cons(2), {gamma_cons}];
    end
    params3.init_facts = [facts(1:ii+1), {Gamma}];
    params3.init_lambda = lambda;
    [lambda, facts3] = palm4LED(params3);
    facts(1:ii+1) = facts3(1:ii+1);
    if fact_side
        Res = facts3{1};
    else
        Res = facts3{ii+1};
    end
    
    %%%% OMP for the coefficients %%%%%
    Dico = lambda*dvp(facts);
    ind = find(sqrt(sum(Dico.^2,1))>1e-2);
    %matnorm = diag(sqrt(sum(Dico.^2,1)));
    norms = sqrt(sum(Dico.^2,1));
    Dico = Dico(:,ind)./repmat(norms(ind),size(Dico,1),1) ; %matnorm(:,ind);
    %Dico = Dico(:,ind);
    %disp(['Avant : ' num2str(norm(params.data-lambda*dvp(facts)*Gamma,'fro')/sqrt(numel(params.data)))])
    Gamma(ind,:) = full(omp(Dico,params.data,Dico'*Dico,params.sparsity_coeffs,'checkdict','off'))./repmat(norms(ind)',1,size(Gamma,2));
    %disp(['Apres : ' num2str(norm(params.data-lambda*dvp(facts)*Gamma,'fro')/sqrt(numel(params.data)))])
end
end