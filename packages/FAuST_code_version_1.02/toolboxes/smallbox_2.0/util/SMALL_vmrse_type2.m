function [RMSErn, RMSEcd, rn_im, cd_im] = SMALL_vmrse_type2(orig, corr, recon)
%%  Implementation of VectorRMSE type2
% 
%   Input:
%       - Original image
%       - Corrupted image
%       - Reconstructed Image
%
%   Output:
%       - RMSErn - RMSE from residual noise (noise not completely removed)
%       - RMSEcd - RMSE from collateral distortion - excessive filtering
%       - rn_im  - image of residual noise
%       - cd_im  - image of collateral distortion
%
%   F. Russo, "New Method for Performance Evaluation of Grayscale Image
%   Denoising filters", IEEE Signal Processing Letters, vol. 17, no. 5,
%   pp.417-420, May 2010

%   Centre for Digital Music, Queen Mary, University of London.
%   This file copyright 2011 Ivan Damnjanovic.
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License as
%   published by the Free Software Foundation; either version 2 of the
%   License, or (at your option) any later version.  See the file
%   COPYING included with this distribution for more information.
%%

    recon_int = round(recon);
    
    RN1 = ((orig<recon_int)&(recon_int<=corr));
    RN2 = ((orig>recon_int)&(recon_int>=corr));
    CD1 = ((orig<recon_int)&(recon_int>corr));
    CD2 = ((orig>recon_int)&(recon_int<corr));
    
    RMSErn = sqrt(sum(sum((RN1+RN2).*(orig-recon).^2)))/512;
    RMSEcd = sqrt(sum(sum((CD1+CD2).*(orig-recon).^2)))/512;
    rn_im=RN1+RN2;
    cd_im=CD1+CD2;
    
end

