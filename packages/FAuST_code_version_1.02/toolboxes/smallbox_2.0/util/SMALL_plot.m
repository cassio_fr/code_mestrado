function SMALL_plot(SMALL)
%% Plots coefficients and reconstructed signals 
%   Function gets as input SMALL structure and plots  the solution and
%   reconstructed signal

%
%   Centre for Digital Music, Queen Mary, University of London.
%   This file copyright 2009 Ivan Damnjanovic.
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License as
%   published by the Free Software Foundation; either version 2 of the
%   License, or (at your option) any later version.  See the file
%   COPYING included with this distribution for more information.
%%   

  figure;
 
  m=size(SMALL.solver,2);
  n=size(SMALL.solver(1).reconstructed,2)+1;
  for i =1:m
  
  subplot(m,n, (i-1)*n+1); plot(1:length(SMALL.solver(i).solution), SMALL.solver(i).solution, 'b')
        title(sprintf('%s in %.2f s', SMALL.solver(i).name, SMALL.solver(i).time),'Interpreter','none')
          xlabel('Coefficient') 
     
  % Plot reconstructed signal against original

  
  for j=2:n
      
      subplot(m,n,(i-1)*n+j); plot(1:length(SMALL.solver(i).reconstructed(:,j-1)), SMALL.solver(i).reconstructed(:,j-1)  ,'b.-',  1:length(SMALL.Problem.signal(:,j-1)), SMALL.Problem.signal(:,j-1),'r--')
          %legend(SMALL.solver(i).name,'Original signal',0,'Location','SouthOutside','Interpreter','none');
               title(sprintf('%s in %.2f s signal %d', SMALL.solver(i).name, SMALL.solver(i).time,n-1),'Interpreter','none')
  end
  end
end
