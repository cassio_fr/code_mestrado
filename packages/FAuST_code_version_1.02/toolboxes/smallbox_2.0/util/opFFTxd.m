function op = opFFTxd(n, dim)
% OPFFT  One-dimensional fast Fourier transform (FFT).
%
%    OPFFT(N) create a one-dimensional normalized Fourier transform
%    operator for vectors of length N.

%   Copyright 2008, Ewout van den Berg and Michael P. Friedlander
%   http://www.cs.ubc.ca/labs/scl/sparco
%   $Id: opFFT.m 1040 2008-06-26 20:29:02Z ewout78 $

op = @(x,mode) opFFTxd_intrnl(n,x, dim,mode);


function y = opFFTxd_intrnl(n,x,dim,mode)
x=reshape(x, [size(x,1)/n n]);
%checkDimensions(n,n,x,mode);
if mode == 0
   y = {size(x,1),size(x,1),[1,1,1,1],{'FFT'}};
elseif mode == 1
   y = fft(x, [], dim);% / sqrt(length(x));
else
   y = ifft(x, [], dim);% * sqrt(length(x));
end
y=reshape(y, [size(y,1)*size(y,2) 1]);   

