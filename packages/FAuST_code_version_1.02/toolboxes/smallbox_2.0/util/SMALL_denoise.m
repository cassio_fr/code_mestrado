function solver=SMALL_denoise(Problem, solver)
%% SMALL denoising - old replaced denoising function, would go out in v2.0
%
%   Function gets as input SMALL structure that contains SPARCO problem to
%   be solved, name of the toolbox and solver, and parameters file for
%   particular solver.
%   It is based on omp/omps denoising by Ron Rubenstein (KSVD toolbox)
%
%   Outputs are denoised image, psnr, number of non-zero coeficients and 
%   time spent

%
%   Centre for Digital Music, Queen Mary, University of London.
%   This file copyright 2009 Ivan Damnjanovic.
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License as
%   published by the Free Software Foundation; either version 2 of the
%   License, or (at your option) any later version.  See the file
%   COPYING included with this distribution for more information.
%   

%%
  
  %%%%%  denoise the signal  %%%%%
  fprintf('\nStarting %s... \n', solver.name);
  start=cputime;
  %%
  if strcmpi(solver.toolbox,'ompbox')
      if (~isfield(solver.param,'lambda'))
          solver.param.lambda = Problem.maxval/(10*Problem.sigma);
      end
      
      solver.param = Problem;
      %solver.param.memusage = 'high';
      solver.param = rmfield(solver.param, {'Noisy' 'Original' 'b' 'm' 'n' 'p' 'initdict'});
      solver.param.x = Problem.Noisy;
      solver.param.dict = Problem.A;
      p = Problem.signalDim;
      msgdelta=5;
      % call the appropriate ompdenoise function
      if (p==1)
          [y,nz] = ompdenoise1(solver.param,msgdelta);
      elseif (p==2)
          [y,nz] = ompdenoise2(solver.param,msgdelta);
      elseif (p==3)
          [y,nz] = ompdenoise3(solver.param,msgdelta);
      else
          [y,nz] = ompdenoise(solver.param,msgdelta);
      end
  elseif strcmpi(solver.toolbox,'ompsbox')
      if (~isfield(solver.param,'lambda'))
          solver.param.lambda = Problem.maxval/(10*Problem.sigma);
      end
      
      solver.param = Problem;
      %solver.param.memusage = 'high';
      solver.param = rmfield(solver.param, {'Noisy' 'Original' 'b' 'm' 'n' 'p' 'initdict'});
      solver.param.x = Problem.Noisy;
      if issparse(Problem.A)
        solver.param.A = Problem.A;
      else
        solver.param.A = sparse(Problem.A);
      end
      p = Problem.signalDim;
      msgdelta=5;
      % call the appropriate ompdenoise function
      if (p==1)
          [y,nz] = ompsdenoise1(solver.param,msgdelta);
      elseif (p==2)
          [y,nz] = ompsdenoise2(solver.param,msgdelta);
      elseif (p==3)
          [y,nz] = ompsdenoise3(solver.param,msgdelta);
      else
          [y,nz] = ompsdenoise(solver.param,msgdelta);
      end
  else
      printf('\nToolbox has not been registered. Please change SMALL_learn file.\n');
      return
  end
 %%
  solver.time = cputime - start;
  fprintf('\n%s finished task in %2f seconds. \n', solver.name, solver.time);
  solver.reconstructed.Image=y;
  solver.reconstructed.psnr=20*log10(Problem.maxval * sqrt(numel(Problem.Original)) / norm(Problem.Original(:)-solver.reconstructed.Image(:)));
  solver.reconstructed.nz=nz;
  
end