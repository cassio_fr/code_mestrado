function y = SL_A(A, mode, m, n, x, I, dim)
%% Sparco to SparseLab implicit matix conversion
%   This is auxilary function to allow implicit matrices from SPARCO
%   to be used with SparsLab solvers

%   Centre for Digital Music, Queen Mary, University of London.
%   This file copyright 2009 Ivan Damnjanovic.
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License as
%   published by the Free Software Foundation; either version 2 of the
%   License, or (at your option) any later version.  See the file
%   COPYING included with this distribution for more information.
%%

if (mode == 1)
    
    u = zeros(dim, 1);
    u(I) = x;
    y = A(u,1);
    
elseif (mode == 2)
    
    x2 = A(x,2);
    y = x2(I);
    
end

end