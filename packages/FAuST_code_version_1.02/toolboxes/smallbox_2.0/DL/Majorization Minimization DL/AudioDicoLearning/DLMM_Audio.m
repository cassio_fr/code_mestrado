function [] = DLMM_Audio(m,n,l,it,Lambda,admiss)
M = str2num(m);
N = str2num(n);
L = str2num(l);
IT = str2num(it);
lambda = str2num(Lambda);
load(['Param',num2str(M),'X',num2str(N),'X',num2str(L),'kADCT2X01.mat'])
method = ['bn';'un'];
res = 1;
while method(res)~=admiss
    res = res+1;
end
[PhiN,PhiM] = size(Phio);
Phi = randn(M,N);
Phi = Phi*diag(1./sqrt(sum(Phi.^2)));
[PhiN,L] = size(x);
phim = PhiM;
unhat = zeros(PhiM,L);
maxIST = 10;
maxIDU = 10;
it = 1;
ert(1) = norm(Phi*unhat-x,'fro')^2 + lambda*(sum(sum(abs(unhat))));
while it <= IT
    it
%     %%%%%% Iterative Soft-Thresholding for Sparse Approximation %%%%%
    tau2 = .1+(svds(Phi,1))^2;   
    eps = 3*10^-4;
    map = 0; % Projecting on the selected space (0=no,1=yes)
    [unhat,er] = mm1(Phi,x,unhat,tau2,lambda,maxIST,eps,map); 
    er
    %%%%%% Majorisation Minimisation for Dictionary Update %%%%%%    
    c = .1 + svds(unhat,1)^2; 
    eps = 10^-7;
    [Phi,unhat] = dict_update_REG_cn(Phi,x,unhat,maxIDU,eps,'un');
    %%%%%
    ert(it+1) = norm(Phi*unhat-x,'fro')^2 + lambda*(sum(sum(abs(unhat))));    
    it = it+1;        
end
save(['DLMMl1',num2str(PhiN),'X',num2str(PhiM),'X',num2str(L),'iK',method(res,:),'Lambda',num2str(lambda),'Audio.mat'],'Phi','x','unhat','ert','lambda')