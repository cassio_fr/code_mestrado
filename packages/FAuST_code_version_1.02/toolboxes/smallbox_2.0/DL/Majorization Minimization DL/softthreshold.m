% Soft threshold operator for matrices
% [Y] = softthreshold(X,theta)
% X : input matrix
% theta : thresholding parameter
function [Y] = softthreshold(X,theta)
Y = X - min(theta,abs(X)).*sign(X);