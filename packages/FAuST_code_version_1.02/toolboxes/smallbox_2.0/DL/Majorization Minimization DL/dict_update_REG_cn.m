function [Phiout,unhatnz] = dict_update_REG_cn(Phi,x,unhat,maxIT,eps,cvset)
%% Regularized Dictionary Learning with the constraint on the column norms %%%%%
% Phi = Normalized Initial Dictionary
% x = Signal(x). This can be a vector or a matrix
% unhat = Initial guess for the coefficients
% to = 1/(step size) . It is larger than spectral norm of coefficient matrix x
% eps = Stopping criterion for iterative softthresholding and MM dictionary update
% cvset = Dictionary constraint. 0 = Non convex ||d|| = 1, 1 = Convex ||d||<=1
% Phiout = Updated Dictionary
% unhatnz Updated Coefficients (the same as input in this version)

%%
B = Phi;
K = zeros(size(Phi,1),size(Phi,2));
c = .1 + svds(unhat,1)^2; %.1
c3 = (1/c)*eye(size(B,2));
c1 = x*unhat'*c3;
c2 = (c*eye(size(B,2))-unhat*unhat')*c3;

%%
      
for i=1:maxIT
%     if i>1
%         B = K;
%     end

    K = c1 + B*c2;
    
    if cvset == 1,
        K = K*diag(min(sum(K.^2).^(-.5),1)); % with convex constraint set
    else
        % Mehrdad original - 
        % K = K*diag(sum(K.^2).^(-.5)); % with fixed-norm constraint set
        K = normc(K);
    end
       
    if (sum(sum((B-K).^2)) < eps)
        break;
    end
    
    B = K;
end
%% depleted atoms cancellation %%%
[Y,I] = sort(sum(K.^2),'descend');
RR = sum(Y>=.01);
Phiout = K(:,I(1:RR));
unhatnz = unhat(I(1:RR),:);
end