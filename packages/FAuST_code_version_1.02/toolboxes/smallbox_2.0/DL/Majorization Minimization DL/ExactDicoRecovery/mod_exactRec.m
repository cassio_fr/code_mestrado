%%% MOD (||ki||<=1) %%%%
function mod_exactRec(it,k,sn)
tic
IT = str2num(it);
K = str2num(k);
SN = str2num(sn);
if SN<10,
    samnum = ['0',num2str(SN)];
else
    samnum = num2str(SN);
end
load(['Param',num2str(K),'kS',samnum,'.mat'])
method = ['bn';'un'];
res = 2; % 1 for bounded-norm, 2 for unit-norm
lambda = 2*.2; % 2 * Smallest coefficients (Soft Thresholding)
% lambda = 2*.2^2; % 2 * Smallest coefficients (Hard Thresholding)
%%%%%%%%%%%%%%
Phi = Phio;
[PhiN,PhiM] = size(Phi);
RR1 = PhiM;
%%%%%%%%%%%%%%
[PhiM,L] = size(ud);
unhat = ones(PhiM,L);
for it = 1:IT
    it
    to = .1+svds(Phi,1);
    [PhiN,PhiM] = size(Phi);
    %%%%
%     eps = 10^-7;
    eps = 3*10^-4;
    maxIT = 1000;
    map = 0;
    [unhat,l1err] = mm1(Phi,x,unhat,to,lambda,maxIT,eps,map); %% Sparse approximation with Iterative Soft-thresholding
    ert(it) = l1err;
    %%%   
    [Phi,unhat] = modcn(x,unhat,res);
end
save(['MODl120t',num2str(IT),'iki',method(res,:),num2str(K),'v2d',num2str(SN),'.mat'],'Phi','Phid','x','ud','unhat','ert')
toc