function [Phiout,unhatnz] = dict_update_MAP_cn(Phi,x,unhat,mu,maxIT,eps,cvset)
%% Maximum A Posteriori Dictionary Update with the constraint on the column norms %%%%%



%%

K = Phi;
B = zeros(size(Phi,1),size(Phi,2));
i = 1;
%%
while (sum(sum((B-K).^2))>eps)&&(i<=maxIT)
    B = K;
    for j = 1:size(K,2),
        K(:,j) = K(:,j) - mu*(eye(size(K,1))-K(:,j)*K(:,j)')*(K*unhat-x)*unhat(j,:)';           
    end
    i = i+1;
end

%% depleted atoms cancellation %%%

[Y,I] = sort(sum(K.^2),'descend');
RR = sum(Y>=.01);
Phiout = K(:,I(1:RR));
unhatnz = unhat(I(1:RR),:);

end
