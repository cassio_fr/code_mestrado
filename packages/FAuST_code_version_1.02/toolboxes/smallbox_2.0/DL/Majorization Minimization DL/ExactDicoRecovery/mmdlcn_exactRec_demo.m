function mmdlcn_exactRec_demo(it,k,sn,cs)
tic
IT = it;
K = k;
SN = sn;
if SN<10,
    samnum = ['0',num2str(SN)];
else
    samnum = num2str(SN);
end
load(['Param',num2str(K),'kS',samnum,'.mat'])
lambda = 2*.2; % 2 * Smallest coefficients (Soft Thresholding)
if cs == 'bn'
    res = 1;
elseif cs == 'un'
    res = 2;
else
    disp('Undefined dictionary admissible set.')
end
method = ['bn';'un'];
res = 2; % 1 for bounded-norm, 2 for unit-norm
%%%%%%%%%%%%%%
Phi = Phio;
M = size(Phi,1);
[Phi,unhat,ert] = mmdl_cn(x,Phi,lambda,IT,res);

save(['RDLl1',num2str(M),'t',num2str(IT),'iki',method(res,:),num2str(K),'v1d',num2str(SN),'.mat'],'Phi','Phid','x','ud','unhat','ert')

toc
