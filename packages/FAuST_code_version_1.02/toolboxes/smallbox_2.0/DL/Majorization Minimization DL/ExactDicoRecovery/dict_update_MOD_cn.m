function [Phiout,unhatnz] = dict_update_MOD_cn(x,unhat,cvset)
%% Method of Optimized Direction (MOD) Dictionary Update followed by the
%% constraint on the column norms %%

%%
eps = .01;
K = x*unhat'*inv(unhat*unhat'+eps*eye(size(unhat,1)));   
if cvset == 1,
    K = K*diag(min(sum(K.^2).^(-.5),1)); % Projecting on the convex constraint set
else
    K = K*diag(sum(K.^2).^(-.5)); % Projecting on the Unit-Norm constraint set
end

%% depleted atoms cancellation %%%
[Y,I] = sort(sum(K.^2),'descend');
RR = sum(Y>=.01);
Phiout = K(:,I(1:RR));
unhatnz = unhat(I(1:RR),:);

end