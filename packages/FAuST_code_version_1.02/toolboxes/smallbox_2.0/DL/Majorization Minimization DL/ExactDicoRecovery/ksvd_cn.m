% K-SVD algorithm for Dictionary Learning
% Y = input data (M X L matrix)
% Phi = initial dictionary (M X N), e.g. random dictionary or first N data samples
% lambda = regularization coefficient (||Phi*X-Y||_F)^2 + lambda*||X||_1
% IT = number of iterations
function [Phiout,X,ert] = ksvd_cn(Y,Phi,lambda,IT)
maxIT = 1000;
[PhiN,PhiM] = size(Phi);
RR1 = PhiM;
%%%%%%%%%%%%%%
% [PhiM,L] = size(ud);
[PhiN,L] = size(Y);
X = ones(PhiM,L);
for it = 1:IT
    to = .1+svds(Phi,1);
    [PhiN,PhiM] = size(Phi);
    %%%%
    eps = 3*10^-4;    
    map = 1; % Projecting on the selected space (0=no,1=yes)
    [X,l1err] = mm1(Phi,Y,X,to,lambda,maxIT,eps,map); %% Sparse approximation with Iterative Soft-thresholding
    ert(it) = l1err;
    %%%          
    [Phi,X] = dict_update_KSVD_cn(Phi,Y,X);  
end
Phiout = Phi;