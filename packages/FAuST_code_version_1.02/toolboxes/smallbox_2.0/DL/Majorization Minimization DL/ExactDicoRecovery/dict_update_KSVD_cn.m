function[Phiout,unhatnz] = dict_update_KSVD_cn(Phi,x,unhat)
%% K-SVD Dictionary Learning with the constraint on the column norms %%%%%

%%
unhatn = unhat;
rPerm = randperm(size(Phi,2));
%% for l = 1:size(Phi,2),
for l = rPerm
    unhatns = unhat;
    unhatns(l,:) = zeros(1,size(unhat,2));
    E = x-Phi*unhatns;
    ER = E(:,(abs(unhat(l,:))>0));
    [U,S,V] = svd(ER,0);
    Phi(:,l) = U(:,1)
    unhatn(l,:) = zeros(1,size(unhat,2));
    unhatn(l,(abs(unhat(l,:))>0)) = S(1,1)*V(:,1);
    unhat = unhatn;
end
%%
unhatnz = unhat;
Phiout = Phi;
end