function [X , cost] = wrapper_mm_solver(b, A, param)
%% SMALL wrapper for Majorization Minimization toolbox solver
%
%   Function gets as input
%       b - measurement vector 
%       A - dictionary 
%       param - structure containing additional parameters. These are:
%           - initcoeff   Initial guess for the coefficients
%                         (optional)
%           - to          1/(step size). It is larger than spectral norm
%                         of dictionary A (default is 0.1+(svds(A,1))^2)
%           - lambda      Lagrangian multiplier. Regulates shrinkage
%                         (default is 0.4)
%           - iternum     Inner-loop maximum iteration number 
%                         (default is 1000)
%           - epsilon     Stopping criterion for iterative softthresholding
%                         (default is 1e-7)
%           - map         Debiasing. 0 = No, 1 = Yes (default is 0)
%
%   Output:
%       x - sparse solution
%       cost - Objective cost

%
%   Centre for Digital Music, Queen Mary, University of London.
%   This file copyright 2011 Ivan Damnjanovic.
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License as
%   published by the Free Software Foundation; either version 2 of the
%   License, or (at your option) any later version.  See the file
%   COPYING included with this distribution for more information.
%   
%%

% Initial guess for the coefficients

if (isfield(param, 'initcoeff'))
   initX = param.initcoeff;
else
   initX = zeros(size(A,2),size(b,2));
end

% to - 1/(step size) . It is larger than spectral norm of dictionary A

if isfield(param, 'to')
   to = param.to;
else
   to = .1+(svds(A,1))^2;
end

% lambda - Lagrangian multiplier. (regulates shrinkage)

if isfield(param, 'lambda')
    lambda = param.lambda;
else
    lambda = 2*.2; 
end

% Inner-loop maximum iteration number.

if isfield(param, 'iternum')
    maxIT = param.iternum;
else
    maxIT = 1000; 
end

% Stopping criterion for iterative softthresholding

if isfield(param, 'epsilon')
    epsilon = param.epsilon;
else
    epsilon = 1e-7; 
end

% Debiasing. 0 = No, 1 = Yes

if isfield(param, 'map')
    map = param.map;
else
    map = 0; 
end


[X, cost] = mm1(A,b,initX,to,lambda,maxIT,epsilon,map); 
cost
end