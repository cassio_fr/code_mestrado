function [D]=my_dummy_DL(X, params) 
%%  Template function that can be used for DL algorithm implementation 
%   
%   
%   input arguments:    
%                       X - matrix columns are training signals
%                       params - struycture with parameters for DL
%                       algorithm
%
%   output arguments: 
%                       D - Learned dictionary.

%%  Change copyright notice as appropriate:
%   Centre for Digital Music, Queen Mary, University of London.
%   This file copyright 2009 Ivan Damnjanovic.
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License as
%   published by the Free Software Foundation; either version 2 of the
%   License, or (at your option) any later version.  See the file
%   COPYING included with this distribution for more information.
%%
