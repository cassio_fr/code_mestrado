function Denoising_dct(demoType)
%% Function Denoising_dct
%  This function denoises images using OMP with a DCT dictionary.
%	Denoising results are stored in 
%	"./output/results_denoising_DCT_user.mat". 
%
%  The input parameter can only be 'example' (default value) or 'full'.   
%   - The default value 'example' input parameter propose *one* example of 
%     image denoising, using a single set of parameters. 
%     Computations should take around 5 minutes.
%   - The 'full' input parameter propose to reproduce the complete results
%     described in [1].
%     WARNING: computation time for the full reproduction of results from [1]
%     can be long (several days) since this tests many factorization settings. 
% 
% For more information on the FAuST Project, please visit the website of 
% the project :  <http://faust.gforge.inria.fr>
%
%% License:
% Copyright (2016):	Luc Le Magoarou, Remi Gribonval
%			INRIA Rennes, FRANCE
%			http://www.inria.fr/
%
% The FAuST Toolbox is distributed under the terms of the GNU Affero 
% General Public License.
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as published 
% by the Free Software Foundation.
%
% This program is distributed in the hope that it will be useful, but 
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
% See the GNU Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%% Contacts:	
%	Luc Le Magoarou: luc.le-magoarou@inria.fr
%	Remi Gribonval : remi.gribonval@inria.fr
%
%% References:
% [1]	Le Magoarou L. and Gribonval R., "Flexible multi-layer sparse 
%	approximations of matrices and applications", Journal of Selected 
%	Topics in Signal Processing, 2016.
%	<https://hal.archives-ouvertes.fr/hal-01167948v1>
%%

    %close all;clear;clc;
    %%%% Setting optional parameters values %%%%
    if (strcmp(demoType,'full'))
        disp('Running full demonstration : WARNING this demonstration required several days.');
        disp('Are you sure you want to run this full demonstration?');
        
        images = cellstr([
            'cameraman     ';
            'house         ';
            'jetplane      ';
            'lake          ';
            'lena_512      ';
            'livingroom    ';
            'mandril       ';
            'peppers       ';
            'pirate        ';
            'walkbridge    ';
            'woman_blonde  ';
            'woman_darkhair';
        ]);
        N_training = 10000;
        sigmas = [10,15,20,30,50];
        N_atoms = [128,256,512];
        s=[2,3,6,12];
        rho=[0.4,0.5,0.7,0.9];
        pause(1);

    elseif (strcmp(demoType,'example'))
        disp('Running an example of the demonstration with following parameters.');
        %% Varying parameters
        images = cellstr([
            %'cameraman     ';
            'house         ';
            %'jetplane      ';
            %'lake          ';
            %'lena_512      ';
            %'livingroom    ';
            %'mandril       ';
            %'peppers       ';
            %'pirate        ';
            %'walkbridge    ';
            %'woman_blonde  ';
            %'woman_darkhair';
            ]);
        N_training = 10000;
        sigmas = [30];
        N_atoms = [256];
        s=[3];
        rho=[0.7];
      
        disp(strcat('image      : ', images));
        disp(strcat('N_training : ', num2str(N_training)));
        disp(strcat('sigmas     : ', num2str(sigmas)));
        disp(strcat('N_atoms    : ', num2str(N_atoms)));        
        disp(strcat('s          : ', num2str(s)));
        disp(strcat('rho        : ', num2str(rho)));
        pause(1);
    else
        disp ('WARNING: The input value is not recognized. The default parameter "example" is used.');
        disp('Running an example of the demonstration with following parameters.');
        %% Varying parameters
        images = cellstr([
            %'cameraman     ';
            'house         ';
            %'jetplane      ';
            %'lake          ';
            %'lena_512      ';
            %'livingroom    ';
            %'mandril       ';
            %'peppers       ';
            %'pirate        ';
            %'walkbridge    ';
            %'woman_blonde  ';
            %'woman_darkhair';
            ]);
        N_training = 10000;
        sigmas = [30];
        N_atoms = [256];
        s=[3];
        rho=[0.7];
   
        disp(strcat('image      : ', images));
        disp(strcat('N_training : ', num2str(N_training)));
        disp(strcat('sigmas     : ', num2str(sigmas)));
        disp(strcat('N_atoms    : ', num2str(N_atoms)));        
        disp(strcat('s          : ', num2str(s)));
        disp(strcat('rho        : ', num2str(rho)));
        pause(1);
    end
    
    runPath=which(mfilename);
    pathname = fileparts(runPath);
    if ( exist(strcat(pathname,'/output'),'dir') == 0 )
        mkdir ( strcat(pathname,'/output/'));
    end

    % (method,learning/denoising,image,noise level, number of atoms, s, rho, measure)
    resultTab = zeros(4,2,size(images,1),numel(sigmas),numel(N_atoms),numel(s),numel(rho),3); 

    %for im_ind = 2;
    for im_ind = 1:numel(images);  %1:size(images,1)
        %for sig_ind = 4;
        for sig_ind = 1:numel(sigmas);
            %for N_atoms_ind = 2;
            for N_atoms_ind = 1:numel(N_atoms);  
                FS=filesep;
                TMPpath=pwd;
                [pathstr1, name, ext] = fileparts(which('SMALLboxSetup.m'));
                cd([pathstr1,FS,'data',FS,'standard_test_images_png']);
                im = imread([images{im_ind},'.png']);
                im = double(im);
                cd(TMPpath);
                SMALL.Problem = generateImageDenoiseProblem(im, N_training,8,N_atoms(N_atoms_ind),sigmas(sig_ind),1.15);
                SMALL.Problem.name=images{im_ind};
                RMSE_init = norm(SMALL.Problem.Original - SMALL.Problem.Noisy,'fro')/sqrt(numel(SMALL.Problem.Original))
                sparsity_coeffs = 5;
                Edata=sqrt(prod(SMALL.Problem.blocksize)) * SMALL.Problem.sigma * SMALL.Problem.gain;
                maxatoms = floor(prod(SMALL.Problem.blocksize)/2);

                %%   Use DCT to denoise
                SMALL.DL(2)=SMALL_init_DL();
                %SMALL.DL(2).toolbox = 'KSVDS';
                SMALL.DL(2).name = 'DCT';

                N_atoms_per_dim = ceil(sqrt(N_atoms(N_atoms_ind)));
                DCT_dict = kron(odctdict(8,N_atoms_per_dim),odctdict(8,N_atoms_per_dim));

                SMALL.DL(1).D = DCT_dict;
                SMALL.Problem.A = DCT_dict;
                SMALL.solver(1)=SMALL_init_solver;%denoising
                SMALL.solver(1).toolbox='ompbox';
                SMALL.solver(1).name='omp';
                SMALL.solver(1).param=struct(...
                    'epsilon',sparsity_coeffs,...
                    'maxatoms', maxatoms);
                b1temp = SMALL.Problem.b1;
                SMALL.Problem = rmfield(SMALL.Problem,'b1');
                %SMALL.Problem = rmfield(SMALL.Problem,'reconstruct');
                SMALL.solver(1)=SMALL_solve(SMALL.Problem, SMALL.solver(1));
                Gamma_ksvd_learn = SMALL.solver(1).solution;
                SMALL.Problem.b1 = b1temp;

                RMSE_learn = norm(SMALL.Problem.b - SMALL.Problem.A*Gamma_ksvd_learn,'fro')/sqrt(numel(SMALL.Problem.b));

                SMALL.Problem.reconstruct = @(x) ImageDenoise_reconstruct(x, SMALL.Problem);
                SMALL.solver(1)=SMALL_solve(SMALL.Problem, SMALL.solver(1));
                PSNR_test = SMALL.solver(1).reconstructed.psnr;
                SSIM_test = SMALL.solver(1).reconstructed.ssim;

                resultTab(3,1,im_ind,sig_ind,N_atoms_ind,1,1,1) = RMSE_learn;
                resultTab(3,2,im_ind,sig_ind,N_atoms_ind,1,1,1) = PSNR_test;
                resultTab(3,2,im_ind,sig_ind,N_atoms_ind,1,1,2) = SSIM_test;


                %% Plot results and save image files
                SMALL_ImgDeNoiseResult(SMALL); 
                %save('results_img_denoising_DCT_current','resultTab');
            end
        end
        if (strcmp(demoType,'full'))
            matfile = fullfile(pathname, 'output/results_img_denoising_DCT_current');
            save(matfile,'resultTab');
        end
    end
    heure = clock ;

    %save(['results_denoising_DCT_user'],'resultTab');

    matfile = fullfile(pathname, 'output/results_denoising_DCT_user');
    save(matfile,'resultTab');
end

