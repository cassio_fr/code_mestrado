%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%	Image denoising experiment	%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
General purpose:

The algorithms implemented here are described in details in [1].
In this directory, you will find code necessary to reproduce the results 
of Sec.VI. of [1]. The folder "precomputed_results_denoising" contains 
also the original figures found in [1].

For more information on the FAuST Project, please visit the website of the 
project: <http://faust.gforge.inria.fr>
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Available tools:

-	Denoising_faust_ksvd.m [Optional]:
	Run this function that will learn a dictionary on some noisy image 
	patches using first KSVD and then the FAuST dictionary learning 
	introduced in [1]. Then image denoising using OMP is done with the 
	two learned dictionaries. 
	Denoising results are stored in 
	"./output/results_denoising_user.mat".

    The input parameter can only be 'example' (default value) or 'full'.   
    -   The default value 'example' input parameter propose *one* example of 
        image denoising, using a single set of parameters. 
        Computations should take around 5 minutes.
    -   The 'full' input parameter propose to reproduce the complete results
        described in [1].
        WARNING: computation time for the full reproduction of results from [1]
        can be long (several days) since this tests many factorization settings. 
		
-	Denoising_dct.m [Optional]:
	Run this script that will perform image denoising using OMP and a
	DCT dictionary. 
	Denoising results are stored in 
	"./output/results_denoising_DCT_user.mat". 

    The input parameter can only be 'example' (default value) or 'full'.   
    -   The default value 'example' input parameter propose *one* example of 
        image denoising, using a single set of parameters. 
        Computations should take around 5 minutes.
    -   The 'full' input parameter propose to reproduce the complete results
        described in [1].
        WARNING: computation time for the full reproduction of results from [1]
        can be long (several days) since this tests many factorization settings. 
	
-	display_results_denoising.m:
	Run the script "display_results_denoising.m" that displays the 
	figures (and some tables not present in the paper) corresponding to 
	the above experiments (cf. Fig. 12 from [1]).
	If you decided *not* to re-run the complete experiment with 
	Denoising_faust_ksvd.m or/and Denoising_dct.m, then the figure is 
	obtained using the denoising results from the file
	"./precomputed_results_denoising/results_denoising.mat"	or/and
	"./precomputed_results_denoising/results_denoising_DCT.mat" 

	Otherwise it is obtained from the file 
	"./output/results_denoising_user.mat" or/and
	"./output/results_denoising_DCT_user.mat".

-   run_demo_precomputed.m is a script used to display all figures of the 
    article [1] concerning image denoising, from precomputed data. 

-   run_demo_recompute_Denoising_faust_ksvd.m is a script used to run full 
    denoising demo, and display corresponding figures of the article [1].

-   run_demo_recompute_Denoising_dct.m is a script used to run full 
    denoising demo, and display corresponding figures of the article [1]. 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
License:

Copyright (2016):	Luc Le Magoarou, Remi Gribonval
			INRIA Rennes, FRANCE
			http://www.inria.fr/

The FAuST Toolbox is distributed under the terms of the GNU Affero General 
Public License.
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Contacts:	
	Luc Le Magoarou: luc.le-magoarou@inria.fr
	Remi Gribonval : remi.gribonval@inria.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
References:

[1]	Le Magoarou L. and Gribonval R., "Flexible multi-layer sparse 
	approximations of matrices and applications", Journal of Selected 
	Topics in Signal Processing, 2016.
	<https://hal.archives-ouvertes.fr/hal-01167948v1>
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	

