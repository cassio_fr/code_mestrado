%% Function Denoising_faust_ksvd('example')
%  Run this function that will learn a dictionary on some noisy image 
%  patches using first KSVD and then the FAuST dictionary learning 
%  introduced in [1]. Then image denoising using OMP is done with the 
%  two learned dictionaries. 
%  Denoising results are stored in 
%  "./output/results_denoising_user.mat".
%
%  The input parameter can only be 'example' (default value) or 'full'.   
%   - The default value 'example' input parameter propose *one* example of 
%     image denoising, using a single set of parameters. 
%     Computations should take around 5 minutes.
%   - The 'full' input parameter propose to reproduce the complete results
%     described in [1].
%     WARNING: computation time for the full reproduction of results from [1]
%     can be long (several days) since this tests many factorization settings. 
%
% For more information on the FAuST Project, please visit the website of 
% the project :  <http://faust.gforge.inria.fr>
%
%% License:
% Copyright (2016):	Luc Le Magoarou, Remi Gribonval
%			INRIA Rennes, FRANCE
%			http://www.inria.fr/
%
% The FAuST Toolbox is distributed under the terms of the GNU Affero 
% General Public License.
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as published 
% by the Free Software Foundation.
%
% This program is distributed in the hope that it will be useful, but 
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
% See the GNU Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%% Contacts:	
%	Luc Le Magoarou: luc.le-magoarou@inria.fr
%	Remi Gribonval : remi.gribonval@inria.fr
%
%% References:
% [1]	Le Magoarou L. and Gribonval R., "Flexible multi-layer sparse 
%	approximations of matrices and applications", Journal of Selected 
%	Topics in Signal Processing, 2016.
%	<https://hal.archives-ouvertes.fr/hal-01167948v1>
%%

%close all;clear;clc;

%%%% Setting optional parameters values %%%%
%% Varying parameters
images = params.imagename;

N_training = params.trainnum;
sigmas = params.sigma;
N_atoms = params.dictsize;
s=params.s(params.alpha);
rho=params.rho(params.alpha);

disp(strcat('image      : ', images));
disp(strcat('N_training : ', num2str(N_training)));
disp(strcat('sigmas     : ', num2str(sigmas)));
disp(strcat('N_atoms    : ', num2str(N_atoms)));        
disp(strcat('s          : ', num2str(s)));
disp(strcat('rho        : ', num2str(rho)));
pause(1);


% resultTab = zeros(4,2,size(images,1),numel(sigmas),numel(N_atoms),numel(s),numel(rho),3); % (method,learning/denoising,image,noise level, number of atoms, s, rho, measure)

%for im_ind = 2;
for im_ind = 1:numel(images);  %1:size(images,1)
    %for sig_ind =4;
    for sig_ind =1:numel(sigmas);
        %for N_atoms_ind = 2;
        for N_atoms_ind = 1:numel(N_atoms);
            FS=filesep;
            TMPpath=pwd;
            [pathstr1, name, ext] = fileparts(which('SMALLboxSetup.m'));
            cd([pathstr1,FS,'data',FS,'standard_test_images_png']);
            im = imread(images{im_ind});
            im = double(im);
            cd(TMPpath);
            SMALL.Problem = generateImageDenoiseProblem(im, N_training,8,N_atoms(N_atoms_ind),sigmas(sig_ind),1.15);
            SMALL.Problem.name=images{im_ind};
            RMSE_init = norm(SMALL.Problem.Original - SMALL.Problem.Noisy,'fro')/sqrt(numel(SMALL.Problem.Original));
            sparsity_coeffs = 5;
            Edata=sqrt(prod(SMALL.Problem.blocksize)) * SMALL.Problem.sigma * SMALL.Problem.gain;
            maxatoms = floor(prod(SMALL.Problem.blocksize)/2);
            
            %%   Use KSVD Dictionary Learning Algorithm to Learn overcomplete dictionary
            SMALL.DL(1)=SMALL_init_DL();
            SMALL.DL(1).toolbox = 'KSVD';
            SMALL.DL(1).name = 'ksvd';

            SMALL.DL(1).param=struct(...
                'Tdata', sparsity_coeffs,...
                'dictsize', SMALL.Problem.p,...
                'iternum', 50,...
                'memusage', 'high');

            SMALL.DL(1) = SMALL_learn(SMALL.Problem, SMALL.DL(1));%dictionary learning
            SMALL.Problem.A = SMALL.DL(1).D;
            D_ksvd = SMALL.DL(1).D;

            SMALL.solver(1)=SMALL_init_solver;%denoising
            SMALL.solver(1).toolbox='ompbox';
            SMALL.solver(1).name='omp';
            SMALL.solver(1).param=struct(...
                'epsilon',sparsity_coeffs,...
                'maxatoms', maxatoms);
            b1temp = SMALL.Problem.b1;
            SMALL.Problem = rmfield(SMALL.Problem,'b1');
            %SMALL.Problem = rmfield(SMALL.Problem,'reconstruct');
            SMALL.solver(1)=SMALL_solve(SMALL.Problem, SMALL.solver(1));
            Gamma_ksvd_learn = SMALL.solver(1).solution;
            SMALL.Problem.b1 = b1temp;

%             RMSE_learn = norm(SMALL.Problem.b - SMALL.Problem.A*Gamma_ksvd_learn,'fro')/sqrt(numel(SMALL.Problem.b));
% 
%             SMALL.Problem.reconstruct = @(x) ImageDenoise_reconstruct(x, SMALL.Problem);
%             SMALL.solver(1)=SMALL_solve(SMALL.Problem, SMALL.solver(1));
%             PSNR_test = SMALL.solver(1).reconstructed.psnr;
%             SSIM_test = SMALL.solver(1).reconstructed.ssim;
% 
%             resultTab(1,1,im_ind,sig_ind,N_atoms_ind,1,1,1) = RMSE_learn;
%             resultTab(1,2,im_ind,sig_ind,N_atoms_ind,1,1,1) = PSNR_test;
%             resultTab(1,2,im_ind,sig_ind,N_atoms_ind,1,1,2) = SSIM_test;

            
            %% Use FAuST Dictionary Learning Algorithm to denoise image 
            %for s_ind=2;
            for s_ind=1:numel(s);
                %for rho_ind=3;
                for rho_ind=1:numel(rho);
                    SMALL.DL(2)=SMALL_init_DL();
                    SMALL.DL(2).toolbox = 'EDL';
                    SMALL.DL(2).name = 'FAuST';

                    %   Defining the parameters for EDL
                    SMALL.DL(2).param=struct(...
                        'nfacts', 4, ...
                        'niter1', 200,...
                        'niter2', 50,...
                        'niter_ksvd',50,...
                        'sparsity_coeffs',sparsity_coeffs,...
                        'verbose', 1,...
                        'update_way', 0,...
                        'fact_side',1,...
                        'coeffs', full(Gamma_ksvd_learn),...
                        'D',D_ksvd);

                    SMALL.DL(2).param.cons = cell(2,3);

                    SMALL.DL(2).param.cons{1,1} = {'sp',SMALL.Problem.m^2*rho(rho_ind),SMALL.Problem.m,SMALL.Problem.m};
                    SMALL.DL(2).param.cons{2,1} = {'sp',s(s_ind)*SMALL.Problem.p,SMALL.Problem.m,SMALL.Problem.p};
                    SMALL.DL(2).param.cons{1,2} = {'sp',SMALL.Problem.m^2*rho(rho_ind)^2,SMALL.Problem.m,SMALL.Problem.m};
                    SMALL.DL(2).param.cons{2,2} = {'sp',s(s_ind)*SMALL.Problem.m,SMALL.Problem.m,SMALL.Problem.m};
                    SMALL.DL(2).param.cons{1,3} = {'sp',SMALL.Problem.m^2*rho(rho_ind)^3,SMALL.Problem.m,SMALL.Problem.m};
                    SMALL.DL(2).param.cons{2,3} = {'sp',s(s_ind)*SMALL.Problem.m,SMALL.Problem.m,SMALL.Problem.m};

                    cons =  SMALL.DL(2).param.cons;
                    RC = 0;
                    for kk = 1:size(cons,2)
                        RC = RC + cons{2,kk}{2};
                    end
                    RC = (RC + cons{1,kk}{2})/(SMALL.Problem.p*SMALL.Problem.m);

                    SMALL.DL(2) = SMALL_learn(SMALL.Problem, SMALL.DL(2));%dictionary learning

                    SMALL.Problem.A = SMALL.DL(2).D{1};         %Setting up reconstruction function
                    Gamma_learn = SMALL.DL(2).D{2};
                    SMALL.DL(2).D = SMALL.DL(2).D{1};
                    RMSE_learn = norm(SMALL.Problem.b - SMALL.Problem.A*Gamma_learn,'fro')/sqrt(numel(SMALL.Problem.b));

%                     SMALL.solver(2)=SMALL_init_solver;%denoising
%                     SMALL.solver(2).toolbox='ompbox';
%                     SMALL.solver(2).name='omp';
%                     SMALL.solver(2).param=struct(...
%                         'epsilon',sparsity_coeffs,...
%                         'maxatoms', maxatoms);
%                     SMALL.Problem.reconstruct = @(x) ImageDenoise_reconstruct(x, SMALL.Problem);
%                     SMALL.solver(2)=SMALL_solve(SMALL.Problem, SMALL.solver(2));
%                     PSNR_test = SMALL.solver(2).reconstructed.psnr;
%                     SSIM_test = SMALL.solver(2).reconstructed.ssim;
% 
%                     resultTab(2,1,im_ind,sig_ind,N_atoms_ind,s_ind,rho_ind,1) = RMSE_learn;
%                     resultTab(2,1,im_ind,sig_ind,N_atoms_ind,s_ind,rho_ind,3) = RC;
%                     resultTab(2,2,im_ind,sig_ind,N_atoms_ind,s_ind,rho_ind,1) = PSNR_test;
%                     resultTab(2,2,im_ind,sig_ind,N_atoms_ind,s_ind,rho_ind,2) = SSIM_test;

                end
            end

            %% Plot results and save image files
%             SMALL_ImgDeNoiseResult(SMALL); 
            %pause;

            %save('results_denoising_current','resultTab');
        end
    end
%     matfile = fullfile(pathname, 'output/results_denoising_faust_ksd_current');
%     save(matfile,'resultTab');
end
% heure = clock ;

%save(['results_denoising_user'],'resultTab');
% matfile = fullfile(pathname, 'output/results_denoising_user');
% save(matfile,'resultTab');

D = SMALL.DL(2).D;
D = normc(D);
D = D(:,(sum(D.^2,1))<= 2);


