%% Description demo_fact_hadamard
%
%  This demo hierarchically factorizes the Hadamard dictionary and then
%  plots the results. This essentially reproduces figure 2 from [1].
%
% For more information on the FAuST Project, please visit the website of 
% the project :  <http://faust.gforge.inria.fr>
%
%% License:
% Copyright (2016):	Luc Le Magoarou, Remi Gribonval
%			INRIA Rennes, FRANCE
%			http://www.inria.fr/
%
% The FAuST Toolbox is distributed under the terms of the GNU Affero 
% General Public License.
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as published 
% by the Free Software Foundation.
%
% This program is distributed in the hope that it will be useful, but 
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
% See the GNU Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%% Contacts:	
%	Luc Le Magoarou: luc.le-magoarou@inria.fr
%	Remi Gribonval : remi.gribonval@inria.fr
%
%% References:
% [1]	Le Magoarou L. and Gribonval R., "Flexible multi-layer sparse 
%	approximations of matrices and applications", Journal of Selected 
%	Topics in Signal Processing, 2016.
%	<https://hal.archives-ouvertes.fr/hal-01167948v1>
%%



%clear
%close all
%clc

n = 32;
M = log2(n);

%% Generating the data
[params.data,~] = hadamard_mat(M);

%% Setting of the parameters
params.nfacts = M;
params.cons = cell(2,M-1);

for j=1:M-1
    params.cons{1,j} = {'splincol',2,n,n};
    params.cons{2,j} = {'splincol',n/2^j,n,n};
end

params.niter1 = 30;
params.niter2 = 30;
params.update_way = 1;
params.verbose = 0;

[lambda, facts] = hierarchical_fact(params);


%% Plotting the results
X = params.data;
Xhat = lambda*dvp(facts);

figure;
hold on;
subplot(1,params.nfacts+1,1);
imagesc(Xhat); axis square


for kk = 1:params.nfacts
    subplot(1,params.nfacts+1,1+kk)
    imagesc(facts{kk}); axis square
end

% figure
% subplot(2,params.nfacts+1,1)
% imagesc(X)
% hold on
% subplot(2,params.nfacts+1,params.nfacts+2)
% imagesc(Xhat)
% 
% for kk = 1:params.nfacts
%     subplot(2,params.nfacts+1,params.nfacts+2+kk)
%     imagesc(facts{kk})
% end

