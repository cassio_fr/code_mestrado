function [Ahat,Bhat,Dhat] = KALS(nmax_it,D,A_old,B_old)
%% Kronecker Alternating Least Square algorithm
[N,M] = size(D);
Ni = sqrt(N);
Mi = sqrt(M);

error_vec = zeros(1,nmax_it);

Prow = permut(Ni,Ni);
Pcol = permut(Mi,Mi);
Dmod = Prow*D*Pcol;
%% Start iterations:
stop_test = 1;
num_it = 0;
% B_old = randn(Ni,Mi);
while stop_test        
    num_it = num_it + 1;    
    
    A_new = zeros(Ni,Mi);
	vec_B_old = reshape(B_old,Mi*Ni,1);
    norm_B_old = norm(vec_B_old,'fro')^2;
    for n=1:Ni
        for m=1:Mi    
           n1 = (n-1)*Ni + 1;
           m1 = (m-1)*Mi + 1;    
           vec_D = reshape(D(n1:n1+Ni-1,m1:m1+Mi-1),Mi*Ni,1);
           A_new(n,m) = (vec_B_old'*vec_D)/norm_B_old;                       
        end
    end
    B_new = zeros(Ni,Mi);
	vec_A_new = reshape(A_new,Mi*Ni,1);
    norm_A_new = norm(vec_A_new,'fro')^2;
    for n=1:Ni
        for m=1:Mi    
            n1 = (n-1)*Ni + 1;
            m1 = (m-1)*Mi + 1;    
            vec_Dmod = reshape(Dmod(n1:n1+Ni-1,m1:m1+Mi-1),Mi*Ni,1);
            B_new(n,m) = (vec_A_new'*vec_Dmod)/norm_A_new;            
        end
    end       
    stop_test = (num_it < nmax_it);    
    A_old = A_new;
    B_old = B_new;
        
    % Convergence analysis
    Dhat = kron(A_old,B_old);
    error_vec(num_it) = norm(D(:)-Dhat(:))^2/norm(D(:))^2;
end
plot(error_vec);

Ahat = A_new;
Bhat = B_new;
Dhat = kron(Ahat,Bhat);