function mine_ksvddenoisedemo
%KSVDDENOISEDEMO K-SVD denoising demonstration.
%  KSVDDENISEDEMO reads an image, adds random white noise and denoises it
%  using K-SVD denoising. The input and output PSNR are compared, and the
%  trained dictionary is displayed.
%
%  To run the demo, type KSVDDENISEDEMO from the Matlab prompt.
%
%  See also KSVDDEMO.


%  Ron Rubinstein
%  Computer Science Department
%  Technion, Haifa 32000 Israel
%  ronrubin@cs
%
%  August 2009


disp(' ');
disp('  **********  K-SVD Denoising Demo  **********');
disp(' ');
disp('  This demo reads an image, adds random Gaussian noise, and denoises the image');
disp('  using K-SVD denoising. The function displays the original, noisy, and denoised');
disp('  images, and shows the resulting trained dictionary.');
disp(' ');


%% prompt user for image %%

pathstr = fileparts(which('ksvddenoisedemo'));
dirname = fullfile(pathstr, 'images', '*.png');
imglist = dir(dirname);

disp('  Available test images:');
disp(' ');
for k = 1:length(imglist)
  printf('  %d. %s', k, imglist(k).name);
end
printf('  %d. All images', length(imglist)+1);
disp(' ');

imnum = 0;
while (~isnumeric(imnum) || ~iswhole(imnum) || imnum<1 || imnum>length(imglist)+1)
  imnum = input(sprintf('  Image to denoise (%d-%d): ', 1, length(imglist)), 's');
  imnum = sscanf(imnum, '%d');
end

total_images = 1;
if imnum == length(imglist)+1, total_images = length(imglist), end
for image = 1:total_images

if image > 1, imnum = image; end
if imnum == 6, imnum = 1; end
imgname = fullfile(pathstr, 'images', imglist(imnum).name);



%% generate noisy image %%

sigma = 20; fprintf('\nSIGMA = %d\n',sigma)
samples_training = 40000; % Standard is 40000
monte_carlo_it = 5;

for k_mc = 1:monte_carlo_it
disp(' ');
disp('Generating noisy image...');

im = imread(imgname);
im = double(im);

% parameters
% alpha = [0:5:20 logspace(log10(25),log10(3000),195)]; %[0:25:200 250:50:950 1050:150:3000]; %nuclear norm regularization coefficient
alpha = [100:9:650 logspace(log10(110),log10(300),40)];%logspace(log10(50),log10(98),10);
% alpha = 2:2:64; % Low-rank approximation

% results to store
SNR = zeros(size(alpha));
nuclear_norm_Dmod = zeros(size(alpha));
rank_Dmod = zeros(size(alpha));
% svd_vecs = zeros(length(alpha),128);

for ktrain = 1:length(samples_training)
for k = 1:length(alpha)

n = randn(size(im)) * sigma;
imnoise = im + n;

%% set parameters %%

params.x = imnoise;
params.blocksize = 8;
params.dictsize = 256;
params.sigma = sigma;
params.maxval = 255;
params.trainnum = samples_training(ktrain);
params.iternum = 100;
params.memusage = 'high';

% denoise!
fprintf('\n>>>>>>>>>> Separable terms optimization via ALM - Iteration %d of %d <<<<<<<<<<\n',k,length(alpha));
params.alpha = alpha(k);

[imout, dict,reord_dict] = mine_ksvddenoise(params);

% show results %

% dictimg = showdict(dict,[1 1]*params.blocksize,round(sqrt(params.dictsize)),round(sqrt(params.dictsize)),'lines','highcontrast');
% figure; imshow(imresize(dictimg,2,'nearest'));
% title('Trained dictionary');
% 
% figure; imshow(im/params.maxval);
% title('Original image');
% 
% figure; imshow(imnoise/params.maxval); 
% title(sprintf('Noisy image, PSNR = %.2fdB', 20*log10(params.maxval * sqrt(numel(im)) / norm(im(:)-imnoise(:))) ));
% 
% figure; imshow(imout/params.maxval);
% title(sprintf('Denoised image, PSNR: %.2fdB', 20*log10(params.maxval * sqrt(numel(im)) / norm(im(:)-imout(:))) ));

SNR(k) = 20*log10(params.maxval * sqrt(numel(im)) / norm(im(:)-imout(:)));
nuclear_norm_Dmod(k) = sum(svd(reord_dict));
rank_Dmod(k) = rank(reord_dict,norm(dict)*2e-7);
% svd_vecs(k,:) = svd(reord_dict);

% save(strcat('new_alternate',num2str(params.iternum),'_SNR_sigma',num2str(sigma),'_Separable_',imglist(imnum).name(1:end-4),'_trainnum',num2str(params.trainnum)),'alpha','SNR','nuclear_norm_Dmod','rank_Dmod');
save(strcat('new_alternate',num2str(params.iternum),'_SNR_sigma',num2str(sigma),'_Separable_',imglist(imnum).name(1:end-4),'_trainnum',num2str(params.trainnum),'_mc',num2str(k_mc)),'alpha','SNR','nuclear_norm_Dmod','rank_Dmod');
% save(strcat('new_SNR_lowrank_sigma',sigma,'_',imglist(imnum).name(1:end-4)),'alpha','SNR');
end
end
end
end
