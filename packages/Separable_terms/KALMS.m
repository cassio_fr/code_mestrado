function [Ahat,Bhat,Dhat] = KALMS(nmax_it,D,A_old,B_old)
%% Kronecker Alternating Least Mean Square algorithm 
alphaA = 0.5;
alphaB = 0.5;
[N,M] = size(D);
Ni = sqrt(N);
Mi = sqrt(M);

error_vec = zeros(1,nmax_it);

%% Start iterations:
stop_test = 1;
num_it = 0;
% A_old = randn(Ni,Mi);
% B_old = randn(Ni,Mi);
while stop_test    
    % Compute the iterations number
    num_it = num_it + 1;

    x = randn(Mi*Mi,1);
    y = D*x;   
    
    vec_zB = kron(A_old,eye(Mi))*x;
    muB = alphaB/(norm(vec_zB,'fro')^2);
    vec_erB = y - kron(eye(Ni),B_old)*vec_zB;
    mat_zB = reshape(vec_zB,Mi,Ni);
    mat_erB = reshape(vec_erB,Ni,Ni);
    B_new = B_old + (muB/Ni)*mat_erB*mat_zB'; 
    
    
    vec_zA = kron(eye(Mi),B_new)*x;
    muA = alphaA/(norm(vec_zA,'fro')^2);
    vec_erA = y - kron(A_old,eye(Ni))*vec_zA;
    
    mat_zA = reshape(vec_zA,Ni,Mi).';
    mat_erA = reshape(vec_erA,Ni,Ni).';
    A_new = A_old + (muA/Ni)*mat_erA*mat_zA';  
                                 
    stop_test = (num_it < nmax_it);
    A_old = A_new;
    B_old = B_new;
    
    % Convergence analysis
    Dhat = kron(A_old,B_old);
    error_vec(num_it) = norm(D(:)-Dhat(:))^2/norm(D(:))^2;
end
plot(error_vec);

Ahat = A_new;
Bhat = B_new;
Dhat = kron(Ahat,Bhat);