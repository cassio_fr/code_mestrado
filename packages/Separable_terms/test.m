% test:
clear all
clc
Ni = 2;
Mi = 3;
N = Ni^2;
M = Mi^2;
Ao = randi(10,Ni,Mi);% (Na x Ma)
Bo = randi(10,Ni,Mi);% (Nb x Mb)
Do = kron(Ao,Bo);
 
D = Do + 0.005*randn(N,M);
% D = Do
kronalgo = 'KALS';
nmax_it = 1000;
switch kronalgo    
    case 'KSVD'        
        [A,B,D] = KSVD(nmax_it,D);        
    case 'KALMS'
        [A,B,D] = KALMS(nmax_it,D);        
    case 'KALS'     
        [A,B,D] = KALS(nmax_it,D);
    otherwise
        disp('here!')
        stop 
end

err = norm(D(:)-Do(:))^2/norm(Do(:))^2

%%
Yx = params.data % = DX = kron(A,B)X  (NaNb x Nx=params.trainnum)
Y = mat2tens(Yx.',[Ni Ni params.trainnum],1,3:2);% (Nx x Nb x Na)
%X = Gamma (NaNb x Nx)

Yb = tens2mat(Y,2);% =B*kron(A,X)  (Nb x Na Nx)
Ya = tens2mat(Y,3);% =A*kron(B,X) (Na x Nb Nx)

limit_error = 10^(-6);
stop_test = 1;
num_it = 0;
nmax_it = 1000;
% Compute NMSE per iteration:
% Y1_est = X_est*(khatri(P_est,A_est)).';% (F x 3M)
% func_cost_est = norm(Y1 - Y1_est,'fro')^2;
% nmseY_it(1,1) = func_cost_est/norm(Y1,'fro')^2; 
% func_cost_est = 1;
nmseY_it = zeros(1,nmax_it);
while stop_test       
    num_it = num_it + 1;                
    % LS estimation of P:
    Z3 = kron(A_old,Gamma);% (MF x R)
    
    =B*kron(A,X)
    
    P_new = Y3*pinv(Z3.');% (3 x R)
    % LS estimation of A:
    Z2 = khatri(P_new,X_est);% (3F x R)
    A_new = Y2*pinv(Z2.');% (M x R)    
    % LS estimation of X:
    Z1 = khatri(P_new,A_new);% (3M x R)
    X_new = Y1*pinv(Z1.');% (F x R)
        
    % Compute new error
    Y1_new = X_new*(khatri(P_new,A_new)).';% (F x 3M)
    func_cost_new = norm(Y1 - Y1_new,'fro')^2;
    nmseY_it(1,num_it) = func_cost_new/norm(Y1,'fro')^2; 
                      
    % Compute stop condition
%     if num_it ==1
        stop_test = (num_it < nmax_it);
%     else
%         stop_test = (num_it < nmax_it) && abs((func_cost_new-func_cost_est)/func_cost_est) > limit_error;
%     end                              
        
    % Update:
    func_cost_est = func_cost_new;
    A_est = A_new;% (M x d)
    X_est = X_new;% (F x d)
    P_est = P_new;% (3 x d)         
end    
Yhat = mat2tens(Y1_new,size(Y),1,3:2);% (F x M x 3)
Uhat = {X_new,A_new,P_new};






