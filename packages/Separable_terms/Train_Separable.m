D = params.initdict;
Y = params.data;

% params.struct_type = 'hankel';
optmization = 'alm_XY_alternate'; % Options are:
                        % 'alm_XY' : Augmented Lagrange using data matrices X and Y
                        % 'alm_XY_alternate' : Augmented Lagrange using data matrices X and Y 
                        %                      with alternating minimization
                        % 'CVX'    : CVX using only D
                        % 'CVX_XY' : CVX using data matrices X and Y
                        % 'low-rank' : Low-rank approximation of D
                        
% Indices for the reordering operators
[N,M] = size(D);
Ni = sqrt(N);
Mi = sqrt(M);
Pnm = permut(Ni,Mi);
P_rank = kron(eye(Mi),kron(Pnm,eye(Ni)));
idx = find(P_rank.'~=0) - (0:16383).'*(16384);
idx_inv = find(P_rank~=0) - (0:16383).'*(16384);
clear P_rank Pnm

%% Sparse Coding parameter setting
CODE_SPARSITY = 1;
CODE_ERROR = 2;

MEM_LOW = 1;
MEM_NORMAL = 2;
MEM_HIGH = 3;

%%%%% parse input parameters %%%%%

ompparams = {'checkdict','off'};

% coding mode %

if (isfield(params,'codemode'))
  switch lower(params.codemode)
    case 'sparsity'
      codemode = CODE_SPARSITY;
      thresh = params.Tdata;
    case 'error'
      codemode = CODE_ERROR;
      thresh = params.Edata;
    otherwise
      error('Invalid coding mode specified');
  end
elseif (isfield(params,'Tdata'))
  codemode = CODE_SPARSITY;
  thresh = params.Tdata;
elseif (isfield(params,'Edata'))
  codemode = CODE_ERROR;
  thresh = params.Edata;

else
  error('Data sparse-coding target not specified');
end


% max number of atoms %

if (codemode==CODE_ERROR && isfield(params,'maxatoms'))
  ompparams{end+1} = 'maxatoms';
  ompparams{end+1} = params.maxatoms;
end


% memory usage %

if (isfield(params,'memusage'))
  switch lower(params.memusage)
    case 'low'
      memusage = MEM_LOW;
    case 'normal'
      memusage = MEM_NORMAL;
    case 'high'
      memusage = MEM_HIGH;
    otherwise
      error('Invalid memory usage mode');
  end
else
  memusage = MEM_NORMAL;
end


% iteration count %

if (isfield(params,'iternum'))
  iternum = params.iternum;
else
  iternum = 10;
end


% omp function %

if (codemode == CODE_SPARSITY)
  ompfunc = @omp;
else
  ompfunc = @omp2;
end


% data norms %

XtX = []; XtXg = [];
if (codemode==CODE_ERROR && memusage==MEM_HIGH)
  XtX = sum(Y.^2);
end

err = zeros(1,iternum);
gerr = zeros(1,iternum);

if (codemode == CODE_SPARSITY)
  errstr = 'RMSE';
else
  errstr = 'mean atomnum';
end

%% Sparsecoding
G = [];
if (memusage >= MEM_NORMAL)
    G = D'*D;
end

if (memusage < MEM_HIGH)
  X = ompfunc(D,Y,G,thresh,ompparams{:});

else  % memusage is high

  if (codemode == CODE_SPARSITY)
    X = ompfunc(D'*Y,G,thresh,ompparams{:});

  else
    X = ompfunc(D'*Y,XtX,G,thresh,ompparams{:});
  end

end            
            
%% Optimization
switch lower(optmization)
    case 'cvx_xy'
        %% Solve via CVX using data Y
        X = X(:,1:40:end); % 256x1000 é o máximo que o servidor (noturno) aguenta
        Y = Y(:,1:40:end); % 64x1000 é o máximo que o servidor (noturno) aguenta

        % Normalization coefficient for Nuclear Norm
        g3_max = norm(Y); % max singular value
        g3 = 150*g3_max; % 50 para disp_rank 36; 150 para 16;

        cvx_begin
            variable De(size(D))
            minimize 0.5*square_pos(norm(De*X-Y,'fro')) + g3*norm_nuc(De - F1*De*F2)
        cvx_end
        D = normc(De);
        
    case 'alm_xy_alternate'
        lambda = params.alpha*norm(Y); % 450*norm(Y) para rank 21. 1000*norm(Y) para rank 14
                              % 3000*norm(Y) para rank 6
        %% Ajuste automatico para step size and u
        step = 1e-10/norm(D);
        u = 1e-10;
        
        found = false;
        
        iter = 0;
        clear lbound rbound
        while ~found
            Z = reord(D,idx)/u; 
            D_hat = zeros(size(D));
            % Min Dmod
            [U, S, V] = svd(reord(D_hat,idx) + Z ,'econ');
            diagS = diag(S);
            svp = length(find(diagS > lambda/u));
            temp_Dmod = U(:, 1:svp) * diag(diagS(1:svp) - lambda/u) * V(:, 1:svp)';
            % Min D_hat
            grad1 = D_hat - reord_inv(temp_Dmod - Z, size(D),idx_inv);
            grad2 = (D_hat*X - Y)*X.';
            norm1 = norm(step*(u*grad1 + grad2), 'fro');
            D_hat = D_hat - step*(u*grad1 + grad2);
            grad1 = D_hat - reord_inv(temp_Dmod - Z, size(D),idx_inv);
            grad2 = (D_hat*X - Y)*X.';
            norm2 = norm(step*(u*grad1 + grad2), 'fro');

            if norm2 < norm1 % Converges
               lbound = step;
               if exist('rbound','var')
                   step = (lbound + rbound)/2;
               else
                   step = step*1e3;
               end
            else % Diverges
                rbound = step;
                if exist('lbound','var')
                   step = (lbound + rbound)/2;
                else
                   step = step/1e3;
                end
            end

            if exist('lbound','var') && exist('rbound','var') 
                if (rbound - lbound)/lbound < 1e-6
                    found = true;
                    if params.sigma > 50
                        step = step/4*(trainnum/40000);
                    else
                        step = step/2*(trainnum/40000);
                    end
                end
            end
            iter = iter + 1;
        end
        step

%         found = false;
%         iter = 0;
%         clear lbound rbound
%         while ~found  
%             Z = reord(D,idx)/u;
%             D_hat = zeros(size(D)); %D;
%             % Min Dmod
%             [U, S, V] = svd(reord(D_hat,idx) + Z ,'econ');
%             diagS = diag(S);
%             svp = length(find(diagS > lambda/u));
%             temp_Dmod = U(:, 1:svp) * diag(diagS(1:svp) - lambda/u) * V(:, 1:svp)';
%             % Min D_hat
%             grad1 = D_hat - reord_inv(temp_Dmod - Z, size(D),idx_inv);
%             grad2 = (D_hat*X - Y)*X.';
%             D_hat = D_hat - step*(u*grad1 + grad2);
% 
%             Z = Z - temp_Dmod + reord(D_hat,idx);
% 
%             norm1 = norm(temp_Dmod + reord(D_hat,idx), 'fro');
% 
%            % Min Dmod
%             [U, S, V] = svd(reord(D_hat,idx) + Z ,'econ');
%             diagS = diag(S);
%             svp = length(find(diagS > lambda/u));
%             temp_Dmod = U(:, 1:svp) * diag(diagS(1:svp) - lambda/u) * V(:, 1:svp)';
%             % Min D_hat
%             grad1 = D_hat - reord_inv(temp_Dmod - Z, size(D),idx_inv);
%             grad2 = (D_hat*X - Y)*X.';
%             D_hat = D_hat - step*(u*grad1 + grad2);
% 
%             norm2 = norm(temp_Dmod + reord(D_hat,idx), 'fro');
% 
%             if norm2 < norm1 % Converges
%                lbound = u;
%                if exist('rbound','var')
%                    u = (lbound + rbound)/2;
%                else
%                    u = u*1e3;
%                end
%             else % Diverges
%                 rbound = u;
%                 if exist('lbound','var')
%                    u = (lbound + rbound)/2;
%                 else
%                    u = u/1e3;
%                 end
%             end
% 
%             if exist('lbound','var') && exist('rbound','var') 
%                 if (rbound - lbound)/lbound < 1e-6
%                     found = true;
%                     u = u/2;
%                 end
%             end
% 
%             iter = iter+1;
%         end
%         u      

        u = 1e7;
       
        %% Alternating
        success = false;
        while ~success
        try
            Z = reord(D,idx)/u;  % It is faster if  these variables are not reseted every iteration
            D_hat = zeros(size(D));
            for k = 1:iternum
                %% Sparsecoding
                G = [];
                if (memusage >= MEM_NORMAL)
                    G = D'*D;
                end

                if (memusage < MEM_HIGH)
                  X = ompfunc(D,Y,G,thresh,ompparams{:});

                else  % memusage is high

                  if (codemode == CODE_SPARSITY)
                    X = ompfunc(D'*Y,G,thresh,ompparams{:});

                  else
                    X = ompfunc(D'*Y,XtX,G,thresh,ompparams{:});
                  end

                end            

                %% Inexact ALM
                % mu_bar = u*1e7;
                % rho = 1.5      

                tol = 1e-4*norm(D,'fro'); % AQUI!!! ORIGINAL: 1e-7
                if k == iternum, tol = 1e-7*norm(D,'fro'); end
                converged = false;
                iter = 1;
                tic
                while ~converged
                % for k = 1:20
                %     primal_converged = false;
                %     while primal_converged == false

                        % Min Dmod
                        [U, S, V] = svd(reord(D_hat,idx) + Z ,'econ');
                        diagS = diag(S);
                        svp = length(find(diagS > lambda/u));
                        temp_Dmod = U(:, 1:svp) * diag(diagS(1:svp) - lambda/u) * V(:, 1:svp)';
                        % Min D_hat
                %         grad_converged = false;
                %         while ~grad_converged
                            grad1 = D_hat - reord_inv(temp_Dmod - Z,size(D),idx_inv);
                            grad2 = (D_hat*X - Y)*X.';
                            temp_D_hat = D_hat - step*(u*grad1 + grad2);
                %             if norm(D_hat - temp_D_hat, 'fro') < tol
                %                 grad_converged = true;
                %             end
                            D_hat = temp_D_hat;
                %         end

                %         if norm(D_hat - temp_D_hat, 'fro') < tol && norm(Dmod - temp_Dmod, 'fro') < tol
                %             primal_converged = true;
                %         end
                        Dmod = temp_Dmod;
                %     end

                    temp_Z = Z - Dmod + reord(D_hat,idx);

                    % stop Criterion    
                    if norm(temp_Z - Z, 'fro') < tol
                        converged = true;
                        disp(['Total nº of iterations: ' num2str(iter)]);
                    end

%                     norm(temp_Z - Z, 'fro')
                    Z = temp_Z;

                %     u = min(u*rho, mu_bar);
                    iter = iter+1;
                    if iter > 1e6, error('ALM did not converge, probably due to bad parameter setting. Retrying...'),end
                end
                toc

            % Results
    %         fprintf('\n=== RESULTS ===\n')
    %         fprintf('||D - D_hat||_F   normalized      : %1.3f\n', norm(D-D_hat,'fro')/norm(D,'fro'));
    %         fprintf('||Y - D_hat*X||_F normalized      : %1.3f\n', norm(Y-D_hat*X,'fro')/norm(Y,'fro'));
    %         fprintf('||reord(D_hat)||_* / ||reord(D)||_* : %1.1f / %1.1f\n', sum(svd(D_hat - F1*D_hat*F2)), sum(svd(D - F1*D*F2)));
    %         fprintf('rank(reord(D_hat)) / rank(reord(D)) : %d / %d\n', rank(D_hat - F1*D_hat*F2,1e-6),rank(D - F1*D*F2,1e-6));
            Dict_improvement = norm(normc(D_hat) - D)
            D = normc(D_hat);
            
            end    
            success = true;
        catch err
            disp(err.message); disp('Trying smaller step.')
            step = step/2
        end 
        end        
end