function [Ahat,Bhat,Dhat] = KSVD(nmax_it,D,A_old,B_old)
%% Power Method for computing the SVD
appSVD = 'App4';
potSVD=1;% for App1 and App3
[N,M] = size(D);
Ni = sqrt(N);
Mi = sqrt(M);

vec_D = reshape(D,Mi*Mi*Ni*Ni,1);
Pnm = permut(Ni,Mi);
P_rank = kron(eye(Mi),kron(Pnm,eye(Ni)));
vec_Dmod = P_rank*vec_D;
Dmod = reshape(vec_Dmod,Mi*Ni,Mi*Ni);
%% Start iterations:
stop_test = 1;
num_it = 0;
% u_old = randn(Mi*Ni,1);
% v_old = randn(Mi*Ni,1);
u_old = reshape(B_old,Mi*Ni,1);
v_old = reshape(conj(A_old),Mi*Ni,1);

while stop_test    
    % Compute the iterations number
    num_it = num_it + 1;
    
	switch appSVD
        case 'App1'% Estimate (Approach 1)
            v_mod = ((Dmod'*Dmod)^potSVD)*v_old;
            v_new = v_mod./norm(v_mod,'fro');
            sigma = norm(Dmod*v_new,'fro');
            u_new = Dmod*v_new./sigma;
        case 'App2'% Estimate (Approach 2)
            u_mod = Dmod*v_old;
            u_new = u_mod./norm(u_mod,'fro');
            v_mod = Dmod'*u_new;
            sigma = norm(v_mod,'fro');
            v_new = v_mod./sigma;
        case 'App3'% Estimate (Approach 3)            
            u_mod = ((Dmod*Dmod')^potSVD)*u_old;
            u_new = u_mod./norm(u_mod,'fro');
            v_mod = Dmod'*u_new;
            sigma = norm(v_mod,'fro');
            v_new = v_mod./sigma;
        case 'App4'% Estimate (Approach 4)
            v_mod = Dmod'*u_old;
            v_new = v_mod./norm(v_mod,'fro');
            u_mod = Dmod*v_new;
            sigma = norm(u_mod,'fro');
            u_new = u_mod./sigma;            
        otherwise
            disp('here!')
            stop
    end    
    
	stop_test = (num_it < nmax_it);
    u_old = u_new;
    v_old = v_new;
    
    % Convergence analysis
    B_old = reshape(sigma*u_new,Ni,Mi,1);
    A_old = reshape(conj(v_new),Ni,Mi,1);
    Dhat = kron(A_old,B_old);
    error_vec(num_it) = norm(D(:)-Dhat(:))^2/norm(D(:))^2;
end
plot(error_vec);

Bhat = reshape(sigma*u_new,Ni,Mi,1);
Ahat = reshape(conj(v_new),Ni,Mi,1);
Dhat = kron(Ahat,Bhat);