function mine_ksvddenoisedemo
%KSVDDENOISEDEMO K-SVD denoising demonstration.
%  KSVDDENISEDEMO reads an image, adds random white noise and denoises it
%  using K-SVD denoising. The input and output PSNR are compared, and the
%  trained dictionary is displayed.
%
%  To run the demo, type KSVDDENISEDEMO from the Matlab prompt.
%
%  See also KSVDDEMO.


%  Ron Rubinstein
%  Computer Science Department
%  Technion, Haifa 32000 Israel
%  ronrubin@cs
%
%  August 2009


disp(' ');
disp('  **********  K-SVD Denoising Demo  **********');
disp(' ');
disp('  This demo reads an image, adds random Gaussian noise, and denoises the image');
disp('  using K-SVD denoising. The function displays the original, noisy, and denoised');
disp('  images, and shows the resulting trained dictionary.');
disp(' ');


%% prompt user for image %%

pathstr = fileparts(which('ksvddenoisedemo'));
dirname = fullfile(pathstr, 'images', '*.png');
imglist = dir(dirname);

disp('  Available test images:');
disp(' ');
for k = 1:length(imglist)
  printf('  %d. %s', k, imglist(k).name);
end
printf('  %d. All images', length(imglist)+1);
disp(' ');

imnum = 0;
while (~isnumeric(imnum) || ~iswhole(imnum) || imnum<1 || imnum>length(imglist)+1)
  imnum = input(sprintf('  Image to denoise (%d-%d): ', 1, length(imglist)), 's');
  imnum = sscanf(imnum, '%d');
end

total_images = 1;
if imnum == length(imglist)+1, total_images = length(imglist), end
for image = 1:total_images 

if image > 1, imnum = image; end
if imnum == 6, imnum = 1; end
imgname = fullfile(pathstr, 'images', imglist(imnum).name);



%% generate noisy image %%

sigma = 20; fprintf('\nSIGMA = %d\n',sigma)
samples_training = 40000; % Standard is 40000

disp(' ');
disp('Generating noisy image...');

im = imread(imgname);
im = double(im);

params.algo = 'low_disp';   % Choose the dictionary learning strategy.
                            % Options are:
                            % 'low_disp'   Low displacement rank dictionary
                            % 'low_rank'   Low rank dictionary
                            % 'separable'  SeDiL dictionary (Hawe2013)
params.iternum = 100;

switch params.algo
    case 'low_disp'
        alpha = [0:5:20 logspace(log10(25),log10(3000),195)]; %[0:25:200 250:50:950 1050:150:3000]; %nuclear norm regularization coefficient
        % alpha = 0:20:1500;
    case 'low_rank'
        alpha = kron(2:2:64,ones(1,20)); % Low-rank approximation 20 MC iteration
    case 'separable'
        alpha = 1:10; % SeDiL
        params.iternum = 3000; % max number of iterations
    case 'faust'
        s = [3 6 12 24];
        rho = [0.7 0.9];
        params.s = kron(s, ones(1,length(rho)*10));
        params.rho = kron(ones(1,length(s)*10),rho);
        params.imagename{1} = imglist(imnum).name;
        alpha = 1:length(params.s);
        RC = zeros(size(alpha)); %Relative complexity
    case 'ksvds'
        params.Tdict_all = kron([3 6 12 18 24 32],ones(1,10)); % sparsity of each trained atom
        alpha = 1:length(params.Tdict_all);
end

% results to store
SNR = zeros(size(alpha));
nuclear_norm_dispD = zeros(size(alpha));
rank_dispD = zeros(size(alpha));
rank_D = zeros(size(alpha));


for ktrain = 1:length(samples_training)
for k = 1:length(alpha)
%% generate noisy image %%
n = randn(size(im)) * sigma;
imnoise = im + n;

%% set parameters %%

params.x = imnoise;
params.blocksize = 8;
params.dictsize = 256;
params.sigma = sigma;
params.maxval = 255;
params.trainnum = samples_training(ktrain);
params.memusage = 'high';

% parameters
params.struct_type = 'hankel';
switch lower(params.struct_type)
    case 'toeplitz'
        F1 = diag(ones(1,params.blocksize^2-1),-1);
        F2 = diag(ones(1,params.dictsize-1),-1)';
%         F2(end,1) = 1; % Using Z1 instead of Z0 (see Takahashi2013)
    case 'hankel'
        F1 = diag(ones(1,params.blocksize^2-1),-1);
        F2 = diag(ones(1,params.dictsize-1),-1);
%         F2(1,end) = 1; % Using Z1 instead of Z0 (see Takahashi2013)
    case 'vandermonde'
        % Not working!
        x = randn(1,params.blocksize^2); x = x/norm(x);% we need to choose a vector. Too restrictive!
        F1 = diag(x); 
        F2 =  diag(ones(1,params.dictsize-1),-1).';
    case 'inv_vandermonde'
        %TODO
    case 'cauchy'
        % Not working!
        x = randn(1,params.blocksize^2); x = x/norm(x);% we need to choose a vector. Too restrictive!
        y = randn(1,params.dictsize); y = y/norm(y);% we need to choose a vector. Too restrictive!
        F1 = diag(x);
        F2 = diag(y);
    case 'inv_cauchy'
        %TODO
    otherwise
        error('Invalid structure type specified');
end
params_light = rmfield(params,'x');

fprintf('\n>>>>>>>>>> Running chosen parameters - %d of %d <<<<<<<<<<\n',k,length(alpha));
params.alpha = alpha(k);

[imout, dict, dict_unnorm] = mine_ksvddenoise(params);

% show results %

% dictimg = showdict(dict,[1 1]*params.blocksize,round(sqrt(params.dictsize)),round(sqrt(params.dictsize)),'lines','highcontrast');
% figure; imshow(imresize(dictimg,2,'nearest'));
% title('Trained dictionary');
% 
% figure; imshow(im/params.maxval);
% title('Original image');
% 
% figure; imshow(imnoise/params.maxval); 
% title(sprintf('Noisy image, PSNR = %.2fdB', 20*log10(params.maxval * sqrt(numel(im)) / norm(im(:)-imnoise(:))) ));
% 
% figure; imshow(imout/params.maxval);
% title(sprintf('Denoised image, PSNR: %.2fdB', 20*log10(params.maxval * sqrt(numel(im)) / norm(im(:)-imout(:))) ));

SNR(k) = 20*log10(params.maxval * sqrt(numel(im)) / norm(im(:)-imout(:)));

% Saving results
switch params.algo
    case 'low_disp'
        nuclear_norm_dispD(k) = sum(svd(dict_unnorm - F1*dict_unnorm*F2));
        rank_dispD(k) = rank(dict_unnorm - F1*dict_unnorm*F2,norm(dict)*2e-7);
        save(strcat('new_alternate',num2str(params.iternum),'_SNR_sigma',num2str(sigma),'_',params.struct_type,'_',imglist(imnum).name(1:end-4),'_trainnum',num2str(params.trainnum)),'params_light','alpha','SNR','nuclear_norm_dispD','rank_dispD');
    case 'low_rank'
        rank_D(k)  = rank(dict,1e-6);
        save(strcat('new_SNR_sigma',num2str(sigma),'_lowrank_',imglist(imnum).name(1:end-4),'_ksvd_initialized'),'params_light','alpha','SNR','rank_D');
    case 'separable'
        save(strcat('new_SNR_sigma',num2str(sigma),'_SeDiL_',imglist(imnum).name(1:end-4),'_ODCT_initialized_trainnum',num2str(params.trainnum)),'params_light','SNR');
    case 'faust'
        RC(k) = (params.s(alpha(k))*(params.dictsize + 2*params.blocksize^2) +params.blocksize^4*params.rho(alpha(k))^3)/(params.dictsize*params.blocksize^2);
        save(strcat('new_SNR_sigma',num2str(sigma),'_FAuST_',imglist(imnum).name(1:end-4),'_trainnum',num2str(params.trainnum)),'params_light','SNR','RC');
    case 'ksvds'
        save(strcat('new_SNR_sigma',num2str(sigma),'_KSVDS_',imglist(imnum).name(1:end-4),'_trainnum',num2str(params.trainnum)),'params_light','SNR');
end

end
end
end
